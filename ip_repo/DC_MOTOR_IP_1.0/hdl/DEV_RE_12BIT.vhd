
Library IEEE;
	use	IEEE.std_logic_1164.all;
	use	IEEE.std_logic_arith.all;
	use	IEEE.std_logic_unsigned.all;

entity DEV_RE_12BIT is
    port(
        RESET      : in std_logic;
        CLK         : in std_logic;--Encoder clock, CLK should be divided version of CAPTURE_CLK
		CAPTURE_CLK	: in std_logic;
        CAPTURE		: in std_logic; --encoder counter value capture enable signal .
        PHA         : in std_logic;
        PHB         : in std_logic;
        ENC_DATA    : out std_logic_vector(11 downto 0)
    );
end DEV_RE_12BIT;

architecture MODULE of DEV_RE_12BIT is

function INC(in1 : std_logic_vector) return std_logic_vector is
        variable cy   : std_logic;
        variable temp : std_logic_vector(10 downto 0);
begin
        cy := '1';

        for i in 0 to 10 loop
                temp(i) := in1(i) xor cy;
                cy      := in1(i) and cy;
        end loop;

        return temp;
end INC;

function DEC(in1 : std_logic_vector) return std_logic_vector is
        variable cy   : std_logic;
        variable temp : std_logic_vector(10 downto 0);
begin
        cy := '0';
    for i in 0 to 10 loop
        temp(i) := not(in1(i)) xor cy;
        cy      := in1(i) or cy;
    end loop;

    return temp;
end DEC;

signal clocked_pha, clocked_phb     : std_logic;
signal filtered_pha, filtered_phb   : std_logic;
signal regpha, regphb           : std_logic_vector(2 downto 0);
signal latched_pha, latched_phb     : std_logic;
signal xor_pha, xor_phb         : std_logic;
signal upen, updn           : std_logic;
signal count_value          : std_logic_vector(10 downto 0);
signal capture_cnt         : std_logic_vector(10 downto 0);

begin

    ClockedPH : process(RESET, clk, pha, phb)
    begin
        if RESET = '1' then
            clocked_pha <= '0';
            clocked_phb <= '0';
        elsif clk = '1' and clk'event then
            clocked_pha <= pha;
            clocked_phb <= phb;
        end if;
    end process;

    FilteredPha : process(RESET, clk, clocked_pha, regpha, filtered_pha)
        variable filtered_pha_temp : std_logic;
    begin
        if regpha = "000" and clocked_pha = '0' then
            filtered_pha_temp := '0';
        elsif regpha = "111" and clocked_pha = '1' then
            filtered_pha_temp := '1';
        else
            filtered_pha_temp := filtered_pha;
        end if;

        if RESET = '1' then
            regpha <= "000";
            filtered_pha <= '0';
        elsif clk = '1' and clk'event then
            regpha <= regpha(1 downto 0) & clocked_pha;
            filtered_pha <= filtered_pha_temp;
        end if;
    end process;

    FilteredPhb : process(RESET, clk, clocked_phb, regphb, filtered_phb)
        variable filtered_phb_temp : std_logic;
    begin
        if regphb = "000" and clocked_phb = '0' then
            filtered_phb_temp := '0';
        elsif regphb = "111" and clocked_phb = '1' then
            filtered_phb_temp := '1';
        else
            filtered_phb_temp := filtered_phb;
        end if;

        if RESET = '1' then
            regphb <= "000";
            filtered_phb <= '0';
        elsif clk = '1' and clk'event then
            regphb <= regphb(1 downto 0) & clocked_phb;
            filtered_phb <= filtered_phb_temp;
        end if;
    end process;

    LatchedPha : process(RESET, clk, filtered_pha)
    begin
        if RESET = '1' then
            latched_pha <= '0';
        elsif clk = '1' and clk'event then
            latched_pha <= filtered_pha;
        end if;
    end process;

    LatchedPhb : process(RESET, clk, filtered_phb)
    begin
        if RESET = '1' then
            latched_phb <= '0';
        elsif clk = '1' and clk'event then
            latched_phb <= filtered_phb;
        end if;
    end process;

    xor_pha <= filtered_pha xor latched_pha;
    xor_phb <= filtered_phb xor latched_phb;

    Decideupdn : process(xor_pha, xor_phb, filtered_pha, filtered_phb, updn)
    begin
        if xor_pha = '1' then
            if (filtered_pha = '1' and filtered_phb = '0') or (filtered_pha = '0' and filtered_phb = '1') then
                updn <= '1'; -- UP Count
            elsif (filtered_pha = '1' and filtered_phb = '1') or (filtered_pha = '0' and filtered_phb = '0') then
                updn <= '0'; -- DOWN Count
            else
                updn <= updn;
            end if;
        elsif xor_phb = '1' then
            if (filtered_pha = '1' and filtered_phb = '1') or (filtered_pha = '0' and filtered_phb = '0') then
                updn <= '1'; -- UP Count
            elsif (filtered_pha = '1' and filtered_phb = '0') or (filtered_pha = '0' and filtered_phb = '1') then
                updn <= '0'; -- DOWN Count
            else
                updn <= updn;
            end if;
        else
            updn <= updn;
        end if;
    end process;

    Generateupen : process(RESET, xor_pha, xor_phb)
    begin
        if RESET = '1' then
            upen <= '0';
        else
            if xor_pha = '1' or xor_phb = '1' then
                upen <= '1';
            else
                upen <= '0';
            end if;
        end if;
    end process;

    INCDEC : process(RESET, clk, updn, upen, count_value)
    begin
        if RESET = '1' then
            count_value <= "10000000000";
        elsif clk = '0' and clk'event then
            if upen = '1' then
                if updn = '1' then
                    count_value <= INC(count_value);
                else
                    count_value <= DEC(count_value);
                end if;
            else
                count_value <= count_value;
            end if;
        end if;
    end process;


    CaptureCounterValue : process(RESET,CAPTURE_CLK)
    begin
        if RESET = '1' then
           capture_cnt<=(others=>'0');
        elsif falling_edge(CAPTURE_CLK) then
            if CAPTURE = '1' then
                capture_cnt <= count_value;
			else	
				capture_cnt<=capture_cnt;
            end if;
        end if;
    end process;

    enc_data <= '0' & capture_cnt;


end MODULE;

configuration RE_12BIT_BODY of DEV_RE_12BIT is
    for MODULE
    end for;
end RE_12BIT_BODY;
