library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_arith.all;
    use IEEE.std_logic_unsigned.all;

entity DEV_DC_MOTOR_CON is
    port(
        RESETB                  : in std_logic;
        MOTOR_CLOCK             : in std_logic;
        PWM_DIV                 : in std_logic_vector(7 downto 0);
        PWM_DIV_LOW             : in std_logic_vector(7 downto 0);
        MOTOR_NEAR_ENC          : in std_logic_vector(7 downto 0);
        PWM_DUTY_BASE           : in std_logic_vector(7 downto 0);
        MOTOR_SPEED_NEAR        : in std_logic_vector(7 downto 0);
        MOTOR_SPEED_FAR_CW      : in std_logic_vector(7 downto 0);
        MOTOR_SPEED_FAR_CCW     : in std_logic_vector(7 downto 0);
        MOTOR_ENC_UP            : in std_logic;
        MOTOR_DIR               : in std_logic;
        MOTOR_PULSE_CON         : in std_logic;
        TARGET_ENCODER          : in std_logic_vector(11 downto 0);
        LIMIT_L_ENCODER         : in std_logic_vector(11 downto 0);
        LIMIT_H_ENCODER         : in std_logic_vector(11 downto 0);
        MOVING_PULSE            : in std_logic_vector(15 downto 0);
        MOTOR_ON                : in std_logic;
        AUTO_CONTROL            : in std_logic;
        CONTROL_METHOD          : in std_logic;
        MOTOR_BREAK             : in std_logic;
        MOTOR_ENC_CON           : in std_logic;
        CURRENT_ENCODER         : in std_logic_vector(11 downto 0);
        END_OF_MOVING           : out std_logic;
        MOTOR_STATUS            : out std_logic_vector(1 downto 0);
        DC_MOTOR_PA             : out std_logic;
        DC_MOTOR_PB             : out std_logic;
        DC_MOTOR_EN             : out std_logic
    );
end DEV_DC_MOTOR_CON;

Architecture DC_MOTOR_CON_A of DEV_DC_MOTOR_CON is
component ENC_COMP is
    port( DataA : in std_logic_vector(11 downto 0); DataB : in
        std_logic_vector(11 downto 0); AGB : out std_logic) ;
end component;

    Constant PWM_MAX                : Integer := 220;
    Constant PWM_MAX_DC_MOTOR       : Integer := 219;
    Constant PWM_DEAD_ZONE          : Integer := 200;
    Constant PWM_STEP_CNT           : Integer := 199;
    Constant PWM_AUTO_ENC           : Integer := 198;
    Constant PWM_AUTO_DIR           : Integer := 199;
    Constant PWM_DUTY_UP            : Integer := 200;
    --constant MOTOR_SMALL_DISTANCE: std_logic_vector(11 downto 0) := "000000000110";   -- 100을 기본으로 한다.

    signal  pwm_cnt  : std_logic_vector(7 downto 0);
    signal  PWM_Duty, Duty_cnt : std_logic_vector(8 downto 0);
    signal  pwm_clock : std_logic;
    signal  PWM_wave, PWM_wave_buf : std_logic;
    signal  now_encoder, MOTOR_SMALL_DISTANCE :  std_logic_vector(11 downto 0);
    signal  gap_encoder, gap_latch :  std_logic_vector(11 downto 0);
    --signal  Moving_Step_cnt : std_logic_vector(15 downto 0);
    signal  motor_dir_ctrl : std_logic_vector(1 downto 0);
    signal  target_big : std_logic;
    signal  small_gap_id : std_logic;
    signal  Motor_A_buf, Motor_B_buf, Motor_EN_buf : std_logic;

    signal  Motor_Pulse_cnt  : std_logic_vector(15 downto 0);
    signal  cur_enc_under_HL, cur_enc_over_LL : std_logic;

begin

    -- For Xilinx Targetting
--  U100 : ENC_COMP port map (
--      target_encoder,
--      now_encoder,
--      target_big
--      );
--
--  U101 : ENC_COMP port map (
--      gap_encoder,
--      MOTOR_SMALL_DISTANCE,
--      small_gap_id
--      );

-- 현재의 Encoder 값이 Limit High 의 값보다 작아야 한다.
--  U102 : ENC_COMP port map (
--      LIMIT_H_ENCODER,
--      now_encoder,
--      cur_enc_under_HL
--      );

-- 현재의 Encoder 값이 Limit Low 의 값보다 작아야 한다.
--  U103 : ENC_COMP port map (
--      now_encoder,
--      LIMIT_L_ENCODER,
--      cur_enc_over_LL
--      );

    target_big          <=  '1' when target_encoder > now_encoder else
                            '0';

    small_gap_id        <=  '1' when gap_encoder > MOTOR_SMALL_DISTANCE else
                            '0';

    cur_enc_under_HL    <=  '1' when LIMIT_H_ENCODER > now_encoder else
                            '0';

    cur_enc_over_LL     <=  '1' when now_encoder > LIMIT_L_ENCODER else
                            '0';

    -- 입력 Clock 의 주파수는 PLL에 의해서 결정되어 진다.
    -- 만약에 10MHz 가 들어온다면, 100kHz 의 PWM 파형을 만든다고 할때,
    -- 100kHz * 100 의 주파수가 필요하다. 주기는 10us => 100ns
    -- 이러한 경우에는 입력파형을 그대로 사용하면 된다.
    -- 50kHz 의 파형으로 PWM 을 사용한다고 하면, pwm_div <= 1 을 주면
    -- 입력 주파수 10MHz 의 절반인, 5MHz 의 PWM 파형을 만들어 내게 된다.
    --End_of_Moving <= '1';
    DC_Motor_PA <= Motor_A_buf;
    DC_Motor_PB <= Motor_B_buf;
    DC_Motor_EN <= Motor_EN_buf;
    Motor_status <= motor_dir_ctrl;
    MOTOR_SMALL_DISTANCE <= "0000" & Motor_near_enc;

    Make_PWM_Clock_Counter : process(resetb, motor_clock, pwm_div, pwm_div_low, pwm_cnt, pwm_clock)
    Begin
        if resetb = '0' then
            pwm_cnt <= "00000000";
            pwm_clock <= '1';
        elsif Rising_edge(motor_clock) then
            if pwm_cnt = "00000000" then
                pwm_clock <= '0';
                pwm_cnt <= pwm_cnt + 1;
            elsif pwm_cnt = pwm_div_low then
                pwm_clock <= '1';
                pwm_cnt <= pwm_cnt + 1;
           elsif pwm_cnt = ( pwm_div + pwm_div_low )then
                pwm_clock <= pwm_clock;
                pwm_cnt <= "00000000";
            else
                pwm_clock <= pwm_clock;
                pwm_cnt <= pwm_cnt + 1;
            End if;
        End if;
    End process;

    Make_PWM_Duty_Counter : process(resetb, pwm_clock, Duty_cnt)
    Begin
        if resetb = '0' then
            Duty_cnt <= "000000000";
        elsif Rising_edge(pwm_clock) then
            if Duty_cnt = PWM_MAX_DC_MOTOR then
                Duty_cnt <= "000000000";
            else
                Duty_cnt <= Duty_cnt + 1;
            End if;
        End if;
    End process;

    PWM_Duty_Latch : process(resetb, pwm_clock, pwm_duty_base, MOVING_PULSE,MOTOR_PULSE_CON,
            Duty_cnt, Motor_enc_con, Auto_control, small_gap_id, Motor_speed_near,
            motor_dir_ctrl, Motor_speed_far_cw, Motor_speed_far_ccw, Motor_dir)
    Begin
        if resetb = '0' then
            PWM_Duty <= "000000000";
        elsif Rising_edge(pwm_clock) then
            if Duty_cnt = PWM_DUTY_UP then  -- 다음 펄스의 듀티를 결정하여야 한다,
                if ( Auto_control = '1' and Motor_enc_con = '1') then   -- Auto mode이고 ENC control mode 인경우
                    if small_gap_id = '0' then     -- Minimum encoder 차이 이내에 있다.
                -- Auto control 할때, 움직이고자 하는 위치와 현재 위치의 차이가
                -- Encoder 값으로 3이하이면, Duty 를 줄여서, Damping 을 줄여준다.
                        PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_near);
                    else
                        if motor_dir_ctrl(0) = '1' then-- CW
                            PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_cw);
                        else
                            PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_ccw);
                        End if;
                    End if;
                    End_of_Moving <= '0';
                elsif  ( Auto_control = '1' and Motor_enc_con = '0') then   -- Auto mode이고 ENC control mode 가 아닌경우
                    if MOTOR_PULSE_CON = '1' then                           -- Pulse control mode 인경우
                        if Motor_On = '1' then                              -- Motor 가 ON이면
                            if MOVING_PULSE = 0 then
                                Motor_Pulse_cnt <= (others => '0');        -- Count값을 그대로 유지(계속 Break를 하기 위해)
                                PWM_Duty <= (others => '0');            -- Duty 를 0으로 주면, Break와 동일한 효과가 난다.
                                End_of_Moving <= '0';                   -- 원하는 Pulse 만큼 움직였다.
                            else
                                if Motor_Break = '1' then                       -- Motor break ON이면
                                    Motor_Pulse_cnt <= (others => '0');         -- Pulse count를 reset하고
                                    if Motor_Dir = '1' then
                                        if cur_enc_under_HL = '1' then -- Upper limit 보다 Encoder값이 작을 경우
                                            PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_cw);
                                        else                            -- Upper limit 보다 Encoder값이 같거나 클경우, 움직이지 않는다.
                                            PWM_Duty <= (others => '0');
                                        end if;
                                    else
                                        if cur_enc_over_LL = '1' then   -- Lower limit 보다 값이 큰 경우
                                            PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_ccw);
                                        else                            -- Lower limit 보다 encoder 값이 작은 경우, 움직이지 않는다.
                                            PWM_Duty <= (others => '0');
                                        end if;
                                    End if;
                                    End_of_Moving <= '0';                       --
                                else                                            -- Break = '0' 인경우
                                    if (MOVING_PULSE > Motor_Pulse_cnt) then    -- Moving_pulse보다 Count가 적은경우
                                        Motor_Pulse_cnt <= Motor_Pulse_cnt + 1; -- Count++하고
                                        if Motor_Dir = '1' then
                                            if cur_enc_under_HL = '1' then -- Upper limit 보다 Encoder값이 작을 경우
                                                PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_cw);
                                            else                            -- Upper limit 보다 Encoder값이 같거나 클경우, 움직이지 않는다.
                                                PWM_Duty <= (others => '0');
                                            end if;
                                        else
                                            if cur_enc_over_LL = '1' then   -- Lower limit 보다 값이 큰 경우
                                                PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_ccw);
                                            else                            -- Lower limit 보다 encoder 값이 작은 경우, 움직이지 않는다.
                                                PWM_Duty <= (others => '0');
                                            end if;
                                        End if;
                                        End_of_Moving <= '0';                   -- 아직 다 움직이지 않았다.
                                    else                                        -- MOVING_PULSE = Motor_Pulse_cnt
                                        Motor_Pulse_cnt <= MOVING_PULSE;        -- Count값을 그대로 유지(계속 Break를 하기 위해)
                                        PWM_Duty <= (others => '0');            -- Duty 를 0으로 주면, Break와 동일한 효과가 난다.
                                        End_of_Moving <= '1';                   -- 원하는 Pulse 만큼 움직였다.
                                    end if;
                                end if;
                            end if;
                        else -- if Motor_On = '1' then
                            End_of_Moving <= '0';
                            Motor_Pulse_cnt <= (others => '0');
                            if Motor_Dir = '1' then
                                if cur_enc_under_HL = '1' then -- Upper limit 보다 Encoder값이 작을 경우
                                    PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_cw);
                                else                            -- Upper limit 보다 Encoder값이 같거나 클경우, 움직이지 않는다.
                                    PWM_Duty <= (others => '0');
                                end if;
                            else
                                if cur_enc_over_LL = '1' then   -- Lower limit 보다 값이 큰 경우
                                    PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_ccw);
                                else                            -- Lower limit 보다 encoder 값이 작은 경우, 움직이지 않는다.
                                    PWM_Duty <= (others => '0');
                                end if;
                            End if;
                        end if;
                    else -- if MOTOR_PULSE_CON = '1' then
                        End_of_Moving <= '0';
                        Motor_Pulse_cnt <= (others => '0');
                        if Motor_Dir = '1' then
                            if cur_enc_under_HL = '1' then -- Upper limit 보다 Encoder값이 작을 경우
                                PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_cw);
                            else                            -- Upper limit 보다 Encoder값이 같거나 클경우, 움직이지 않는다.
                                PWM_Duty <= (others => '0');
                            end if;
                        else
                            if cur_enc_over_LL = '1' then   -- Lower limit 보다 값이 큰 경우
                                PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_ccw);
                            else                            -- Lower limit 보다 encoder 값이 작은 경우, 움직이지 않는다.
                                PWM_Duty <= (others => '0');
                            end if;
                        End if;
                    end if;
                else -- elsif ( Auto_control = '1' and Motor_enc_con = '0') then
                    End_of_Moving <= '0';
                    Motor_Pulse_cnt <= (others => '0');
                    if Motor_Dir = '1' then
                        if cur_enc_under_HL = '1' then -- Upper limit 보다 Encoder값이 작을 경우
                            PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_cw);
                        else                            -- Upper limit 보다 Encoder값이 같거나 클경우, 움직이지 않는다.
                            PWM_Duty <= (others => '0');
                        end if;
                    else
                        if cur_enc_over_LL = '1' then   -- Lower limit 보다 값이 큰 경우
                            PWM_Duty <= ('0' & pwm_duty_base) + ('0' & Motor_speed_far_ccw);
                        else                            -- Lower limit 보다 encoder 값이 작은 경우, 움직이지 않는다.
                            PWM_Duty <= (others => '0');
                        end if;
                    End if;
                End if;
            End if;
        End if;
    End process;

    Encoder_Latch : process(resetb, pwm_clock, Current_encoder, Duty_cnt)
    Begin
        if resetb = '0' then
            now_encoder <= "000000000000";
        elsif Rising_edge(pwm_clock) then
            if Duty_cnt = PWM_AUTO_ENC then
                now_encoder <= Current_encoder;
            End if;
        End if;
    End process;



    make_Encoder_gap_Latch : process(resetb, pwm_clock, target_encoder, now_encoder, Duty_cnt, target_big)
    Begin
        if resetb = '0' then
            gap_encoder <= "000000000000";
            motor_dir_ctrl <= "01";
        elsif Rising_edge(pwm_clock) then
            if Duty_cnt = PWM_AUTO_DIR then
                if target_big = '1' then
                    gap_encoder <= target_encoder - now_encoder;
                    if gap_latch = 1 or gap_latch = 0 then
                        motor_dir_ctrl <= "10";
                    else
                        if Motor_enc_up = '0' then
                            motor_dir_ctrl <= "00";
                        else
                            motor_dir_ctrl <= "01";
                        End if;
                    end if;
                else
                    if ( target_encoder =  now_encoder ) then
                        gap_encoder <= "000000000000";
                        motor_dir_ctrl <= "10";
                    else
                        gap_encoder <= now_encoder - target_encoder;
                        if gap_latch = 1 or gap_latch = 0 then
                            motor_dir_ctrl <= "10";
                        else
                            if Motor_enc_up = '0' then
                                motor_dir_ctrl <= "01";
                            else
                                motor_dir_ctrl <= "00";
                            End if;
                        end if;
                    End if;
                End if;
            End if;
        End if;
    End process;

    make_Encoder_gap_Latch2 : process(resetb, pwm_clock, Duty_cnt, gap_encoder)
    Begin
        if resetb = '0' then
            gap_latch <= "000000000000";
        elsif Rising_edge(pwm_clock) then
            if Duty_cnt = PWM_AUTO_ENC then
                gap_latch <= gap_encoder;
            End if;
        End if;
    End process;


    Make_PWM_Duty : process(resetb, pwm_clock, PWM_wave_buf, PWM_Duty, Duty_cnt)
    Begin
        if resetb = '0' then
            PWM_wave_buf <= '0';
        elsif Rising_edge(pwm_clock) then
            if PWM_Duty = 0 then
                PWM_wave_buf <= '0';
            else
                if Duty_cnt = 0 then
                    PWM_wave_buf <= '1';
                elsif Duty_cnt = PWM_Duty then
                    PWM_wave_buf <= '0';
                else
                    PWM_wave_buf <= PWM_wave_buf;
                End if;
            End if;
        End if;
    end process;

    PWM_Duty_pol : process(resetb, PWM_wave_buf, CONTROL_METHOD)
    begin
        if resetb = '0' then
            PWM_wave <= '0';
        else
            if CONTROL_METHOD = '1' then
                PWM_wave <= not PWM_wave_buf;
            else
                PWM_wave <= PWM_wave_buf;
            End if;
        End if;
    end process;

    -- Motor driver TA7279P
    --IN1 IN2 OUT1    OUT2
    -- 1    1  L       L            Brake
    -- 0    1  L       H            CW/CCW
    -- 1    0  H       L            CCW/CW
    -- 0    0 High impedance        Stop
    DC_Motor_Signal_Gen : process(resetb, Motor_On, PWM_Wave, Motor_dir,
            Auto_control, CONTROL_METHOD, Motor_Break, Motor_enc_con, Motor_dir_ctrl)
    begin
        if resetb = '0' then
            Motor_A_buf <= '0';
            Motor_B_buf <= '0';
            Motor_EN_buf <= '0';
        else
            if Motor_enc_con = '0' then
                if Auto_control = '1' then
                    if Motor_On = '1' then
                        Motor_EN_buf <= '1';
                        if Motor_Break = '1' then
                            if CONTROL_METHOD = '1' then
                                Motor_A_buf <= '1';
                                Motor_B_buf <= '1';
                            else
                                Motor_A_buf <= '0';
                                Motor_B_buf <= '0';
                            End if;
                        else -- if Motor_Break = '1' then
                            if Motor_Dir = '1' then
                                if CONTROL_METHOD = '1' then   -- 한 상을 '+' 로 고정해서, 다른 상을 '+' , '-' 번갈아가면 주는 방식
                                    Motor_A_buf <= '1';
                                    Motor_B_buf <=  PWM_Wave;
                                else
                                    Motor_A_buf <= PWM_wave;--'1';
                                    Motor_B_buf <= '0';-- not PWM_Wave;
                                End if;
                            else -- if Motor_Dir = '1' then
                                if CONTROL_METHOD = '1' then   -- 한 상을 '+' 로 고정해서, 다른 상을 '+' , '-' 번갈아가면 주는 방식
                                    Motor_A_buf <= PWM_Wave;
                                    Motor_B_buf <= '1';
                                else
                                    Motor_A_buf <= '0';--'1';
                                    Motor_B_buf <= PWM_wave;--not PWM_Wave;
                                End if;
                            End if;
                        End if;
                    else -- if Motor_On = '1' then
                        Motor_A_buf <= '0';
                        Motor_B_buf <= '0';
                        Motor_EN_buf <= '0';
                    End if;
                else -- if Auto_control = '1' then
                    if Motor_On = '1' then
                        Motor_EN_buf <= '1';
                        if Motor_Break = '1' then
                            if CONTROL_METHOD = '1' then
                                Motor_A_buf <= '1';
                                Motor_B_buf <= '1';
                            else
                                Motor_A_buf <= '0';
                                Motor_B_buf <= '0';
                            End if;
                        else
                            if Motor_Dir = '1' then
                                if CONTROL_METHOD = '1' then   -- 한 상을 '+' 로 고정해서, 다른 상을 '+' , '-' 번갈아가면 주는 방식
                                    Motor_A_buf <= '1';
                                    Motor_B_buf <=  PWM_Wave;
                                else
                                    Motor_A_buf <= PWM_wave;--'1';
                                    Motor_B_buf <= '0';-- not PWM_Wave;
                                End if;
                            else
                                if CONTROL_METHOD = '1' then   -- 한 상을 '+' 로 고정해서, 다른 상을 '+' , '-' 번갈아가면 주는 방식
                                    Motor_A_buf <=  PWM_Wave;
                                    Motor_B_buf <= '1';

                                else
                                    Motor_A_buf <= '0';--not PWM_Wave;
                                    Motor_B_buf <= PWM_wave;--'1';
                                End if;
                            End if;
                        End if;
                    else    -- Manual control, Motor off (OPEN)
                        Motor_A_buf <= '0';--'1';
                        Motor_B_buf <= '0';--
                        Motor_EN_buf <= '0';
                    End if;
                End if;
            else    --if Motor_enc_con = '0' then
                if Auto_control = '1' then
                    if Motor_On = '1' then
                        Motor_EN_buf <= '1';
                        if Motor_Break = '1' then
                            if CONTROL_METHOD = '1' then
                                Motor_A_buf <= '1';
                                Motor_B_buf <= '1';
                            else
                                Motor_A_buf <= '0';
                                Motor_B_buf <= '0';
                            End if;
                        else -- if Motor_Break = '1' then
                            if Motor_dir_ctrl(1) = '1' then
                                Motor_A_buf <= '1';
                                Motor_B_buf <= '1';
                            else -- if Motor_dir_ctrl(1) = '1' then
                                if Motor_dir_ctrl(0) = '1' then
                                    if CONTROL_METHOD = '1' then   -- 한 상을 '+' 로 고정해서, 다른 상을 '+' , '-' 번갈아가면 주는 방식
                                        Motor_A_buf <= '1';
                                        Motor_B_buf <=  PWM_Wave;
                                    else
                                        Motor_A_buf <= PWM_wave;--'1';
                                        Motor_B_buf <= '0';-- not PWM_Wave;
                                    End if;
                                else
                                    if CONTROL_METHOD = '1' then   -- 한 상을 '+' 로 고정해서, 다른 상을 '+' , '-' 번갈아가면 주는 방식
                                        Motor_A_buf <= PWM_Wave;
                                        Motor_B_buf <= '1';
                                    else
                                        Motor_A_buf <= '0';--'1';
                                        Motor_B_buf <= PWM_wave;--not PWM_Wave;
                                    End if;
                                End if;
                            End if;
                        End if;
                    else -- if Motor_On = '1' then
                        Motor_A_buf <= '0';
                        Motor_B_buf <= '0';
                        Motor_EN_buf <= '0';
                    End if;
                else -- if Auto_control = '1' then
                    if Motor_On = '1' then
                        Motor_EN_buf <= '1';
                        if Motor_Break = '1' then
                            if CONTROL_METHOD = '1' then
                                Motor_A_buf <= '1';
                                Motor_B_buf <= '1';
                            else
                                Motor_A_buf <= '0';
                                Motor_B_buf <= '0';
                            End if;
                        else
                            if Motor_Dir = '1' then
                                if CONTROL_METHOD = '1' then   -- 한 상을 '+' 로 고정해서, 다른 상을 '+' , '-' 번갈아가면 주는 방식
                                    Motor_A_buf <= '1';
                                    Motor_B_buf <=  PWM_Wave;
                                else
                                    Motor_A_buf <= PWM_wave;--'1';
                                    Motor_B_buf <= '0';-- not PWM_Wave;
                                End if;
                            else
                                if CONTROL_METHOD = '1' then   -- 한 상을 '+' 로 고정해서, 다른 상을 '+' , '-' 번갈아가면 주는 방식
                                    Motor_A_buf <=  PWM_Wave;
                                    Motor_B_buf <= '1';

                                else
                                    Motor_A_buf <= '0';--not PWM_Wave;
                                    Motor_B_buf <= PWM_wave;--'1';
                                end if;
                            end if;
                        end if;
                    else    -- Manual control, Motor off (OPEN)
                        Motor_A_buf <= '0';--'1';
                        Motor_B_buf <= '0';--not PWM_Wave;
                        Motor_EN_buf <= '0';
                    end if;
                end if;
            end if;  --if Motor_enc_con = '0' then
        end if;
    end process;

end DC_MOTOR_CON_A;
