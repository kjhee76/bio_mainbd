library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use	IEEE.std_logic_arith.all;
use	IEEE.std_logic_unsigned.all;

entity DC_MOTOR_IP_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 6
	);
	port (
		-- Users to add ports here
		 MOTOR_CLOCK : in std_logic;
         ENC_PHA : in std_logic;
         ENC_PHB : in std_logic;
         
         DC_MOTOR_PA : out std_logic;
         DC_MOTOR_PB : out std_logic;
         DC_MOTOR_EN : out std_logic;
         
         DC_MOTOR_PI : in std_logic;
         
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end DC_MOTOR_IP_v1_0;

architecture arch_imp of DC_MOTOR_IP_v1_0 is

	-- component declaration
	component DC_MOTOR_IP_v1_0_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
		);
		port (
		CLRL_REG : out std_logic_vector(7 downto 0);
        ENC_LIMITH_REG : out std_logic_vector(11 downto 0);
        ENC_LIMITL_REG : out std_logic_vector(11 downto 0);
        MOVING_PULSE_REG : out std_logic_vector(15 downto 0);
        NEAR_ENC_REG : out std_logic_vector(7 downto 0);
        PWM_BASE_REG : out std_logic_vector(7 downto 0);
        PWM_DIVL_REG : out std_logic_vector(7 downto 0);
        PWM_DIV_REG : out std_logic_vector(7 downto 0);
        SPEED_FARCCW_REG : out std_logic_vector(7 downto 0);
        SPEED_FARCW_REG : out std_logic_vector(7 downto 0);
        SPEED_NEAR_REG : out std_logic_vector(7 downto 0);
        TARGEETENC_REG : out std_logic_vector(11 downto 0);
        MOTOR_STATUS : in std_logic_vector(1 downto 0);
        ENCODER_COUNT : out std_logic_vector(11 downto 0);
        END_OF_MOVING : in std_logic;
        DC_MOTOR_PI : in std_logic;
        
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component DC_MOTOR_IP_v1_0_S00_AXI;

	COMPONENT DEV_RE_12BIT
	PORT(
		RESET : IN std_logic;
		CLK : IN std_logic;
		CAPTURE_CLK : IN std_logic;
		CAPTURE : IN std_logic;
		PHA : IN std_logic;
		PHB : IN std_logic;          
		ENC_DATA : OUT std_logic_vector(11 downto 0)
		);
	END COMPONENT;
	
	COMPONENT DEV_DC_MOTOR_CON
	PORT(
		RESETB : IN std_logic;
		MOTOR_CLOCK : IN std_logic;
		PWM_DIV : IN std_logic_vector(7 downto 0);
		PWM_DIV_LOW : IN std_logic_vector(7 downto 0);
		MOTOR_NEAR_ENC : IN std_logic_vector(7 downto 0);
		PWM_DUTY_BASE : IN std_logic_vector(7 downto 0);
		MOTOR_SPEED_NEAR : IN std_logic_vector(7 downto 0);
		MOTOR_SPEED_FAR_CW : IN std_logic_vector(7 downto 0);
		MOTOR_SPEED_FAR_CCW : IN std_logic_vector(7 downto 0);
		MOTOR_ENC_UP : IN std_logic;
		MOTOR_DIR : IN std_logic;
		MOTOR_PULSE_CON : IN std_logic;
		TARGET_ENCODER : IN std_logic_vector(11 downto 0);
		LIMIT_L_ENCODER : IN std_logic_vector(11 downto 0);
		LIMIT_H_ENCODER : IN std_logic_vector(11 downto 0);
		MOVING_PULSE : IN std_logic_vector(15 downto 0);
		MOTOR_ON : IN std_logic;
		AUTO_CONTROL : IN std_logic;
		CONTROL_METHOD : IN std_logic;
		MOTOR_BREAK : IN std_logic;
		MOTOR_ENC_CON : IN std_logic;
		CURRENT_ENCODER : IN std_logic_vector(11 downto 0);          
		END_OF_MOVING : OUT std_logic;
		MOTOR_STATUS : OUT std_logic_vector(1 downto 0);
		DC_MOTOR_PA : OUT std_logic;
		DC_MOTOR_PB : OUT std_logic;
		DC_MOTOR_EN : OUT std_logic
		);
	END COMPONENT;
	
	signal  dc_motor_enc_con : std_logic;
	signal  dc_motor_break : std_logic;
	signal  dc_motor_enc_up : std_logic;
	signal  dc_motor_control_method : std_logic;
	signal  dc_motor_pulse_con : std_logic;
	signal  dc_motor_dir : std_logic;
	signal  dc_motor_auto_control : std_logic;
	signal  dc_motor_on : std_logic;
	
	signal ctrl_reg : std_logic_vector(7 downto 0);
	signal enc_limith_reg : std_logic_vector(11 downto 0);
	signal enc_limitl_reg : std_logic_vector(11 downto 0);
	signal moving_pulse_reg : std_logic_vector(15 downto 0);
	signal near_enc_reg : std_logic_vector(7 downto 0);
	signal pwm_base_reg : std_logic_vector(7 downto 0);
	signal pwm_divl_reg : std_logic_vector(7 downto 0);
	signal pwm_div_reg : std_logic_vector(7 downto 0);
	signal speed_farccw_reg : std_logic_vector(7 downto 0);
	signal speed_farcw_reg : std_logic_vector(7 downto 0);
	signal speed_near_reg : std_logic_vector(7 downto 0);
	signal targetenc_reg : std_logic_vector(11 downto 0);
	signal motor_status_reg : std_logic_vector(1 downto 0);
	signal encoder_count : std_logic_vector(11 downto 0);
	signal end_of_moving : std_logic;
	
	signal enc_data : std_logic_vector(11 downto 0);
	
    signal enc_cnt_sig      : std_logic_vector(7 downto 0);
    signal enc_clk_sig      : std_logic;
    
--    signal dc_motor_pi_sig    : std_logic;

    constant CLOCK_1M_DIV_CON: std_logic_vector(7 downto 0):=CONV_STD_LOGIC_VECTOR(50,8); --DIV : /100
begin

-- Instantiation of Axi Bus Interface S00_AXI
Inst_DC_MOTOR_IP_v1_0_S00_AXI : DC_MOTOR_IP_v1_0_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
		CLRL_REG => ctrl_reg, 
        ENC_LIMITH_REG =>enc_limith_reg,
        ENC_LIMITL_REG =>enc_limitl_reg,
        MOVING_PULSE_REG =>moving_pulse_reg,
        NEAR_ENC_REG =>near_enc_reg,
        PWM_BASE_REG =>pwm_base_reg,
        PWM_DIVL_REG =>pwm_divl_reg,
        PWM_DIV_REG  =>pwm_div_reg,
        SPEED_FARCCW_REG =>speed_farccw_reg,
        SPEED_FARCW_REG  =>speed_farcw_reg,
        SPEED_NEAR_REG  =>speed_near_reg,
        TARGEETENC_REG  =>targetenc_reg,
        MOTOR_STATUS  =>motor_status_reg,
        ENCODER_COUNT  =>encoder_count,
        END_OF_MOVING =>end_of_moving,
        DC_MOTOR_PI => DC_MOTOR_PI,
        
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

	-- Add user logic here
	Inst_DEV_RE_12BIT: DEV_RE_12BIT PORT MAP(
		RESET => s00_axi_aresetn,
		CLK => enc_clk_sig,
		CAPTURE_CLK => s00_axi_aclk,
		CAPTURE => '1',
		PHA => ENC_PHA,
		PHB => ENC_PHB,
		ENC_DATA => enc_data
	);
	
		Inst_DEV_DC_MOTOR_CON: DEV_DC_MOTOR_CON PORT MAP(
		RESETB => s00_axi_aresetn,
		MOTOR_CLOCK => MOTOR_CLOCK,
		PWM_DIV => pwm_div_reg,
		PWM_DIV_LOW => pwm_divl_reg,
		MOTOR_NEAR_ENC => near_enc_reg,
		PWM_DUTY_BASE => pwm_base_reg,
		MOTOR_SPEED_NEAR => speed_near_reg,
		MOTOR_SPEED_FAR_CW => speed_farcw_reg,
		MOTOR_SPEED_FAR_CCW => speed_farccw_reg,
		MOTOR_ENC_UP => dc_motor_enc_up,
		MOTOR_DIR => dc_motor_dir,
		MOTOR_PULSE_CON => dc_motor_pulse_con,
		TARGET_ENCODER => targetenc_reg,
		LIMIT_L_ENCODER => enc_limitl_reg,
		LIMIT_H_ENCODER => enc_limith_reg,
		MOVING_PULSE => moving_pulse_reg,
		MOTOR_ON => dc_motor_on,
		AUTO_CONTROL => dc_motor_auto_control,
		CONTROL_METHOD => dc_motor_control_method,
		MOTOR_BREAK => dc_motor_break,
		MOTOR_ENC_CON => dc_motor_enc_con,
		CURRENT_ENCODER => encoder_count,
		END_OF_MOVING => end_of_moving,
		MOTOR_STATUS => motor_status_reg,
		DC_MOTOR_PA => DC_MOTOR_PA,
		DC_MOTOR_PB => DC_MOTOR_PB,
		DC_MOTOR_EN => DC_MOTOR_EN
	);
	
	dc_motor_enc_con <= ctrl_reg(0);
dc_motor_break <= ctrl_reg(1);
dc_motor_enc_up <= ctrl_reg(2);
dc_motor_control_method <= ctrl_reg(3);
dc_motor_pulse_con <= ctrl_reg(4);
dc_motor_dir <= ctrl_reg(5);
dc_motor_auto_control <= ctrl_reg(6);
dc_motor_on <= ctrl_reg(7);

ENC_CLK_CNT_GEN : 	process(s00_axi_aresetn, s00_axi_aclk)
					begin
					if s00_axi_aresetn = '0' then
						enc_cnt_sig <= (others => '0');
						enc_clk_sig <= '0';
					elsif rising_edge(s00_axi_aclk) then
						if enc_cnt_sig = CLOCK_1M_DIV_CON then
							enc_cnt_sig <= (others => '0');
							enc_clk_sig <= not enc_clk_sig;
						else
							enc_cnt_sig <= enc_cnt_sig + 1;
							enc_clk_sig <= enc_clk_sig;
						end if;
					end if;
					end process;

	-- User logic ends

end arch_imp;
