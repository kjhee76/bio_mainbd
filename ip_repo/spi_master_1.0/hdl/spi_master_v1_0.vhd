library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spi_master_v1_0 is
	generic (
		-- Users to add parameters here
        N : positive := 40;                                             -- 24bit serial word length is default
        CPOL : std_logic := '1';                                        -- SPI mode selection (mode 0 default)
        CPHA : std_logic := '0';                                        -- CPOL = clock polarity, CPHA = clock phase.
        PREFETCH : positive := 2;                                       -- prefetch lookahead cycles
        SPI_2X_CLK_DIV : positive := 5;
		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 5
	);
	port (
		-- Users to add ports here
--        clock_i : in std_logic;
--        reset_i : in std_logic;
        spi_cs_o : out std_logic;
        spi_sck_o : out std_logic;
        spi_mosi_o : out std_logic;
        spi_miso_i : in std_logic;
--        spi_start_en_o : out std_logic;
        
		-- User ports ends
		-- Do not modify the ports beyond this line
		
		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end spi_master_v1_0;

architecture arch_imp of spi_master_v1_0 is

	-- component declaration
	component spi_master_v1_0_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 5
		);
		port (
		spi_dataH_i_reg : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);  --cpu write
        spi_dataL_i_reg : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);  --cpu write 
        spi_dataH_o_reg : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);  -- cpu read
        spi_dataL_o_reg : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        spi_data_wr_reg : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);  -- cpu write
        
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component spi_master_v1_0_S00_AXI;

	COMPONENT spi_master
	Generic (   
        N : positive := 40;                                             -- 24bit serial word length is default
        CPOL : std_logic := '1';                                        -- SPI mode selection (mode 0 default)
        CPHA : std_logic := '0';                                        -- CPOL = clock polarity, CPHA = clock phase.
        PREFETCH : positive := 2;                                       -- prefetch lookahead cycles
        SPI_2X_CLK_DIV : positive := 5
    );
	PORT (
		sclk_i : IN std_logic;
		pclk_i : IN std_logic;
		rst_i : IN std_logic;
		spi_miso_i : IN std_logic;
		di_i : IN std_logic_vector(N-1 downto 0);
		wren_i : IN std_logic;          
		spi_ssel_o : OUT std_logic;
		spi_sck_o : OUT std_logic;
		spi_mosi_o : OUT std_logic;
		di_req_o : OUT std_logic;
		wr_ack_o : OUT std_logic;
		do_valid_o : OUT std_logic;
		do_o : OUT std_logic_vector(N-1 downto 0);
		sck_ena_o : OUT std_logic;
		sck_ena_ce_o : OUT std_logic;
		do_transfer_o : OUT std_logic;
		wren_o : OUT std_logic;
		rx_bit_reg_o : OUT std_logic;
		state_dbg_o : OUT std_logic_vector(3 downto 0);
		core_clk_o : OUT std_logic;
		core_n_clk_o : OUT std_logic;
		core_ce_o : OUT std_logic;
		core_n_ce_o : OUT std_logic;
		sh_reg_dbg_o : OUT std_logic_vector(N-1 downto 0)
		);
	END COMPONENT;

signal spi_data_i : std_logic_vector (N-1 downto 0);
signal spi_data_o : std_logic_vector (N-1 downto 0);
signal spi_start_en_i : std_logic;

signal spi_data_i_H : std_logic_vector(31 downto 0);
signal spi_data_i_L : std_logic_vector (31 downto 0);	
signal spi_data_o_H : std_logic_vector (31 downto 0);
signal spi_data_o_L : std_logic_vector (31 downto 0);
signal spi_data_wr : std_logic_vector (31 downto 0);	

signal spi_data_i_reg :std_logic_vector(63 downto 0);
signal spi_data_o_reg :std_logic_vector(63 downto 0);
begin
-- Instantiation of Axi Bus Interface S00_AXI
spi_master_v1_0_S00_AXI_inst : spi_master_v1_0_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	
	port map (
	    spi_dataH_i_reg => spi_data_i_H,
        spi_dataL_i_reg => spi_data_i_L,
        spi_dataH_o_reg =>  spi_data_o_H,
        spi_dataL_o_reg => spi_data_o_L,
        spi_data_wr_reg => spi_data_wr,
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

	Inst_spi_master : spi_master	
	generic map (
		N	=> N,
		CPOL => CPOL,                                        -- SPI mode selection (mode 0 default)
        CPHA  => CPHA,                                        -- CPOL = clock polarity, CPHA = clock phase.
        PREFETCH  => PREFETCH,                                       -- prefetch lookahead cycles
        SPI_2X_CLK_DIV  => SPI_2X_CLK_DIV
	)

	PORT MAP(
		sclk_i => s00_axi_aclk,
		pclk_i => s00_axi_aclk,
		rst_i => not s00_axi_aresetn,
		spi_ssel_o => spi_cs_o,
		spi_sck_o => spi_sck_o,
		spi_mosi_o => spi_mosi_o,
		spi_miso_i => spi_miso_i,
		di_req_o => OPEN,
		di_i => spi_data_i_reg (N-1 downto 0),
		wren_i => spi_start_en_i,
		wr_ack_o => OPEN,
		do_valid_o => OPEN,
		do_o => spi_data_o_reg (N-1 downto 0),
		sck_ena_o => OPEN,
		sck_ena_ce_o => OPEN,
		do_transfer_o => OPEN,
		wren_o => OPEN,
		rx_bit_reg_o => OPEN,
		state_dbg_o => OPEN,
		core_clk_o => OPEN,
		core_n_clk_o => OPEN,
		core_ce_o => OPEN,
		core_n_ce_o => OPEN,
		sh_reg_dbg_o => OPEN
	);
	-- Add user logic here
spi_data_i_reg <= spi_data_i_H & spi_data_i_L;
spi_data_o_reg <= spi_data_o_H & spi_data_o_L;
spi_start_en_i <= spi_data_wr(0);
--spi_start_en_o <= spi_start_en_i;
	-- User logic ends

end arch_imp;
