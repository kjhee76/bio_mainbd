--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
--Date        : Fri Nov  1 09:28:24 2019
--Host        : kimjh-PC running 64-bit Service Pack 1  (build 7601)
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    CSN_SM_MOT : out STD_LOGIC;
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    EN_SM_MOT : out STD_LOGIC_VECTOR ( 0 to 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    LED_DAC_DIN : out STD_LOGIC;
    LED_DAC_LDAC : out STD_LOGIC_VECTOR ( 0 to 0 );
    LED_DAC_SCLK : out STD_LOGIC;
    LED_DAC_SYNC : out STD_LOGIC;
    REF_MOT_A : out STD_LOGIC;
    REF_MOT_B : out STD_LOGIC;
    REF_MOT_EN : out STD_LOGIC;
    SCK_SM_MOT : out STD_LOGIC;
    SDI_SM_MOT : out STD_LOGIC;
    SDO_SM_MOT : in STD_LOGIC;
    XE_A : in STD_LOGIC;
    XE_B : in STD_LOGIC;
    X_MOT_A : out STD_LOGIC;
    X_MOT_B : out STD_LOGIC;
    X_MOT_EN : out STD_LOGIC;
    X_MOT_PI : in STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    EN_SM_MOT : out STD_LOGIC_VECTOR ( 0 to 0 );
    CSN_SM_MOT : out STD_LOGIC;
    SCK_SM_MOT : out STD_LOGIC;
    SDI_SM_MOT : out STD_LOGIC;
    SDO_SM_MOT : in STD_LOGIC;
    X_MOT_A : out STD_LOGIC;
    X_MOT_B : out STD_LOGIC;
    X_MOT_EN : out STD_LOGIC;
    X_MOT_PI : in STD_LOGIC;
    XE_A : in STD_LOGIC;
    XE_B : in STD_LOGIC;
    LED_DAC_SCLK : out STD_LOGIC;
    LED_DAC_DIN : out STD_LOGIC;
    LED_DAC_SYNC : out STD_LOGIC;
    LED_DAC_LDAC : out STD_LOGIC_VECTOR ( 0 to 0 );
    REF_MOT_A : out STD_LOGIC;
    REF_MOT_B : out STD_LOGIC;
    REF_MOT_EN : out STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      CSN_SM_MOT => CSN_SM_MOT,
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      EN_SM_MOT(0) => EN_SM_MOT(0),
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      LED_DAC_DIN => LED_DAC_DIN,
      LED_DAC_LDAC(0) => LED_DAC_LDAC(0),
      LED_DAC_SCLK => LED_DAC_SCLK,
      LED_DAC_SYNC => LED_DAC_SYNC,
      REF_MOT_A => REF_MOT_A,
      REF_MOT_B => REF_MOT_B,
      REF_MOT_EN => REF_MOT_EN,
      SCK_SM_MOT => SCK_SM_MOT,
      SDI_SM_MOT => SDI_SM_MOT,
      SDO_SM_MOT => SDO_SM_MOT,
      XE_A => XE_A,
      XE_B => XE_B,
      X_MOT_A => X_MOT_A,
      X_MOT_B => X_MOT_B,
      X_MOT_EN => X_MOT_EN,
      X_MOT_PI => X_MOT_PI
    );
end STRUCTURE;
