create_project mainBoard D:/Documents/project/biometer/GIT/bio_mainbd/work -part xc7z020clg484-1


set_property target_language VHDL [current_project]
set_property  ip_repo_paths  d:/Documents/project/biometer/GIT/bio_mainbd/ip_repo [current_project]
update_ip_catalog

#Create Block Design
set_property  ip_repo_paths  d:/Documents/project/biometer/GIT/bio_mainbd/ip_repo [current_project]
update_ip_catalog

create_bd_design "design_1"

#zynq syatem
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
endgroup

#zynq DDR fixed io connection
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]

#Peripheral IO PINS configuratiion
startgroup
set_property -dict [list CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 1.8V} CONFIG.PCW_QSPI_PERIPHERAL_ENABLE {1} CONFIG.PCW_QSPI_QSPI_IO {MIO 1 .. 6} CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {1} CONFIG.PCW_ENET0_ENET0_IO {MIO 16 .. 27} CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} CONFIG.PCW_UART1_PERIPHERAL_ENABLE {1} CONFIG.PCW_UART1_UART1_IO {MIO 48 .. 49} CONFIG.PCW_SPI1_PERIPHERAL_ENABLE {1} CONFIG.PCW_SPI1_SPI1_IO {MIO 10 .. 15} CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39} CONFIG.PCW_I2C0_PERIPHERAL_ENABLE {1} CONFIG.PCW_I2C0_I2C0_IO {MIO 14 .. 15}] [get_bd_cells processing_system7_0]
endgroup

#PL Fabric Clock 100MHz
startgroup
set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {100} CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {50} CONFIG.PCW_EN_CLK1_PORT {0} CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {1}] [get_bd_cells processing_system7_0]
endgroup

#DDR3 Configuration
startgroup
set_property -dict [list CONFIG.PCW_UIPARAM_DDR_PARTNO {MT41J256M16 RE-125}] [get_bd_cells processing_system7_0]
endgroup

#spi master block design
startgroup
create_bd_cell -type ip -vlnv xilinx.com:user:spi_master:1.0 spi_master_0
endgroup
#spi master automation connection
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/spi_master_0/S00_AXI} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins spi_master_0/S00_AXI]

#DC motor block design
startgroup
create_bd_cell -type ip -vlnv xilinx.com:user:DC_MOTOR_IP:1.0 DC_MOTOR_IP_0
endgroup
#DC motor bd_connection
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {/processing_system7_0/FCLK_CLK0 (100 MHz)} Clk_slave {Auto} Clk_xbar {/processing_system7_0/FCLK_CLK0 (100 MHz)} Master {/processing_system7_0/M_AXI_GP0} Slave {/DC_MOTOR_IP_0/S00_AXI} intc_ip {/ps7_0_axi_periph} master_apm {0}}  [get_bd_intf_pins DC_MOTOR_IP_0/S00_AXI]


#constant for EN_SM_MOT
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0
endgroup
#constant output {0}
set_property -dict [list CONFIG.CONST_VAL {0}] [get_bd_cells xlconstant_0]

#Create port

create_bd_port -dir O EN_SM_MOT
connect_bd_net [get_bd_ports EN_SM_MOT] [get_bd_pins xlconstant_0/dout]
create_bd_port -dir O CSN_SM_MOT
connect_bd_net [get_bd_ports CSN_SM_MOT] [get_bd_pins spi_master_0/spi_cs_o]
create_bd_port -dir O SCK_SM_MOT
connect_bd_net [get_bd_ports SCK_SM_MOT] [get_bd_pins spi_master_0/spi_sck_o]
create_bd_port -dir O SDI_SM_MOT
connect_bd_net [get_bd_ports SDI_SM_MOT] [get_bd_pins spi_master_0/spi_mosi_o]
create_bd_port -dir I SDO_SM_MOT
connect_bd_net [get_bd_ports SDO_SM_MOT] [get_bd_pins spi_master_0/spi_miso_i]


create_bd_port -dir O X_MOT_A
startgroup
connect_bd_net [get_bd_ports X_MOT_A] [get_bd_pins DC_MOTOR_IP_0/DC_MOTOR_PA]
endgroup
create_bd_port -dir O X_MOT_B
connect_bd_net [get_bd_ports X_MOT_B] [get_bd_pins DC_MOTOR_IP_0/DC_MOTOR_PB]
create_bd_port -dir O X_MOT_DN
connect_bd_net [get_bd_ports X_MOT_DN] [get_bd_pins DC_MOTOR_IP_0/DC_MOTOR_EN]
create_bd_port -dir I X_MOT_PI
connect_bd_net [get_bd_ports X_MOT_PI] [get_bd_pins DC_MOTOR_IP_0/DC_MOTOR_PI]
create_bd_port -dir I XE_A
connect_bd_net [get_bd_ports XE_A] [get_bd_pins DC_MOTOR_IP_0/ENC_PHA]
create_bd_port -dir I XE_B
connect_bd_net [get_bd_ports XE_B] [get_bd_pins DC_MOTOR_IP_0/ENC_PHB]


#top wrapper
make_wrapper -files [get_files D:/Documents/project/biometer/GIT/bio_mainbd/work/mainBoard.srcs/sources_1/bd/design_1/design_1.bd] -top
add_files -norecurse D:/Documents/project/biometer/GIT/bio_mainbd/work/mainBoard.srcs/sources_1/bd/design_1/hdl/design_1_wrapper.vhd

#IO ports
place_ports {EN_SM_MOT[0]} V4
set_property IOSTANDARD LVCMOS33 [get_ports [list {EN_SM_MOT[0]}]]
set_property IOSTANDARD LVCMOS33 [get_ports [list CSN_SM_MOT]]
set_property IOSTANDARD LVCMOS33 [get_ports [list SCK_SM_MOT]]
set_property IOSTANDARD LVCMOS33 [get_ports [list SDI_SM_MOT]]
set_property IOSTANDARD LVCMOS33 [get_ports [list SDO_SM_MOT]]
place_ports CSN_SM_MOT V5
place_ports SCK_SM_MOT U5
place_ports SDI_SM_MOT U6
place_ports SDO_SM_MOT W7
file mkdir D:/Documents/project/biometer/GIT/bio_mainbd/work/mainBoard.srcs/constrs_1/new
close [ open D:/Documents/project/biometer/GIT/bio_mainbd/work/mainBoard.srcs/constrs_1/new/pins.xdc w ]
add_files -fileset constrs_1 D:/Documents/project/biometer/GIT/bio_mainbd/work/mainBoard.srcs/constrs_1/new/pins.xdc
set_property target_constrs_file D:/Documents/project/biometer/GIT/bio_mainbd/work/mainBoard.srcs/constrs_1/new/pins.xdc [current_fileset -constrset]
save_constraints -force

place_ports X_MOT_EN V9
set_property IOSTANDARD LVCMOS33 [get_ports [list X_MOT_EN]]
place_ports X_MOT_PI Y14
set_property IOSTANDARD LVCMOS25 [get_ports [list X_MOT_PI]]
place_ports XE_A V13
place_ports XE_B W13
set_property IOSTANDARD LVCMOS33 [get_ports [list XE_A]]
set_property IOSTANDARD LVCMOS33 [get_ports [list XE_B]]
set_property IOSTANDARD LVCMOS25 [get_ports [list XE_A]]
set_property IOSTANDARD LVCMOS25 [get_ports [list XE_B]]