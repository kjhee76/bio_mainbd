
/*
#include "OCT_Def.h"
#include "OCT_Uart.h"
#include "OCT_Dev.h" 
#include "OCT_DC_Motor.h"
#include "OCT_FPGA.h"
#include "OCT_AF.h"
//#include "OCT_Timer.h"
#include "OCT_SysVar.h"
#include "AM1808_timer.h"
#include "OCT_track.h"
*/
#include "xil_types.h"
#include "dc_motor.h"
#include "bio_hbs.h"
#include "track.h"
#include "sysVar.h"
#include "autofocus.h"

//	Common Control Command
//////////////////////////////////////////////////////////////////////////

#define ON				1
#define OFF				0

#define DCM_DEBUG

union	DC_Motor_Control	dcm_ctrl_reg[DCM_NUM];

dcm_control_t       dcm_ctrl[DCM_NUM] ;  
dcm_state_t         dcm_stat[DCM_NUM] ;
dcm_setup_t 		dcm_setup[DCM_NUM] ;

motor_setup_t 			motor_setup ;
dcmotor_force_move_flag_t dcmotor_force_move_flag;

extern HBS_st HBS;
extern track_stat_t 	track_stat ;


//Z motor CW : 피검자 쪽으로 ENCODER 는 UP 방향 : SPRING 누르는 방향
//Z motor CCW : 사용자 쪽으로 ENCODER 는 DOWON 방향 : SPRING 펴지는 방향
//Z PI = 0 : 피검자 쪽, PI가 안가려졌음
//Z PI = 1 : 사용자 쪽, PI가 가려졌음
//
//X motor CW : 사용자측면에서 볼때 왼쪽(피검자쪽에서 오른쪽) 쪽으로 ENCODER 는 UP 방향 : SPRING 누르는 방향
//X motor CCW : 사용자측면에서 볼때 오른쪽(피검자쪽에서 왼쪽) ENCODER 는 DOWON 방향 : SPRING 펴지는 방향
//X PI = 0 : 왼쪽(사용자측에서), PI가 안가려졌음
//X PI = 1 : 오른쪽(사용자측에서), PI가 가려졌음
//
//Y motor CW : 사용자측면에서 볼때 하단방향 쪽으로 ENCODER 는 UP 방향
//Y motor CCW : 사용자측면에서 볼때 상단방향 ENCODER 는 DOWON 방향
//Y PI = 0 : 상단(사용자측에서), PI가 안가려졌음
//Y PI = 1 : 하단(사용자측에서), PI가 가려졌음


void init_DCM_sys_var(void)
{
//REF Motor Only for TEST
#if 0
#if defined (DCM_DEBUG)
	printf("HBS.SysCal.AutoStage_Cal.dcm_setup_x address: 0x%X size: %d \n", (u32)&HBS.SysCal.AutoStage_Cal.dcm_setup_x , sizeof(HBS.SysCal.AutoStage_Cal.dcm_setup_x));
	printf("HBS.SysCal.AutoStage_Cal.dcm_setup_z address: 0x%X size: %d \n", (u32)&HBS.SysCal.AutoStage_Cal.dcm_setup_z , sizeof(HBS.SysCal.AutoStage_Cal.dcm_setup_z));
#endif
#endif
	/*
	dcm_setup[DCM_XH].duty_base		= HBS.SysCal.AutoStage_Cal.dcm_setup_x.duty_base;
	dcm_setup[DCM_XH].duty_fcw 		= HBS.SysCal.AutoStage_Cal.dcm_setup_x.duty_fcw ;
	dcm_setup[DCM_XH].duty_fccw 	= HBS.SysCal.AutoStage_Cal.dcm_setup_x.duty_fccw;
	dcm_setup[DCM_XH].duty_ncw 		= HBS.SysCal.AutoStage_Cal.dcm_setup_x.duty_ncw;
	dcm_setup[DCM_XH].duty_nccw 	= HBS.SysCal.AutoStage_Cal.dcm_setup_x.duty_nccw;
	dcm_setup[DCM_XH].enc_near 		= HBS.SysCal.AutoStage_Cal.dcm_setup_x.enc_near;
	dcm_setup[DCM_XH].enc_offs 		= HBS.SysCal.AutoStage_Cal.dcm_setup_x.enc_offs;

	dcm_setup[DCM_ZH].duty_base 	= HBS.SysCal.AutoStage_Cal.dcm_setup_z.duty_base;
	dcm_setup[DCM_ZH].duty_fcw 		= HBS.SysCal.AutoStage_Cal.dcm_setup_z.duty_fcw ;
	dcm_setup[DCM_ZH].duty_fccw 	= HBS.SysCal.AutoStage_Cal.dcm_setup_z.duty_fccw;
	dcm_setup[DCM_ZH].duty_ncw 		= HBS.SysCal.AutoStage_Cal.dcm_setup_z.duty_ncw;
	dcm_setup[DCM_ZH].duty_nccw		= HBS.SysCal.AutoStage_Cal.dcm_setup_z.duty_nccw;
	dcm_setup[DCM_ZH].enc_near 		= HBS.SysCal.AutoStage_Cal.dcm_setup_z.enc_near;
	dcm_setup[DCM_ZH].enc_offs 		= HBS.SysCal.AutoStage_Cal.dcm_setup_z.enc_offs;

	dcm_setup[DCM_XH].duty_base		= 30;
	dcm_setup[DCM_XH].duty_fcw 		= 120 ;
	dcm_setup[DCM_XH].duty_fccw 	= 120;
	dcm_setup[DCM_XH].duty_ncw 		= 30;
	dcm_setup[DCM_XH].duty_nccw 	= 30;
	dcm_setup[DCM_XH].enc_near 		= HBS.SysCal.AutoStage_Cal.dcm_setup_x.enc_near;
	dcm_setup[DCM_XH].enc_offs 		= HBS.SysCal.AutoStage_Cal.dcm_setup_x.enc_offs;

	dcm_setup[DCM_YH].duty_base 	= 30;
	dcm_setup[DCM_YH].duty_fcw 		= 120 ;
	dcm_setup[DCM_YH].duty_fccw 	= 120;
	dcm_setup[DCM_YH].duty_ncw 		= 30;
	dcm_setup[DCM_YH].duty_nccw		= 30;
	dcm_setup[DCM_YH].enc_near 		= HBS.SysCal.AutoStage_Cal.dcm_setup_z.enc_near;
	dcm_setup[DCM_YH].enc_offs 		= HBS.SysCal.AutoStage_Cal.dcm_setup_z.enc_offs;


	dcm_setup[DCM_ZH].duty_base 	= 30;
	dcm_setup[DCM_ZH].duty_fcw 		= 120 ;
	dcm_setup[DCM_ZH].duty_fccw 	= 120;
	dcm_setup[DCM_ZH].duty_ncw 		= 30;
	dcm_setup[DCM_ZH].duty_nccw		= 30;
	dcm_setup[DCM_ZH].enc_near 		= HBS.SysCal.AutoStage_Cal.dcm_setup_z.enc_near;
	dcm_setup[DCM_ZH].enc_offs 		= HBS.SysCal.AutoStage_Cal.dcm_setup_z.enc_offs;
	*/

	dcm_setup[DCM_REF].duty_base 	= 30;
	dcm_setup[DCM_REF].duty_fcw 	= 120 ;
	dcm_setup[DCM_REF].duty_fccw 	= 120;
	dcm_setup[DCM_REF].duty_ncw 	= 30;
	dcm_setup[DCM_REF].duty_nccw	= 30;
	dcm_setup[DCM_REF].enc_near 	= HBS.SysCal.AutoStage_Cal.dcm_setup_z.enc_near;
	dcm_setup[DCM_REF].enc_offs 	= HBS.SysCal.AutoStage_Cal.dcm_setup_z.enc_offs;
	return ;
}

void init_dc_motors(void)
{
    int         i ;

    init_DCM_sys_var();

#if 0
    for (i = 0; i < DCM_NUM; i++) {
        motor_enable(i, FALSE) ;
    }

    for (i = 0; i < DCM_NUM; i++) {
        init_dc_motor(i) ;
    }
#endif

    motor_enable(DCM_REF, FALSE) ;
    init_dc_motor(DCM_REF) ;

    DCMOTREF_LIMIT_H_REG = 0xFFE;
    DCMOTREF_LIMIT_L_REG = 0x001;
    DCMOTREF_ENCODER_DATA_REG = 0x0400;

#if 0
    dcmotor_force_move_flag.x=FALSE;
    dcmotor_force_move_flag.z=FALSE;
    track_stat.on_xmove_enc	=FALSE;
    track_stat.on_xmove		=FALSE;
    track_stat.on_zmove_enc	=FALSE;
    track_stat.on_zmove		=FALSE;
    track_stat.ss_cntx=0;
    track_stat.ss_cntz=0;
#endif

    // encoder 값은 앞으로 가면, 늘어나고 뒤로 오면 줄어든다.
    // PI 는 front half 에서 off, rear half 에서 ON 상태로 된다.
    // Front 에서 Clockwise면 rear 방향으로 오른쪽으로 돌면서
    // counterclockwise면, 왼쪽으로 돌면서 rear 로 간다.
#if 0
	//  11bit Encoder
    dcm_stat[DCM_XH].center_enc     = 0x0400 ;
    dcm_stat[DCM_CR].center_enc     = 0x0400 ;	// Chin reset motor
    dcm_stat[DCM_ZH].center_enc     = 0x0400 ;
    dcm_stat[DCM_XH].center_set     = FALSE ;
    dcm_stat[DCM_CR].center_set     = FALSE ;	// Chin reset motor
    dcm_stat[DCM_ZH].center_set     = FALSE ;

    HBS.InitStat.DevStaus|=INIT_CHIN_M_BIT;

    for (i = 0; i < DCM_NUM; i++) {
        motor_brake(i, TRUE) ;
    }
#endif
    motor_brake(DCM_REF, TRUE) ;
    return ;
}

void init_dc_motor(int motor_no)
{
	 	init_dcm_ctrl(motor_no) ;

	    switch (motor_no) {

	    case DCM_CR:
	            //prev, chinrest motor only
	            dcm_ctrl[DCM_CR].pwm_div          = 12 ; // 12			//15MHz/12=1.25MHz ref clk. ref_clk/220= 5.68kHz PWM frequency, low audible noise
	            dcm_ctrl[DCM_CR].duty_base        = 70 ; // 50 ;    //12.5 seconds up/dn configuration duty_bas3=50, speed_xx_cw=65 , duty_base+speed_xx_cw max 220, at 22uH inductor with copal motor,peak current 3A
	            dcm_ctrl[DCM_CR].speed_far_cw     = 150 ; // 65 ;      // 왼쪽으로 움직이는 방향 속도,실제 pwm_duty=speed_far_cw+duty_base
	            dcm_ctrl[DCM_CR].speed_far_ccw    = 150 ; // 65;      // 오른쪽으로 움직이는 방향 속도
	            dcm_ctrl[DCM_CR].enc_ctrl         = OFF ;
	            dcm_ctrl[DCM_CR].rotate_dir       = ROTATE_CW ;
	            dcm_ctrl[DCM_CR].auto_ctrl        = OFF ;
	            dcm_ctrl[DCM_CR].side             = CR_LOW_SIDE ; //  LOW_SIDE ;

	        //    motor_brake(DCM_CR, TRUE) ;
	            break ;

	    case DCM_XH:
	            dcm_ctrl[motor_no].pwm_div          = 72 ; // 5 ;
	            dcm_ctrl[motor_no].duty_base        = 30 ;      // 30 ;
	            dcm_ctrl[motor_no].speed_far_cw     = 0 ; // 0 ;		// Spring 을 누르러 가는 방향 속도 L 방향
	            dcm_ctrl[motor_no].speed_far_ccw    = 0 ; // 0 ;
	            dcm_ctrl[motor_no].speed_near       = 0 ; // 0 ;
	            dcm_ctrl[motor_no].enc_near         = 10 ;
	            dcm_ctrl[motor_no].enc_ctrl         = OFF ;
	            dcm_ctrl[motor_no].rotate_dir       = ROTATE_CW ;
	            dcm_ctrl[motor_no].auto_ctrl        = OFF ;
	            dcm_ctrl[motor_no].side             = LOW_SIDE ; //  LOW_SIDE ;
	            break ;

	    case DCM_YH:
	            dcm_ctrl[motor_no].pwm_div          = 72 ; // 5 ;
	            dcm_ctrl[motor_no].duty_base        = 30 ;      // 30 ;
	            dcm_ctrl[motor_no].speed_far_cw     = 0 ; // 0 ;		// Spring 을 누르러 가는 방향 속도 L 방향
	            dcm_ctrl[motor_no].speed_far_ccw    = 0 ; // 0 ;
	            dcm_ctrl[motor_no].speed_near       = 0 ; // 0 ;
	            dcm_ctrl[motor_no].enc_near         = 10 ;
	            dcm_ctrl[motor_no].enc_ctrl         = OFF ;
	            dcm_ctrl[motor_no].rotate_dir       = ROTATE_CW ;
	            dcm_ctrl[motor_no].auto_ctrl        = OFF ;
	            dcm_ctrl[motor_no].side             = LOW_SIDE ; //  LOW_SIDE ;
	            break ;

	    case DCM_ZH:
	            dcm_ctrl[motor_no].pwm_div          = 72 ; // 5 ;
	            dcm_ctrl[motor_no].duty_base        = 20 ;
	            dcm_ctrl[motor_no].speed_far_cw     = 65;//40 ;      // Front 방향 눈으로 다가가는 방향의 속도, 스프링을 당기는
	            dcm_ctrl[motor_no].speed_far_ccw    = 40;//65 ;      // 눈에서 멀어지는 방향의 속도, spring을 누르러 가는 방향
	            dcm_ctrl[motor_no].speed_near       = 5 ;
	            dcm_ctrl[motor_no].enc_near         = 6 ;
	            dcm_ctrl[motor_no].enc_ctrl         = OFF ;
	            dcm_ctrl[motor_no].rotate_dir       = ROTATE_CW ;
	            dcm_ctrl[motor_no].auto_ctrl        = OFF ;
	            dcm_ctrl[motor_no].side             = LOW_SIDE ; //  LOW_SIDE ;
	            break ;

	    case DCM_REF:
	            dcm_ctrl[motor_no].pwm_div          = 12 ; // 5 ;
	            dcm_ctrl[motor_no].duty_base        = 70 ;
	            dcm_ctrl[motor_no].speed_far_cw     = 150;//40 ;      // Front 방향 눈으로 다가가는 방향의 속도, 스프링을 당기는
	            dcm_ctrl[motor_no].speed_far_ccw    = 150;//65 ;      // 눈에서 멀어지는 방향의 속도, spring을 누르러 가는 방향
	            dcm_ctrl[motor_no].speed_near       = 5 ;
	            dcm_ctrl[motor_no].enc_near         = 6 ;
	            dcm_ctrl[motor_no].enc_ctrl         = OFF ;
	            dcm_ctrl[motor_no].rotate_dir       = ROTATE_CW ;
	            dcm_ctrl[motor_no].auto_ctrl        = OFF ;
	            dcm_ctrl[motor_no].side             = LOW_SIDE ; //  LOW_SIDE ;
	            break ;
	    }

	    dcm_stat[motor_no].on_brake         = FALSE ;
	    dcm_stat[motor_no].on_move          = FALSE ;
	    dcm_stat[motor_no].on_dir           = ROTATE_CW ;
	    dcm_stat[motor_no].on_near 			= FALSE ;

	    update_motor_divider(motor_no) ;
	    update_motor_speed(motor_no) ;
	    update_motor_ctrl(motor_no) ;
	    //HBS.InitStat.DevStaus|= INIT_CMOTOR_BIT;
	    return ;
}
//master x move


void init_dcm_ctrl(int motor_no)
{
	dcm_ctrl_reg[motor_no].byte					= DCM_MOTOR_DEF_VAL ;
	dcm_ctrl_reg[motor_no].b.Motor_Auto			= OFF;
	dcm_ctrl_reg[motor_no].b.Motor_Break			= OFF;
	if(motor_no==DCM_CR){
		dcm_ctrl_reg[motor_no].b.Motor_Con			= CR_LOW_SIDE;
	}
	else
	{
		dcm_ctrl_reg[motor_no].b.Motor_Con			= LOW_SIDE;
	}
//	OCT_motor[motor_no].b.Motor_Con				= LOW_SIDE;
	dcm_ctrl_reg[motor_no].b.Motor_Dir				= ROTATE_CW;	// counterclockwise
	dcm_ctrl_reg[motor_no].b.Motor_On				= OFF;
	dcm_ctrl_reg[motor_no].b.Motor_Start			= OFF; // currently CTRL_SRC
	dcm_ctrl_reg[motor_no].b.Motor_Enc_con			= OFF;

    return ;
}

void motor_enable(int motor_no, int flag)
{
	if(flag)	dcm_ctrl[motor_no].motor_on		= TRUE;
	else		dcm_ctrl[motor_no].motor_on		= FALSE;
	
	if(flag)	dcm_ctrl_reg[motor_no].b.Motor_On	= TRUE;
	else		dcm_ctrl_reg[motor_no].b.Motor_On	= FALSE;



	if(motor_no == DCM_ZH)
	{
		DCMOTZ_CTRL_REG	=	dcm_ctrl_reg[motor_no].byte;
	}
	else if(motor_no == DCM_XH)
	{
		DCMOTX_CTRL_REG	=	dcm_ctrl_reg[motor_no].byte;
	}
	else if(motor_no == DCM_CR)
	{
		DCMOTCR_CTRL_REG		=	dcm_ctrl_reg[motor_no].byte;
	}
	else if(motor_no == DCM_REF)
	{
		DCMOTREF_CTRL_REG		=	dcm_ctrl_reg[motor_no].byte;
	}
	    return ;

}

void motor_side(int motor_no, int side) 
{   
    if (side != HIGH_SIDE && side != LOW_SIDE) {
        return ;
    }
    else {
    	dcm_ctrl_reg[motor_no].b.Motor_Con         = side ;
        update_motor_ctrl(motor_no) ;
    }
    return ;
}

void motor_rotate(int motor_no, int dir) 
{
    if (dir != ROTATE_CW && dir != ROTATE_CCW) {
    	 return ;
    }
    else {
        dcm_ctrl[motor_no].rotate_dir       = dir ;
        dcm_ctrl_reg[motor_no].b.Motor_Dir     = dir ;
        update_motor_ctrl(motor_no) ;
    }
    return ;
}

void motor_brake(int motor_no, int on_brake)
{
    if (on_brake) {
    	dcm_ctrl_reg[motor_no].b.Motor_On      = FALSE ;
    	dcm_ctrl_reg[motor_no].b.Motor_Break   = ON ;
    }
    else {
    	dcm_ctrl_reg[motor_no].b.Motor_On      = FALSE ;
        dcm_ctrl_reg[motor_no].b.Motor_Break   = OFF ;
    }

    update_motor_ctrl(motor_no) ;
    return ;
}

void motor_move(int motor_no, int on_move)
{
    if (on_move) {
    	dcm_ctrl_reg[motor_no].b.Motor_On      = TRUE ;
    	dcm_ctrl_reg[motor_no].b.Motor_Break   = OFF ;
    }
    else {
    	dcm_ctrl_reg[motor_no].b.Motor_On      = FALSE ;
    	dcm_ctrl_reg[motor_no].b.Motor_Break   = OFF ;
    }

    update_motor_ctrl(motor_no) ;
    return ;
}

void update_motor_speed(int motor_no) 
{
    
	 switch (motor_no) {

	    case DCM_CR:
	    		DCMOTCR_PWM_DUTY_BASE_REG	= dcm_ctrl[motor_no].duty_base ;
	    		DCMOTCR_FAR_CW_REG			= dcm_ctrl[motor_no].speed_far_cw ;
	    		DCMOTCR_FAR_CCW_REG			= dcm_ctrl[motor_no].speed_far_ccw ;
	            break ;

	    case DCM_XH:
	    		DCMOTX_PWM_DUTY_BASE_REG	= dcm_ctrl[motor_no].duty_base ;
	    		DCMOTX_NEARENC_REG			= dcm_ctrl[motor_no].enc_near ;
	    		DCMOTX_SPEED_NEAR_REG		= dcm_ctrl[motor_no].speed_near;
	    		DCMOTX_FAR_CW_REG			= dcm_ctrl[motor_no].speed_far_cw  ;
	    		DCMOTX_FAR_CCW_REG			= dcm_ctrl[motor_no].speed_far_ccw ;
	            break ;

	    case DCM_YH:
	    		DCMOTY_PWM_DUTY_BASE_REG	= dcm_ctrl[motor_no].duty_base ;
	    		DCMOTY_NEARENC_REG			= dcm_ctrl[motor_no].enc_near ;
	    		DCMOTY_SPEED_NEAR_REG		= dcm_ctrl[motor_no].speed_near;
	    		DCMOTY_FAR_CW_REG			= dcm_ctrl[motor_no].speed_far_cw  ;
	    		DCMOTY_FAR_CCW_REG			= dcm_ctrl[motor_no].speed_far_ccw  ;
	            break ;

	    case DCM_ZH:
	    		DCMOTZ_PWM_DUTY_BASE_REG	= dcm_ctrl[motor_no].duty_base ;
	    		DCMOTZ_NEARENC_REG			= dcm_ctrl[motor_no].enc_near ;
	    		DCMOTZ_SPEED_NEAR_REG		= dcm_ctrl[motor_no].speed_near;
	    		DCMOTZ_FAR_CW_REG			= dcm_ctrl[motor_no].speed_far_cw  ;
	    		DCMOTZ_FAR_CCW_REG			= dcm_ctrl[motor_no].speed_far_ccw ;
	            break ;

	    case DCM_REF:
	    		DCMOTREF_PWM_DUTY_BASE_REG	= dcm_ctrl[motor_no].duty_base ;
	    		DCMOTREF_NEARENC_REG		= dcm_ctrl[motor_no].enc_near ;
	    		DCMOTREF_SPEED_NEAR_REG		= dcm_ctrl[motor_no].speed_near;
	    		DCMOTREF_FAR_CW_REG			= dcm_ctrl[motor_no].speed_far_cw  ;
	    		DCMOTREF_FAR_CCW_REG		= dcm_ctrl[motor_no].speed_far_ccw ;
	            break ;

	    }

    return ;
}


void update_motor_ctrl(int motor_no) 
{

	 switch (motor_no) {

	    case DCM_CR:
	    		DCMOTCR_CTRL_REG		= dcm_ctrl_reg[motor_no].byte;
	            break ;

	    case DCM_XH:
	    		DCMOTX_CTRL_REG	= dcm_ctrl_reg[motor_no].byte;
	            break ;

	    case DCM_YH:
	    		DCMOTY_CTRL_REG	= dcm_ctrl_reg[motor_no].byte;
	            break ;

	    case DCM_ZH:
	    		DCMOTZ_CTRL_REG	= dcm_ctrl_reg[motor_no].byte;
	            break ;

	    case DCM_REF:
	    		DCMOTREF_CTRL_REG	= dcm_ctrl_reg[motor_no].byte;
	            break ;

	    }


	 return ;

 }

/* for Biometer*/
int update_motor_divider(int motor_no) 
{
	int		div, divh, divl;

    div     = dcm_ctrl[motor_no].pwm_div ;

    if (div < 0 || div >= 256) {
        return -1 ;
    }

    if (div % 2) {
		divl = div / 2;
		divh = div - divl;
	}
	else {
		divh = divl = div / 2;
	}


    switch (motor_no) {
      case DCM_CR:
    	  DCMOTCR_PWM_DIV_REG		= divh;
    	  DCMOTCR_PWM_DIVL_REG		= divl;
    	  break ;

      case DCM_XH:
    	  DCMOTX_PWM_DIV_REG	= divh;
    	  DCMOTX_PWM_DIVL_REG	= divl;
          break ;

      case DCM_YH:
    	  DCMOTY_PWM_DIV_REG	= divh;
    	  DCMOTY_PWM_DIVL_REG	= divl;
          break ;

      case DCM_ZH:
    	  DCMOTZ_PWM_DIV_REG	= divh;
    	  DCMOTZ_PWM_DIVL_REG	= divl;
          break ;

      case DCM_REF:
    	  DCMOTREF_PWM_DIV_REG	= divh;
    	  DCMOTREF_PWM_DIVL_REG	= divl;
          break ;
      }
      
    return (div + 1) ;
}


int DCM_move_center(int motor_no)
{
    switch (motor_no) {
    case DCM_XH:
            return DCM_move_center_xh() ;

    case DCM_ZH:
            return DCM_move_center_zh() ;

    default:
            return FALSE ;
    }
}

int ccw_movent_check(int motor_no,int duty_ccw,int duty_cw)
{
	int			cnt;
	int			exp_cnt1 = 600 ;
	u16         enc_v ;
	int        pis_v ;
	int			enc_diff = 3 ;
	int 		ret;
	for (cnt = 0, ret = FALSE; cnt < 3; cnt++)
	{
	       DCM_move_ccw(motor_no, duty_ccw) ; //moving to user side
	       ret	= wait_enc_diff2(motor_no, enc_diff, exp_cnt1) ;
		   if (ret == FALSE) {
				DCM_move_cw(motor_no, duty_cw) ;
				usleep(250000) ;
				DCM_brake(motor_no) ;
				usleep(500000) ;
				enc_v      = enc_read(motor_no) ;
				pis_v      = pis_read(motor_no) ;
			}
			else
			{
			  break ;
			}
	}

	return ret;
}

int cw_movent_check(int motor_no,int duty_ccw,int duty_cw)
{
	int			cnt;
	int			exp_cnt1 = 600 ;
	u16         enc_v ;
	int        pis_v ;
	int			enc_diff = 3 ;
	int 		ret;

	for (cnt = 0, ret = FALSE; cnt < 3; cnt++)
	{
		DCM_move_cw(motor_no, duty_cw) ;
		ret		= wait_enc_diff2(motor_no, enc_diff, exp_cnt1) ;
		if (ret == FALSE) {
			DCM_move_ccw(motor_no, duty_ccw) ;
			usleep(250000);
			DCM_brake(motor_no) ;
			usleep(500000);
			enc_v      = enc_read(motor_no) ;
			pis_v      = pis_read(motor_no) ;
		}
		else
		{
			break ;
		}
	}


	return ret;
}

int ccw_find_pi_on(int motor_no,int duty_ccw,int duty_cw)
{
	int			cnt;
	int			exp_cnt2 = 900 ;

	int 		ret;

	for (cnt = 0, ret = FALSE; cnt < 3; cnt++)
	{
	      DCM_move_ccw(motor_no, duty_ccw) ;
		  ret= wait_pi_signal2(motor_no, ON, exp_cnt2) ;
		  if (ret == FALSE)
		  {
			DCM_move_cw(motor_no, duty_cw) ;
			usleep(250000);
			DCM_brake(motor_no) ;
			usleep(250000);
		  }
		else
		{
			break ;
		}
	}

	return ret;

}

int cw_find_pi_on(int motor_no,int duty_ccw,int duty_cw)
{
	int			cnt;
	int			exp_cnt2 = 900 ;

	int 		ret;

	for (cnt = 0, ret = FALSE; cnt < 3; cnt++)
	{
	      DCM_move_cw(motor_no, duty_ccw) ;
		  ret= wait_pi_signal2(motor_no, ON, exp_cnt2) ;
		  if (ret == FALSE)
		  {
			DCM_move_ccw(motor_no, duty_cw) ;
			usleep(250000);
			DCM_brake(motor_no) ;
			usleep(250000);
		  }
		else
		{
			break ;
		}
	}

	return ret;

}




int cw_find_pi_off(int motor_no,int duty_ccw,int duty_cw)
{
	int			cnt;
	int			exp_cnt1 = 600 ;


	int 		ret;

	for (cnt = 0, ret = FALSE; cnt < 3; cnt++)
	{
		DCM_move_cw(motor_no, duty_cw) ;
		ret	= wait_pi_signal2(motor_no, OFF, exp_cnt1) ;
		if (ret == FALSE)
		{
			DCM_move_ccw(motor_no, duty_ccw) ;
			usleep(250000);
			DCM_brake(motor_no) ;
			usleep(250000);
		}
		else
		{
			break ;
		}
	}

	return ret;

}

int ccw_find_pi_off(int motor_no,int duty_ccw,int duty_cw)
{
	int			cnt;
	int			exp_cnt1 = 600 ;


	int 		ret;

	for (cnt = 0, ret = FALSE; cnt < 3; cnt++)
	{
		DCM_move_ccw(motor_no, duty_cw) ;
		ret	= wait_pi_signal2(motor_no, OFF, exp_cnt1) ;
		if (ret == FALSE)
		{
			DCM_move_cw(motor_no, duty_ccw) ;
			usleep(250000);
			DCM_brake(motor_no) ;
			usleep(250000);
		}
		else
		{
			break ;
		}
	}

	return ret;

}

int DCM_move_center_xh(void)
{
    u16         enc_v1, enc_v2 ;
    int        pis_v1, pis_v2 ;

    int         motor_no ;
	int			duty_ccw, duty_cw ;
	int			cnt, ret ;

	printf("Detecting XH center ...\n") ;

    motor_no    = DCM_XH ;
    if (dcm_ctrl[motor_no].auto_ctrl == ON) {
	//	uart_printf(dbg_msg1, "err: auto ctrl") ;
        return FALSE ;
    }

	//	Wait until x movement is compeletly stopped.
	///////////////////////////////////////////////////////////////////////
	DCM_brake(motor_no) ;
	enc_v1      = enc_read(motor_no) ;
	for (cnt = 0; cnt < 60; cnt++) {
		usleep(50000);
		if (enc_v1 != enc_read(motor_no)) {
			enc_v1	= enc_read(motor_no) ;
		}
		else {
			break ;
		}
	}

    enc_v1      = enc_read(motor_no) ;
    pis_v1      = pis_read(motor_no) ;
	duty_cw		= dcm_setup[motor_no].duty_base + (dcm_setup[motor_no].duty_fcw) ;
	duty_ccw	= dcm_setup[motor_no].duty_base + (dcm_setup[motor_no].duty_fccw) ;

	if (pis_v1 == OFF)
    {
		printf("initial pi off\n\r");
		printf("\t\t enc_v: %d, pi: %d => cw, duty: %d\n", enc_v1, pis_v1, duty_cw) ;


		ret=cw_movent_check(motor_no,duty_ccw,duty_cw);


		// motor를 움직여서, 3초이내에 encoder 3이상의 차이가 나면,
		// motor가 제대로 움직이고, encoder값이 정상으로 들어오고 있다는 의미이다.
        if (ret == FALSE) {
        	printf("x cw movement check error\n\r");
	        goto time_expired ;
        }
		else {
			DCM_brake(motor_no) ;
			usleep(250000);
		}

        enc_v2      = enc_read(motor_no) ;
        pis_v2      = pis_read(motor_no) ;

        if (enc_v1 > enc_v2)	// 제래로 움직였다.
          {
        	printf("initial encoder check ok.\n\r");
		    if (pis_v2 == OFF)
            {
				// 아직도 PI가 OFF 이면 계속 CCW로 돌려서, PI가 ON이 될때까지 간다.
				printf("\t\t enc_2: %d, pi: %d => cw, duty: %d\n", enc_v2, pis_v2, duty_cw) ;
				ret=cw_find_pi_on(motor_no,duty_ccw,duty_cw);

                if (ret == FALSE) {
                		printf("x ccw find pi on  error\n\r");
			            goto time_expired ;
                }
                else {
					// PI를 찾았으면, 다시 피검자쪽으로 CW방향으로 돌려서..
					// PI가 벗어나기 시작할때, Encoder값을 읽어야 한다.
					DCM_brake(motor_no) ;
					usleep(250000);

					enc_v2      = enc_read(motor_no) ;
					pis_v2      = pis_read(motor_no) ;

					printf("\t\t moving to pi off (ccw), duty: %d\n", duty_ccw) ;

					ret=ccw_find_pi_off(motor_no,duty_ccw,duty_cw);

                    if (ret == FALSE) {

                        goto time_expired ;
                    }
                    else {
						DCM_brake(motor_no) ;

                        goto center_found ;
                    }
                }
            }
            else
            {
                // 50ms 안에, PI 를 지나쳤으므로, CCW 로 PI 가 OFF 될때까지 돌리면 된다.
				printf("\t\t moving to pi off (ccw), duty: %d\n", duty_ccw) ;

				ret=ccw_find_pi_off(motor_no,duty_ccw,duty_cw);

                if (ret == FALSE) {
                	printf("x ccw find pi off  error\n\r");

                    goto time_expired ;
                }
                else {
					DCM_brake(motor_no) ;

	                goto center_found ;
                }
            }
        }
        else
        {
			printf("\t\t => enc_v1 > enc_v2 : incorrect position (Z link position is opposite side)\n");
            motor_move(motor_no, OFF);
			goto time_expired ;
        }
    }
    else
    {
		// PI 가 ON(가려졌음) 라는 것은 사용자쪽에 Stage가 있다는 뜻이다.
		// Z motor를 CW 로 돌려서 피검자측으로 보내야 한다.
		printf("\t\t enc_1: %d, pi: %d => ccw, duty: %d\n", enc_v1, pis_v1, duty_ccw) ;

		ret=ccw_movent_check(motor_no,duty_ccw,duty_cw);

        if (ret == FALSE) {
        	printf("x ccw movemenct check error\n\r");
		     goto time_expired ;
        }
		else {
			DCM_brake(motor_no) ;
			usleep(250000);
		}

        enc_v2      = enc_read(motor_no) ;
        pis_v2      = pis_read(motor_no) ;

       if (enc_v1 < enc_v2)

        {
            if (pis_v2 == ON)
            {
				// PI가 계속 ON이면, 조금 더 보내다가..
				// PI가 OFF 가 될때, Encoder를 읽도록 한다.
				printf("\t\t enc_2: %d, pi: %d => ccw, duty: %d\n", enc_v2, pis_v2, duty_ccw) ;

				ret=ccw_find_pi_off(motor_no,duty_ccw,duty_cw);

                if (ret == FALSE) {
                	printf("x cw find pi off check error\n\r");
			        goto time_expired ;
                }
                else {
					DCM_brake(motor_no) ;
					goto center_found ;
                }
            }
            else
            {
				// PI가 OFF 이면, 중간점을 지나갔음.
				// CCW로 돌려서, PI를 ON 될때까지 다시 움직여야 한다.
				printf("\t\t moving to pi on (cw), duty: %d\n", duty_cw) ;

				ret=cw_find_pi_on(motor_no,duty_ccw,duty_cw);

                if (ret == FALSE) {
                	printf("x cw find pi on check error\n\r");
					goto time_expired ;
                }
                else {
					DCM_brake(motor_no) ;
					usleep(250000);

					enc_v2      = enc_read(motor_no) ;
					pis_v2      = pis_read(motor_no) ;

					// PI가 ON이 될때까지 왔으므로, CW로 돌려서 PI가 OFF 되는 시점에서 ENCODER를 읽는다.
					printf("\t\t moving to pi off (ccw), duty: %d\n", duty_ccw) ;

					ret=ccw_find_pi_off(motor_no,duty_ccw,duty_cw);

                    if (ret == FALSE) {
                    	printf("x ccw find pi off check error\n\r");
					    goto time_expired ;
                    }
                    else {
						DCM_brake(motor_no) ;

						goto center_found ;
                    }
                }
            }
        }
        else {
			printf("\t\t => enc_v1 > enc_v2 : incorrect position (Z link position is opposite side)\n");
            motor_move(motor_no, OFF);
		//	sprintf(dbg_msg1, "inv:%d,%d,%d,%d(cw)", enc_v1, pis_v1, enc_v2, pis_v2) ;
			goto time_expired ;
        }
    }

center_found:
    dcm_stat[motor_no].center_enc   = enc_read(motor_no) ; //  - 14 ;
	dcm_stat[motor_no].center_set   = TRUE ;
    DCM_brake(motor_no) ;
	printf("\t\t => center enc_v: %d\n", dcm_stat[motor_no].center_enc) ;
    return TRUE ;

time_expired:
    DCM_stop(motor_no) ;
    printf("\t\t => time expired!\n") ;
    return FALSE ;
}


int DCM_move_center_zh(void)
{
    u16         enc_v1, enc_v2 ;
    int        pis_v1, pis_v2 ;

    int         motor_no ;
	int			duty_ccw, duty_cw ;
	int			cnt ;
	int		ret ;

	printf("Detecting ZH center ...\n") ;

    motor_no    = DCM_ZH ;
    if (dcm_ctrl[motor_no].auto_ctrl == ON) {
		printf("err: auto ctrl\n") ;
        return FALSE ;
    }

	//	Wait until z movement is compeletly stopped.
	///////////////////////////////////////////////////////////////////////
	DCM_brake(motor_no) ;
	enc_v1      = enc_read(motor_no) ;
	for (cnt = 0; cnt < 60; cnt++) {
		usleep(50000);
		if (enc_v1 != enc_read(motor_no)) {
			enc_v1	= enc_read(motor_no) ;
		}
		else {
			break ;
		}
	}

	enc_v1      = enc_read(motor_no) ;
    pis_v1      = pis_read(motor_no) ;
	duty_cw		= dcm_setup[motor_no].duty_base + (dcm_setup[motor_no].duty_fcw) ;
	duty_ccw	= dcm_setup[motor_no].duty_base + (dcm_setup[motor_no].duty_fccw) ;

	if (pis_v1 == OFF) //z stage is in patient side
    {

		printf("\t\t enc_1: %d, pi: %d => moving to user side, duty: %d\n", enc_v1, pis_v1, duty_ccw) ;
		// motor를 움직여서, 3초이내에 encoder 3이상의 차이가 나면,
		// motor가 제대로 움직이고, encoder값이 정상으로 들어오고 있다는 의미이다.
		ret=ccw_movent_check(motor_no,duty_ccw,duty_cw);

		if (ret == FALSE) {
			goto time_expired ;
		}
		else {
			DCM_brake(motor_no) ;
			usleep(250000);
		}

        enc_v2      = enc_read(motor_no) ;
        pis_v2      = pis_read(motor_no) ;

        if (enc_v1 > enc_v2)	// 'new xz
        {
            if (pis_v2 == OFF)
            {
				// 아직도 PI가 OFF 이면 계속 CCW로 돌려서, PI가 ON이 될때까지 간다.
				printf("\t\t enc_2: %d, pi: %d => moving to user side, duty: %d\n", enc_v2, pis_v2, duty_ccw) ;

				ret=ccw_find_pi_on(motor_no,duty_ccw,duty_cw);

                if (ret == FALSE) {
			        goto time_expired ;
                }
                else {
					// PI를 찾았으면, 다시 피검자쪽으로 CW방향으로 돌려서..
					// PI가 벗어나기 시작할때, Encoder값을 읽어야 한다.
					DCM_brake(motor_no) ;
					usleep(250000);

					enc_v2      = enc_read(motor_no) ;
					pis_v2      = pis_read(motor_no) ;
					printf("\t\t moving to pi off (moving to patient side), duty: %d\n", duty_cw) ;

					ret=cw_find_pi_off(motor_no,duty_ccw,duty_cw);

                    if (ret == FALSE) {
					    goto time_expired ;
                    }
                    else {
						DCM_brake(motor_no) ;
					    goto center_found ;
                    }
                }
            }
            else
            {

				printf("\t\t moving to pi off (cw), duty: %d\n", duty_cw) ;

				ret=cw_find_pi_off(motor_no,duty_ccw,duty_cw);

                if (ret == FALSE) {
			        goto time_expired ;
                }
                else {
					DCM_brake(motor_no) ;
			        goto center_found ;
                }
            }
        }
        else
        {
			printf("\t\t enc_v1 > enc_v2 : incorrect position (Z link position is opposite side)\n");
            motor_move(motor_no, OFF);
            goto time_expired ;
        }
    }
    else //stage is in the user side
    {
		// Z motor를 CW 로 돌려서 피검자측으로 보내야 한다.
		printf("\t\t enc_1: %d, pi: %d => cw(moving to patient side), duty: %d\n", enc_v1, pis_v1, duty_cw) ;
		ret=cw_movent_check(motor_no,duty_ccw,duty_cw);

        if (ret == FALSE) {
		     goto time_expired ;
        }
		else {
			DCM_brake(motor_no) ;
			usleep(250000);
		}

        enc_v2      = enc_read(motor_no) ;
        pis_v2      = pis_read(motor_no) ;

        if (enc_v1 < enc_v2)
        {
            if (pis_v2 == ON)
            {
				// PI가 계속 ON이면, 조금 더 보내다가..
				// PI가 OFF 가 될때, Encoder를 읽도록 한다.
				printf("\t\t enc_2: %d, pi: %d => cw, duty: %d\n", enc_v2, pis_v2, duty_cw) ;
			    ret=cw_find_pi_off(motor_no,duty_ccw,duty_cw);

                if (ret == FALSE) {
				      goto time_expired ;
                }
                else {
					DCM_brake(motor_no) ;
				    goto center_found ;
                }
            }
            else
            {
				// PI가 OFF 이면, 중간점을 지나갔음.
				// CCW로 돌려서, PI를 ON 될때까지 다시 움직여야 한다.
				printf("\t\t moving to pi on (ccw), duty: %d\n", duty_ccw) ;

				ret= ccw_find_pi_on( motor_no,duty_ccw,duty_cw);

                if (ret == FALSE) {
				     goto time_expired ;
                }
                else {
					DCM_brake(motor_no) ;
					usleep(250000);

					enc_v2      = enc_read(motor_no) ;
					pis_v2      = pis_read(motor_no) ;

					// PI가 ON이 될때까지 왔으므로, CW로 돌려서 PI가 OFF 되는 시점에서 ENCODER를 읽는다.
					printf("\t\t moving to pi off (cw), duty: %d\n", duty_cw) ;
					ret= cw_find_pi_off(motor_no,duty_ccw,duty_cw);

                    if (ret == FALSE) {
					//	sprintf(dbg_msg3, "exp:%d,%d,%d,%d,%d,%d", enc_v1, pis_v1, enc_v2, pis_v2, enc_read(motor_no), pis_read(motor_no)) ;
                        goto time_expired ;
                    }
                    else {
						DCM_brake(motor_no) ;
					//	sprintf(dbg_msg3, "ok4:%d,%d,%d,%d,%d,%d", enc_v1, pis_v1, enc_v2, pis_v2, enc_read(motor_no), pis_read(motor_no)) ;
                        goto center_found ;
                    }
                }
            }
        }
        else {
			printf("\t\t => enc_v1 > enc_v2 : incorrect position (Z link position is opposite side)\n");
            motor_move(motor_no, OFF);
			//sprintf(dbg_msg3, "inv:%d,%d,%d,%d(cw)", enc_v1, pis_v1, enc_v2, pis_v2) ;
			goto time_expired ;
        }
    }


center_found:
    dcm_stat[motor_no].center_enc   = enc_read(motor_no) ;
    dcm_stat[motor_no].center_set   = TRUE ;
	DCM_brake(motor_no) ;
	printf("\t\t => center enc_v: %d\n", dcm_stat[motor_no].center_enc) ;
    return TRUE ;

time_expired:
    DCM_stop(motor_no) ;
    printf("\t\t => time expired!\n") ;
    return FALSE ;
}

void DCM_move_cw(int motor_no, int duty)
{

	if (dcm_stat[motor_no].on_move == TRUE &&
        dcm_stat[motor_no].on_dir == ROTATE_CCW) {
        DCM_brake(motor_no) ;
    }

	DCM_duty(motor_no, duty) ;


	motor_rotate(motor_no, ROTATE_CW) ;


    DCM_move(motor_no) ;
    return ;
}


///
void DCM_af_xzmove(int motor_no, int dir,int duty)
{
int rdir;
	 switch (motor_no) {

		    case DCM_ZH:
		    	DCMOTZ_PWM_DUTY_BASE_REG	= duty ;
		    	DCMOTZ_NEARENC_REG			= 0;
		    	DCMOTZ_SPEED_NEAR_REG		= 0;
		    	DCMOTZ_FAR_CW_REG			= 0;
		    	DCMOTZ_FAR_CCW_REG			= 0;
		    		rdir =dir;
		    		dcmotor_force_move_flag.z	= TRUE;


		            break ;
		    case DCM_XH:
		    	DCMOTX_PWM_DUTY_BASE_REG	= duty ;
		    	DCMOTX_NEARENC_REG			= 0;
		    	DCMOTX_SPEED_NEAR_REG		= 0;
		    	DCMOTX_FAR_CW_REG			= 0;
		    	DCMOTX_FAR_CCW_REG			= 0;
		    		if(dir==ROTATE_CCW)	rdir= ROTATE_CW; //NEW_XZ reversed direction, for compatibility with PC SW
		    		else rdir= ROTATE_CCW;
		    		dcmotor_force_move_flag.x	= TRUE;
		            break ;
		    }

	  dcm_ctrl[motor_no].rotate_dir       = rdir ;
	  dcm_ctrl_reg[motor_no].b.Motor_Dir     = rdir ;
	  update_motor_ctrl(motor_no) ;
	  motor_move(motor_no, TRUE) ;

	return ;
}

void DCM_move_ccw(int motor_no, int duty)
{
    if (dcm_stat[motor_no].on_move == TRUE &&
        dcm_stat[motor_no].on_dir == ROTATE_CW) {
        DCM_brake(motor_no) ;
    }

	DCM_duty(motor_no, duty) ;
	motor_rotate(motor_no, ROTATE_CCW) ;
    DCM_move(motor_no) ;
    return ;
}

void DCM_move_auto(int motor_no, int targ_enc, int duty)
{
    update_motor_encoder(motor_no, targ_enc) ;
    motor_auto_mode(motor_no, ON) ;

    /*
    if (on_fast == TRUE) {
        DCM_move_fast(motor_no) ;
    }
    else {
        DCM_move_slow(motor_no) ;
    }
    */
    DCM_duty(motor_no, duty) ;
    DCM_move(motor_no) ;
    return ;
}

void DCM_move_auto2(int motor_no, int direct, int targ_enc, int dist_enc)
{
    update_motor_encoder(motor_no, targ_enc) ;
    motor_auto_mode(motor_no, ON) ;
    DCM_duty_auto(motor_no, direct, dist_enc) ;
    DCM_move(motor_no) ;
    return ;
}

void DCM_brake(int motor_no)
{
    if (dcm_stat[motor_no].on_brake == TRUE &&
        dcm_stat[motor_no].on_move == FALSE) {
        return ;
    }
    else {
        dcm_stat[motor_no].on_brake     = TRUE ;
        dcm_stat[motor_no].on_move      = FALSE ;
    }

    motor_move(motor_no, FALSE) ;
    // wait_ms(1) ;
    motor_brake(motor_no, TRUE) ;
    return ;
}

void DCM_duty(int motor_no, int duty)
{



    dcm_ctrl[motor_no].duty_base        = duty ;
    dcm_ctrl[motor_no].speed_far_cw     = 0 ;
    dcm_ctrl[motor_no].speed_far_ccw    = 0 ;
    dcm_ctrl[motor_no].speed_near       = 0 ;

    update_motor_speed(motor_no) ;
    return ;
}

void DCM_duty_auto(int motor_no, int direct, int dist_enc)
{

	int 		duty_base;
	int 		duty_ncw, duty_nccw, enc_near ;


	duty_base		= motor_setup.m_ctrl[motor_no].duty_base ;

	duty_ncw 		= motor_setup.m_ctrl[motor_no].duty_near_cw ;
	duty_nccw 		= motor_setup.m_ctrl[motor_no].duty_near_ccw ;
	enc_near 		= motor_setup.m_ctrl[motor_no].enc_near ;


	dcm_ctrl[motor_no].duty_base 		= duty_base ;
	dcm_ctrl[motor_no].enc_near 		= enc_near ;


	{
		switch (direct) {
		case LEFT:
		case UP:
		// case DC_FORWARD:
		case DC_BACKWARD:
					dcm_ctrl[motor_no].speed_near 		= duty_ncw ;

					break ;
		case RIGHT:
		case DOWN:
		// case DC_BACKWARD:
		case DC_FORWARD:
					dcm_ctrl[motor_no].speed_near 		= duty_nccw ;

					break ;
		}
	}

    if (dist_enc <= dcm_ctrl[motor_no].enc_near) {
    	dcm_stat[motor_no].on_near 		= TRUE ;
    }
    else {
    	dcm_stat[motor_no].on_near 		= FALSE ;
    }

    if (direct == LEFT || direct == UP || direct == DC_BACKWARD) { // direct == DC_FORWARD) {
    	dcm_stat[motor_no].on_dir 		= ROTATE_CW ;
    }
    else {
    	dcm_stat[motor_no].on_dir 		= ROTATE_CCW ;
    }

    update_motor_speed(motor_no) ;
    return ;
}

void DCM_move(int motor_no)
{

    if (dcm_stat[motor_no].on_move == TRUE) {
        return ;
    }
    else {
        dcm_stat[motor_no].on_move  = TRUE ;
    }

    if (dcm_stat[motor_no].on_brake == TRUE) {
        dcm_stat[motor_no].on_brake     = FALSE ;
        motor_brake(motor_no, FALSE) ;
    }


    motor_move(motor_no, TRUE) ;
    return ;
}

void DCM_stop(int motor_no)
{
    if (dcm_stat[motor_no].on_move == FALSE) {
        return ;
    }
    else {
        dcm_stat[motor_no].on_move  = FALSE ;
    }

    motor_move(motor_no, FALSE) ;
    return ;
}

void motor_auto_mode(int motor_no, int flag)
{
    dcm_ctrl[motor_no].enc_ctrl             = flag ;
    dcm_ctrl_reg[motor_no].b.Motor_Enc_con     = flag ;
    dcm_ctrl_reg[motor_no].b.Motor_Auto        = flag ;
    dcm_ctrl[motor_no].auto_ctrl            = flag ;

    update_motor_ctrl(motor_no) ;
    return ;
}

void update_motor_encoder(int motor_no, int targ_enc)
{
    switch (motor_no) {

    case DCM_ZH:
    	DCMOTZ_TARGET_ENC_REG	=  targ_enc& 0x7ff;
            break ;

    case DCM_XH:
    	DCMOTX_TARGET_ENC_REG	=  targ_enc& 0x7ff;
            break ;


    }
    return ;
}
int wait_enc_diff2(int enc_no, int diff, int exp_cnt)
{
	int			i ;
    u16         enc ;

    enc         = enc_read(enc_no) ;

	for (i = 0; i < exp_cnt; i++) {
        if (abs((int)enc - (int)enc_read(enc_no)) >= diff) {
            return TRUE ;
        }
        usleep(10000);
	}

	return FALSE ;
}

int wait_pi_signal2(int enc_no, int pis, int exp_cnt)
{
	int			i ;

	for (i = 0; i < exp_cnt; i++) {
		if (pis_read(enc_no) == pis) {
			return TRUE ;
		}
		usleep(10000);
	}

	return FALSE ;
}
/*
BOOL pis_read(int enc_no)
{
    switch (enc_no) {
    case ENC_X_HEAD:
            return PI_X_HEAD() ;
    case ENC_Z_HEAD:
            return PI_Z_HEAD() ;

    default:
            return FALSE ;
    }
}
*/


//
int pis_read(int enc_no)
{
    switch (enc_no) {
#if 0  //********************************************************
    case ENC_X_HEAD:
            return dc_motor_pi_state(DCMOTX_PI_BIT_POS) ;
    case ENC_Z_HEAD:
            return dc_motor_pi_state(DCMOTZ_PI_BIT_POS) ;
    case CR_LOW_PI:
    		return dc_motor_pi_state(CR_LOW_PI_BIT_POS) ;
    case CR_HIGH_PI:
      		return dc_motor_pi_state(CR_HIGH_PI_BIT_POS) ;
    default:
            return FALSE ;
#endif
    }
}

int atomic_dc_motor_pi_state(int bit_pos)
{
	int phase;
#if 0  //********************************************************
	if ((FpgaRegRd(FPGA_CXYZ_PI_STATUS))&(1<<bit_pos))
	{
			phase	= FALSE;               // PI 가안가려졌다
	}
	else
	{
			phase   =  TRUE;              // PI 가가려졌다.
	}
#endif
	return phase;
}

int dc_motor_pi_state(int bit_pos)
{
	int			cnt = 0 ;
		int			phase ;
		static int	old_v = -1 ;

#if 0  //********************************************************
		if ((FpgaRegRd(FPGA_CXYZ_PI_STATUS))&(1<<bit_pos)) {
			phase	= FALSE;               // PI 가안가려졌다
		}
		else {
			phase   =  TRUE;              // PI 가가려졌다.
		}

		if (old_v != -1 && phase == old_v) {
			return phase ;
		}

		while (1)
		{
			if ((FpgaRegRd(FPGA_CXYZ_PI_STATUS))&(1<<bit_pos)) {		// PI 가안가려졌다
				if (phase == FALSE) {
					cnt++ ;
				}
				else {
					phase	= FALSE ;
					cnt		= 0 ;
				}
			}
			else {							// PI 가가려졌다.
				if (phase == TRUE) {
					cnt++ ;
				}
				else {
					phase	= TRUE ;
					cnt		= 0 ;
				}
			}

			if (cnt > PI_MAX_CNT) {
				break ;
			}
		//	wait_us(1) ; , deleted to use in ISR
		}
#endif
		old_v	= phase ;
		return phase ;
}

void HEAD_move_enc(int direct, int targ_enc)
{
    int         cent_enc, move_enc, dist_enc  ;

    if ((direct & DIR_Z) == DIR_Z)
    {
        if (!motor_center(DCM_ZH, &cent_enc) ||
        	!motor_encoder(DCM_ZH, &move_enc)) {
            return ;
        }

        if (targ_enc > (cent_enc+ENC_HEAD_FRONT_END)) {
            targ_enc    = cent_enc + ENC_HEAD_FRONT_END ;
        }
        if (targ_enc < (cent_enc+ENC_HEAD_REAR_END)) {
            targ_enc    = cent_enc + ENC_HEAD_REAR_END ;
        }

        dist_enc 	= (targ_enc - move_enc) ;
        direct 		= (dist_enc > 0 ? DC_FORWARD : DC_BACKWARD) ;
        dist_enc 	= abs(dist_enc) ;

       // _trace4("\t\t=> ZH: dir: %c, enc: %d => %d\n", (direct == DC_FORWARD ? 'F' : 'B'), move_enc, targ_enc) ;
        DCM_move_auto2(DCM_ZH, direct, targ_enc, dist_enc) ;
    }

    if ((direct & DIR_X) == DIR_X)
    {
        if (!motor_center(DCM_XH, &cent_enc) ||
        	!motor_encoder(DCM_XH, &move_enc)) {
            return ;
        }

        if (targ_enc > (cent_enc+ENC_HEAD_LEFT_END)) {
            targ_enc    = cent_enc + ENC_HEAD_LEFT_END ;
        }
        if (targ_enc < (cent_enc+ENC_HEAD_RIGHT_END)) {
            targ_enc    = cent_enc + ENC_HEAD_RIGHT_END ;
        }

        dist_enc 	= (targ_enc - move_enc) ;
        direct 		= (dist_enc > 0 ? LEFT : RIGHT) ;
        dist_enc	= abs(dist_enc) ;

       // _trace4("\t\t=> XH: dir: %c, enc: %d => %d\n", (direct == LEFT ? 'L' : 'R'), move_enc, targ_enc) ;
        DCM_move_auto2(DCM_XH, direct, targ_enc, dist_enc) ;
    }
    return ;
}

int motor_encoder(int motor_no, int *enc_val)
{
    if (dcm_stat[motor_no].center_set == FALSE) {
        *enc_val    = 0 ;
        if(motor_no==DCM_XH)
        {
        	HBS.AutoStageMotorInfo.Xmotor.EncPos=0;
        }
        else if(motor_no==DCM_ZH)
        {
        	HBS.AutoStageMotorInfo.Zmotor.EncPos=0;
        }
        // _trace2("encoder not found => %d\n", motor_no) ;
        return FALSE ;
    }
    else {
        *enc_val    = (int) enc_read(motor_no) ;
        if(motor_no==DCM_XH)
        {
           	HBS.AutoStageMotorInfo.Xmotor.EncPos=*enc_val;
        }
        else if(motor_no==DCM_ZH)
        {
           	HBS.AutoStageMotorInfo.Zmotor.EncPos=*enc_val;
        }
        return TRUE ;
    }
}


int motor_center(int motor_no, int *cent_enc)
{
    if (dcm_stat[motor_no].center_set == FALSE) {
        *cent_enc   = 0 ;
        // _trace2("center not found => %d\n", motor_no) ;
        return FALSE ;
    }
    else {
        *cent_enc   = dcm_stat[motor_no].center_enc ;
		*cent_enc	+= dcm_setup[motor_no].enc_offs ;
		return TRUE ;
    }
}

void dcm_cr_ctrl(unsigned cpu_ctrl)
{
	dcm_ctrl_reg[DCM_CR].byte					= DCM_MOTOR_DEF_VAL ;
	dcm_ctrl_reg[DCM_CR].b.Motor_Auto			= OFF;
	dcm_ctrl_reg[DCM_CR].b.Motor_Break			= OFF;
	//OCT_motor[DCM_CR].b.Motor_Con			= LOW_SIDE;
	dcm_ctrl_reg[DCM_CR].b.Motor_Con			= CR_LOW_SIDE;
	dcm_ctrl_reg[DCM_CR].b.Motor_Dir			= ROTATE_CW;	// counterclockwise
	dcm_ctrl_reg[DCM_CR].b.Motor_On			= OFF;
	dcm_ctrl_reg[DCM_CR].b.Motor_Start			= cpu_ctrl; // currently CTRL_SRC
	dcm_ctrl_reg[DCM_CR].b.Motor_Enc_con		= OFF;

    return ;
}

#define CR_MOVE_TIMEOUT 20000 //in ms: 20sec

int MoveHeadRestLowLimit(void)
{
	int time_out;
	int pi_state;


	if(pis_read(CR_LOW_PI)) return TRUE;
	dcm_cr_ctrl(ON);
	time_out=0;
	DCM_move_cw(DCM_CR,220);
	while((pi_state=pis_read(CR_LOW_PI))==FALSE)
	{
//		uart_printf("moving CR cnt=%d\n\r",time_out);
		if(time_out>CR_MOVE_TIMEOUT) return FALSE;
		usleep(1000);
		time_out++;
	}
	DCM_brake(DCM_CR);
	dcm_cr_ctrl(OFF);
	update_motor_ctrl(DCM_CR);
	return TRUE;

}

BOOL MoveHeadRestHighLimit(void)
{
	int time_out;
	BOOL pi_state;


	if(pis_read(CR_HIGH_PI)) return TRUE;
	dcm_cr_ctrl(ON);
	time_out=0;
	DCM_move_ccw(DCM_CR,220);
	while((pi_state=pis_read(CR_HIGH_PI))==FALSE)
	{
	//	uart_printf("moving CR cnt=%d\n\r",time_out);
		if(time_out>CR_MOVE_TIMEOUT) return FALSE;
		usleep(1000);
		time_out++;
	}
	DCM_brake(DCM_CR);
	dcm_cr_ctrl(OFF);
	update_motor_ctrl(DCM_CR);
	return TRUE;
}

void CR_move(char dir)
{
	if(dir==CR_MOVE_UP)
	{
		if(pis_read(CR_HIGH_PI))
		{
			DCM_brake(DCM_CR);
			dcm_cr_ctrl(OFF);
			update_motor_ctrl(DCM_CR);
			printf(" cr move high limit pI\n\r");
			return ;
		}
		dcm_cr_ctrl(ON);
		DCM_move_ccw(DCM_CR,220);
	}
	else
	{
		if(pis_read(CR_LOW_PI))
		{
			DCM_brake(DCM_CR);
			dcm_cr_ctrl(OFF);
			update_motor_ctrl(DCM_CR);
			printf(" cr move low limit pI\n\r");
			return;
		}

		dcm_cr_ctrl(ON);
		DCM_move_cw(DCM_CR,220);
	}

	//uart_printf(" cr move dir=%d\n\r",dir);
	return;
}

void CR_stop(void)
{
	DCM_brake(DCM_CR);
	dcm_cr_ctrl(OFF);
	update_motor_ctrl(DCM_CR);
}

void cr_limit_check(void)
{

	if((pis_read(CR_LOW_PI))&&(dcm_ctrl[DCM_CR].rotate_dir ==ROTATE_CW))
		{
	//	uart_printf("Cr low pi, stop in isr\n\r");
		DCM_brake(DCM_CR);
		}
	if((pis_read(CR_HIGH_PI))&&(dcm_ctrl[DCM_CR].rotate_dir ==ROTATE_CCW)){
	//	uart_printf("Cr high pi, stop in isr\n\r");
		DCM_brake(DCM_CR);
	}
}




