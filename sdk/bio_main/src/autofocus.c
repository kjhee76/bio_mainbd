/*
 * OCT_AF.c
 *
 *  Created on: 2016. 4. 20.
 *      Author: Administrator
 */

#include "autofocus.h"
#include "bio_hbs.h"


//#define	AF_DEBUG

JOYSTICK_st joy;
//_OCT_AF		oct_af;

extern	step_m_stat_t	gmotor_stat[MAX_STEP_MOTOR_NUM+2];
extern 	HBS_st			HBS;
/* static functions */
static U32 	get_joy_enc(int *val);
static int	joy_enc2ymotor(U32 enc_diff);
static int 	read_joy_wheel(void);
static int joy_move(void);
#if 0
static void joy_y_sm_holding(void);
#endif

U16 enc_read(int enc_no)
{
	U16		enc_ret = 0;

	switch (enc_no)
	{
#if 0  // ********************************************************
		case ENC_Z_HEAD:
			enc_ret		= FpgaRegRd(FPGA_DCMOTZ_ENCODER_DATA);
			break;
		case ENC_X_HEAD:
			enc_ret		= FpgaRegRd(FPGA_DCMOTX_ENCODER_DATA);
			break;
		case ENC_JOY:
			enc_ret		= FpgaRegRd(JOYSTICK_ENCODER_DATA);
			break;
		default:
			break;
#endif
	}

	return enc_ret;
}

int joy_enc_init(void)
{
	int enc_val;

	if (get_joy_enc(&enc_val) == FALSE) {
		return -1;
	}
	if(joy.enc_init == FALSE)
	{
		joy.enc_prev	= enc_val ;
		joy.prev_speed   = 0;
		joy.move_speed   = 0;
		joy.enc_init	= TRUE ;
	}
	return 0;
}

void joystick_enable(int enable)
{
	joy.enable = enable;
}

void joystick_ctrl(void)
{
	if(joy.enable)
	{
		if (read_joy_wheel() != 0)
		{
#if 0
			if( is_af_y_move() )
				clear_af_y_move();
#endif
			joy_move() ;
		}
#if 0 // *********************************************
		else if((gmotor_stat[Y_AXIS_MOTOR].joy_move_flag == TRUE)
		//	    && (HBS.AutoStageMotorInfo.Ymotor.LimitSensorStatus[0] == FALSE)
		//		&& (HBS.AutoStageMotorInfo.Ymotor.LimitSensorStatus[1] == FALSE)
				)
		{
			set_sm_stop(&gmotor_stat[Y_AXIS_MOTOR]);
		}
#endif
	}
}

static int read_joy_wheel(void)
{
	int 	enc_val, enc_diff ;
	int 	dir;
	int 	enc_speed, move_speed;

	if (joy.enable == FALSE)	return 0 ;
	if (get_joy_enc(&enc_val) == FALSE)		return 0;

	enc_diff 	= enc_val - joy.enc_prev ;
	if (enc_diff < -1000 || enc_diff > 1000)
	{
		enc_diff	= 0 ;
	}

	joy.enc_prev 	= enc_val ;
	joy.prev_speed  = joy.move_speed;
	joy.prev_dir  	= joy.move_dir;

	if( enc_diff != 0) dir	= (enc_diff>0?-1:+1);
	enc_speed	= dir * joy_enc2ymotor(abs(enc_diff));

	if( HBS.AutoStageMotorInfo.Ymotor.LimitSensorStatus[0] == TRUE)
	{
		if(enc_speed <0) enc_speed = 0;
	}
	else if( HBS.AutoStageMotorInfo.Ymotor.LimitSensorStatus[1] == TRUE)
	{
		if(enc_speed > 0) enc_speed = 0;
	}

	joy.pp_enc_speed	= joy.p_enc_speed;
	joy.p_enc_speed		= enc_speed;

/* Speed control : shuld not roll over  2 speed grade */
	if((enc_speed != 0)||(abs(joy.p_enc_speed) > 1))
	{
		if (enc_speed == joy.prev_speed) move_speed = joy.prev_speed;
		else if (enc_speed > joy.prev_speed) move_speed = joy.prev_speed + 1;
		else if (enc_speed < joy.prev_speed) move_speed = joy.prev_speed - 1;
	}
	else
	{
		if(abs(joy.p_enc_speed) == 1) move_speed = joy.p_enc_speed;
		else if((abs(joy.p_enc_speed) == 0)&&(abs(joy.pp_enc_speed) == 1)) move_speed = joy.pp_enc_speed;
		else move_speed = 0;
	}

#if defined (AF_DEBUG)
	uart_printf("\n\tenc_diff=%d (%d,%d,%d) dy_stepmotor_speed %d  => %d \n",enc_diff, joy.pp_enc_speed, joy.p_enc_speed, enc_speed, joy.prev_speed, joy.move_speed);
#endif

	joy.move_dir = dir;
	joy.move_speed = move_speed;

	return joy.move_speed;
}
#if 0
static int read_joy_wheel(void)
{
	int 	enc_val, enc_diff ;
	int 	pre_diff;

	if (joy.enable == FALSE)	return 0 ;
	if (get_joy_enc(&enc_val) == FALSE)		return 0;

	pre_diff    = joy.enc_diff;

	enc_diff 	= enc_val - joy.enc_prev ;
	if (enc_diff < -1000 || enc_diff > 1000) {
//		new_diff 	= 0 ;
		enc_diff	= 0 ;
	}

	joy.enc_diff 	= enc_diff ;
	joy.enc_prev 	= enc_val ;
	joy.pre_diff 	= pre_diff ;

	joy.prev_speed   = joy.move_speed;
	joy.pre_move_sign = joy.move_sign;
	if( joy.enc_diff != 0) joy.move_sign	= (joy.enc_diff>0?-1:+1);
//	joy.cur_speed	= joy_enc2ymotor(abs(joy.enc_diff));
	joy.cur_speed	= joy.move_sign * joy_enc2ymotor(abs(joy.enc_diff));

//	joy.move_dir =  joy.move_sign;
if(joy.move_sign >= 0)
{
	if (joy.move_sign * joy.cur_speed == joy.pre_move_sign * joy.prev_speed) joy.move_speed = joy.prev_speed;
	else if (joy.move_sign * joy.cur_speed > joy.pre_move_sign * joy.prev_speed) joy.move_speed = joy.prev_speed + 1;
	else if (joy.move_sign * joy.cur_speed < joy.pre_move_sign * joy.prev_speed) joy.move_speed = joy.prev_speed - 1;
}
else
{
	if (joy.move_sign * joy.cur_speed == joy.pre_move_sign * joy.prev_speed) joy.move_speed = joy.prev_speed;
	else if (joy.move_sign * joy.cur_speed < joy.pre_move_sign * joy.prev_speed) joy.move_speed = joy.prev_speed + 1;
	else if (joy.move_sign * joy.cur_speed > joy.pre_move_sign * joy.prev_speed) joy.move_speed = joy.prev_speed - 1;
}
	#if 0
//	uart_printf("\n=========================================================================================");
	uart_printf("\n\tenc_diff=%d y_stepmotor_speed %d , %d => %d \n",joy.enc_diff, joy.prev_speed, joy.cur_speed,joy.move_speed);
//	uart_printf("\n=========================================================================================");
	#endif
	//	if (enc_diff != 0) {
//		uart_printf("\n\t==>enc_val: %d , joystick diff: %d => %d\n", enc_val, enc_diff, new_diff) ;
//	}
	/*
	if((joy.pre_move_sign != joy.move_sign) && (joy_move_speed > 1))
	{
		joy.move_speed = joy.prev_speed - 1;
		return 0;
	}
	*/
	joy.move_dir = (joy.move_speed < 0) ? -1 : 1;
	return joy.move_speed;
}
#endif

#if 0
static void joy_y_sm_holding(void)
{
//	STEP_MOTOR_TORQUE_LOW(Y_AXIS_MOTOR);
	step_m_torque_set(Y_AXIS_MOTOR, TORQUE_HOLDING);
}
#endif

#define Y_AXIS_MOTOR 10

static int joy_move(void)
{
//	int diff;
#if 0
	if( is_sm_stop_cmd(&gmotor_stat[Y_AXIS_MOTOR]))
	{
		clear_sm_stop(&gmotor_stat[Y_AXIS_MOTOR]);
	}
#endif
	if ( joy.move_speed != 0 )
	{
//		gmotor_stat[Y_AXIS_MOTOR].joy_move_flag = TRUE;
//		StepMotorMove_JOY(Y_AXIS_MOTOR,joy.move_speed, joy.prev_speed, joy.move_dir,5000);
//		joy.move_last = abs(diff);
//		joy.prev_dir		= joy.move_sign;
//		joy.prev_speed	= joy.move_speed;
		 return TRUE;
	}
	else
	{
//		joy.prev_speed		= 0;
//		joy.chattering_cnt	= 0;
		return FALSE;
	}
}

static int	joy_enc2ymotor(U32 enc_diff)
{
#if 0
	if( enc_diff == 0)							return	0;
	else if( enc_diff >= 1 &&  enc_diff <= 3)	return	1;
	else if( enc_diff >= 4 &&  enc_diff <= 4)	return	2;
	else if( enc_diff >= 5 &&  enc_diff <= 5)	return	3;
	else if( enc_diff >= 6 /*&&  enc_diff <= 200*/)	return	4;
#endif

	if( enc_diff < 4)
		return enc_diff;
	else
	return 4;
}

static U32 get_joy_enc(int *val)
{
	*val	= enc_read(ENC_JOY) ;
	return 1;
}

#if 0
void oct_af_init(void)
{
	oct_af.af_on				= TRUE;
	oct_af.af_x_on				= FALSE;
	oct_af.af_x_move			= FALSE;
	oct_af.af_y_on				= TRUE;
	oct_af.af_y_move			= FALSE;
	oct_af.af_z_on				= FALSE;
	oct_af.af_z_move			= FALSE;
	oct_af.af_fundus_focus_on	= FALSE;
	oct_af.af_oct_focus_on		= FALSE;
	oct_af.af_oct_z_on			= FALSE;
}

int	is_oct_af(void)
{
	return oct_af.af_on;
}
int	is_oct_af_y(void)
{
	return oct_af.af_y_on;
}

void	set_af_y_move(void)
{
	oct_af.af_y_move	= TRUE;
}

int		is_af_y_move(void)
{
	return	oct_af.af_y_move;
}
void	clear_af_y_move(void)
{
	oct_af.af_y_move	= FALSE;
}
int	af_y_ctrl(void)
{
	int		cnt;
	float	af_sim ;

	if( is_oct_af() != TRUE ) 		return FALSE;
	if( is_oct_af_y() != TRUE ) 	return FALSE;

	for(cnt = 0 ;cnt < 10; cnt++)
	{
		if( cnt < 5 )	af_sim = -4.0;
		else			af_sim = +4.0;

		uart_printf("\n\t==>AF move (%.1f) pixel",af_sim);
		af_y_move(TRUE, af_sim);
		uart_printf("\n\t\t==>wait 500ms");
		wait_ms(500);
	}

	return TRUE;
}

int	af_y_move(int af_on, float diff_pixel)
{
	float	metric_distance;
	int		pulse_distance;
	int		af_dir;
	int		af_pulse;

	if( af_on == FALSE )
		return TRUE;

	metric_distance	= PIXEL2METRIC * diff_pixel;
	pulse_distance	= Y_AXIS_MM2PULSE(metric_distance);
	af_pulse	= abs(pulse_distance);
	if( af_pulse == 0 )
		return FALSE;

	if(diff_pixel >= 0)	af_dir = -1;
	else				af_dir = +1;

	joy_history_clear(&joy);
	set_af_y_move();
	StepMotorMove_Y_AF(Y_AXIS_MOTOR, 4, af_pulse, af_dir);

	return TRUE;
}
#endif
