
#ifndef	_LED_H__
#define	_LED_H__

#define LED_MIN_INTENSITY 5
#define 	AD5308_REF_MOT_CH 	0x7   // VOUTH

short int AD5308DataWrite(u16 ch, u16 Data);
u16 SetLED(u8 LED_ch, u16 brightness);
#endif
