/*
 * OCT_Track.h
 *
 *  Created on: 2016. 4. 27.
 *      Author: HUVITZ
 */

#ifndef __TRACK_H_
#define __TRACK_H_

//#include "OCT_Def.h"
#include "dc_motor.h"

//	Constant Definition
//PID controller gain setting

#define MAX_PWM_DUTY 200
#define MAX_PWM_DUTY_ZENC 200
#define MAX_PWM_DUTY_XENC 200

#define KP_X_ENC 25 //encoder control P gain
#define KI_X_ENC 0//encoder control I gain
#define KP_Z_ENC 25 //encoder control P gain
#define KI_Z_ENC 0//encoder control I gain

#define MAX_KP_X 20
#define MIN_KP_X 0.2
#define MAX_KI_X 2
#define MIN_KI_X 0
#define DEFAULT_KP_X 3
#define DEFAULT_KI_X 0.01

//temporary structure mapping
//
// kp_cw:  duty-fast-cw/1000, default value:2
// kp_ccw: duty-fast-ccw/1000, --reserved
// ki_cw:  duty near_cw/1000, default value:0.1
// ki_ccw: duty near_ccw/1000, --reserved

#define MAX_KP_Z 20
#define MIN_KP_Z 0.2
#define MAX_KI_Z 2
#define MIN_KI_Z 0
#define DEFAULT_KP_Z 2
#define DEFAULT_KI_Z 0.01

//////////////////////////////////////////////////////////////////////

#define ENC_XL_END 				+30
#define ENC_XR_END				-30
#define ENC_ZR_END 				-30
#define ENC_ZF_END 				+30

#define ENC_MAX_ZF_END          +60
#define ENC_MAX_ZR_END          -60
#define ENC_MAX_XR_END			-60
#define ENC_MAX_XL_END          +60

#define REF_SS_CNTX				20
#define REF_SS_CNTZ				20

#define X_SS_ERROR				2.0
#define Z_SS_ERROR				2.0


/*
#define ENC_XL_END 				-100
#define ENC_XR_END				+100
#define ENC_YU_END 				+90
#define ENC_YD_END 				-70
#define ENC_ZR_END 				-100
#define ENC_ZF_END 				+100
*/
//	Structure Declaration
//////////////////////////////////////////////////////////////////////
typedef struct _track_stat_t_
{
	volatile int 		on_xmove_enc ;
	volatile int 		on_zmove_enc ;
	volatile int 		on_xmove;
	volatile int 		on_zmove;
	volatile int 		enable ;

	int	ss_cntx;
	int ss_cntz;

	volatile int		enc_x_pre ;
	volatile int		enc_z_pre ;
	volatile int		enc_x_dst ;
	volatile int		enc_z_dst ;
	volatile int		enc_x_cmd_dst;
	volatile int		enc_z_cmd_dst;

	int 		xdir ;
	int 		zdir ;

} track_stat_t ;


extern track_stat_t			track_stat ;
extern dcmotor_force_move_flag_t dcmotor_force_move_flag;


//	Function Declaration
//////////////////////////////////////////////////////////////////////
void 	init_track_stat(void) ;
void 	track_start(void) ;
void 	track_pause(void) ;

int		find_x_center(void) ;
int		find_z_center(void) ;

void	save_track_enc(void) ;
void	track_x(float xpos) ;
void	track_z(float zlev) ;

void    pi_control_x(int dest);
void    pi_control_z(int dest);
void    pi_control_x_enc(int dest);
void    pi_control_z_enc(int dest);
void    track_x_pi(int dest);
void    track_z_pi(int dest);
void    track_x_pi_enc(void);
void    track_z_pi_enc(void);
void 	x_end_check(void);
void 	z_end_check(void);
void 	start_x_enc(int dest);
void 	start_z_enc(int dest);

void 	move_x(int dist) ;
void 	move_z(int dist) ;
void 	stop_x(void) ;
void 	stop_z(void) ;

int	move_to_xcent(void) ;
int	move_to_zcent(void) ;

int 	get_track_xpos(void) ;

int 	get_track_zpos(void) ;
int		get_track_xenc(void) ;
int		get_track_zenc(void) ;
int		get_track_xduty(void) ;
int		get_track_zduty(void) ;

int 	is_XL_end(void) ;
int 	is_XR_end(void) ;
int 	is_ZR_end(void) ;
int 	is_ZF_end(void) ;
int	is_X_center(void) ;
int	is_Z_center(void) ;

int	moving_XL(void) ;
int	moving_XR(void) ;
int	moving_ZR(void) ;
int	moving_ZF(void) ;


#endif /* __TRACK_H_ */
