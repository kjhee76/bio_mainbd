/*
 * OCT_SysVar.h
 *
 *  Created on: 2013. 1. 15.
 *      Author: HUVITZ
 */

#ifndef _SYSVAR_H_
#define _SYSVAR_H_

#define INIT_CPU_BIT 		0x1
#define INIT_SDRAM_BIT 		(0x1<<1)
#define INIT_FPGA_BIT 		(0x1<<2)
#define INIT_OCT_F_M_BIT 	(0x1<<3)  //OCT Focus motor
#define INIT_OCT_REF_M_BIT 	(0x1<<4)  //OCT REF motor
#define INIT_OCT_POL_M_BIT 	(0x1<<5)  //OCT POL motor
#define INIT_FUN_F_M_BIT	(0x1<<6)  //FUNDUS Focus motor
#define INIT_OCT_R_M_BIT 	(0x1<<7)  //OCT Quick Return Mirror IN/OUT Motor
#define INIT_SPLIT_F_M_BIT 	(0x1<<8)  //OCT split focus motor  IN/OUT Motor
#define INIT_DIOPT_M_BIT 	(0x1<<9)  //FUNDUS Diopter Compensation Lens Select Motor
#define INIT_PUPIL_M_BIT 	(0x1<<10)  //PUPIL MASK Select Motor
#define INIT_ODCL_M_BIT 	(0x1<<11)  //OCT Diopter Compensation Lens Select Motor
#define INIT_X_AXIS_BIT 	(0x1<<12)  //X motor
#define INIT_Y_AXIS_BIT 	(0x1<<13)  //Y motor
#define INIT_Z_AXIS_BIT 	(0x1<<14)  //Z motor
#define INIT_CHIN_M_BIT 	(0x1<<15)  //CHIN motor
#define INIT_GALVOX_BIT 	(0x1<<16)
#define INIT_GALVOY_BIT 	(0x1<<17)
#define INIT_C_IRCAM_BIT  	(0x1<<18)
#define INIT_R_IRCAM_BIT  	(0x1<<19)
#define INIT_SLD_BIT    	(0x1<<20)
#define INIT_EEPROM_BIT   	(0x1<<21)
#define INIT_LCD_BIT	  	(0x1<<22)


#define SYS_CPU_DEV_ID		0
#define SYS_SDRAM_DEV_ID	1
#define SYS_FPGA_DEV_ID		2
#define SYS_FMOTOR_DEV_ID	3
#define SYS_REFMOT_DEV_ID	4
#define SYS_POLMOT_DEV_ID	5
#define SYS_CORMOT_DEV_ID	6
#define SYS_SCANMOT_DEV_ID	7
#define SYS_CHINMOT_DEV_ID	8
#define SYS_GALVOX_DEV_ID	9
#define SYS_GALVOY_DEV_ID	10
#define SYS_IRCAM_DEV_ID	11	
#define SYS_SLD_DEV_ID		12	
#define SYS_FRAM_DEV_ID		13

#define FRAM_INIT_ERR_CODE 1
#define FRAM_LOAD_ERR_CODE 2




#define SYS_INIT_DONE 2
#define SYS_INIT_FAIL 1
#define SYS_INIT_PROGRESS 0

enum BOOT_MODE{
		NORMAL,
		INIT_SKIP,
		OPT_ALIGN
	};


#endif /* _SYSVAR_H_ */
