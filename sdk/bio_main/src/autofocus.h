/*
 * OCT_AF.h
 *
 *  Created on: 2016. 4. 20.
 *      Author: Administrator
 */
#include "def.h"

#ifndef _AUTOFOCUS_H_
#define _AUTOFOCUS_H_

#define ENC_X_HEAD 					2
#define ENC_Z_HEAD					1
#define CR_LOW_PI					4
#define CR_HIGH_PI					3
#define ENC_JOY						0

#define PI_MAX_CNT             10
#define JOY_MOVE_CNT				9		// 3		// 1		// 3
#define JOY_STOP_CNT				2

#define INTERVAL_JOY_CHECK		TIMER64P3_12_TIMER_1US_CNT*1000 //10ms

//	Constant Definition
//////////////////////////////////////////////////////////////////////

//#define Y_MOTOR                     0
#define DCM_YB                      Y_MOTOR

int 	joy_enc_init(void);
U16 	enc_read(int enc_no);
void 	joystick_ctrl(void);
void	joy_history_clear(JOYSTICK_st *joy);
void	joy_push(int dir, int speed, JOYSTICK_st *joy);
int		joy_move_determine(int dir, int speed, JOYSTICK_st *joy);
void 	joy_y_sm_holding(void);
void 	joystick_enable(int enable);

void 	oct_af_init(void);
int		is_oct_af(void);
int		is_oct_af_y(void);
void	set_af_y_move(void);
int		is_af_y_move(void);
void	clear_af_y_move(void);
int		af_y_ctrl(void);
int		af_y_move(int af_on, float diff_pixel);
extern	JOYSTICK_st joy;
#endif /* _AUTOFOCUS_H_ */
