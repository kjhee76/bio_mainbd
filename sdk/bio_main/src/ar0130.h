
#ifndef __AR0130_H__
#define __AR0130_H__

#include "xiicps.h"

/************************** Constant Definitions ******************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define IIC_DEVICE_ID		XPAR_XIICPS_0_DEVICE_ID

/*
 * The slave address to send to and receive from.
 */
#define IIC_SLAVE_ADDR		0x10
#define IIC_SCLK_RATE		100000

XIicPs Iic;				/* Instance of the IIC Device */

#define MAX_CAMERA_COUNT 1


#define V4L2_MBUS_FMT_SGRBG12_1X12 0
#define V4L2_FIELD_NONE 0
#define V4L2_COLORSPACE_SRGB 0

#define AR0130_ROW_START		0x01
#define	AR0130_ROW_START_MIN		0
#define	AR0130_ROW_START_MAX		1280
#define	AR0130_ROW_START_DEF		0
#define AR0130_COLUMN_START		0x02
#define	AR0130_COLUMN_START_MIN		0
#define	AR0130_COLUMN_START_MAX		960
#define	AR0130_COLUMN_START_DEF		0
#define AR0130_WINDOW_HEIGHT		0x03
#define	AR0130_WINDOW_HEIGHT_MIN	360
#define	AR0130_WINDOW_HEIGHT_MAX	960
#define	AR0130_WINDOW_HEIGHT_DEF	960
#define AR0130_WINDOW_WIDTH		0x04
#define	AR0130_WINDOW_WIDTH_MIN		640
#define	AR0130_WINDOW_WIDTH_MAX		1280
#define	AR0130_WINDOW_WIDTH_DEF		1280

#define AR0130_PIXEL_ARRAY_WIDTH	1280
#define AR0130_PIXEL_ARRAY_HEIGHT	960

#define MAX_WIDTH   		1280
#define MAX_HEIGHT  		960
#define VGA_WIDTH		640
#define VGA_HEIGHT		480
#define ENABLE			1
#define DISABLE			0

#define AR0130_CHIP_ID 		0x2402
#define AR0130_RESET_REG 	0x301A
#define AR0130_STREAM_ON	0x10DC
#define AR0130_STREAM_OFF	0x10D8
#define AR0130_SEQ_PORT		0x3086
#define AR0130_TEST_REG		0x3070
#define	AR0130_TEST_PATTERN	0x0000

enum {
	AR0130_COLOR_VERSION,
	AR0130_MONOCHROME_VERSION,
};

struct v4l2_rect {
	s32	left;
	s32 top;
	u32 width;
	u32 height;
};

struct v4l2_control {
	s32 id;
	s32 value;
};

/**
 * struct v4l2_mbus_framefmt - frame format on the media bus
 * @width:      frame width
 * @height:     frame height
 * @code:       data format code (from enum v4l2_mbus_pixelcode)
 * @field:      used interlacing type (from enum v4l2_field)
 * @colorspace: colorspace of the data (from enum v4l2_colorspace)
 * @ycbcr_enc:  YCbCr encoding of the data (from enum v4l2_ycbcr_encoding)
 * @quantization: quantization of the data (from enum v4l2_quantization)
 * @xfer_func:  transfer function of the data (from enum v4l2_xfer_func)
 */
struct v4l2_mbus_framefmt {
	u32	width;
	u32 height;
	u32	code;
	u32	field;
	u32	colorspace;
	u16	ycbcr_enc;
	u16	quantization;
	u16	xfer_func;
	u16	reserved[11];
};

int IicPsInit(u16 DeviceId);

int ar0130_reg_read(u32 camPort, u16 regAddr, u16* rData);
int ar0130_reg_write(u32 camPort, u16 regAddr, u16 wData);
int ar0130_calc_size(u32 request_width, unsigned int request_height);
//int ar0130_reset(U32 camPort, int enable);
int ar0130_pll_enable(u32 camPort);
int ar0130_linear_mode_setup(u32 camPort);
//int ar0130_set_resolution(u32 camPort, U32);
int ar0130_set_autoexposure(u32 camPort, int enable);

int ar0130_init(u32 camPort);
int init_camera(void);
#endif
