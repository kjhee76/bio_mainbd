/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

#include "ar0130.h"
#include "led.h"
#include "bio_hbs.h"
#include "sysVar.h"
#include "dc_motor.h"

#define APP_VERSION		"0.001" // fixation target,IRCAM_INT command added
//u32 				*BufAddrTable		=(u32 *)INT_SRAM_BASE;

HBS_st	HBS;

void init_HBS(void);

int init_refstage(void)
{
	int ret;
	int center_refpos;
	int centerPos;

	ret=DCM_move_center(DCM_REF); //detect center(PI) position
	motor_center(DCM_REF,&center_refpos);
	//HBS.AutoStageMotorInfo.Xmotor.CenterPos=(S16)(center_xpos);
	centerPos = (S16)(center_refpos);

	if(ret==TRUE)
	{

//		ret=move_to_refcent();
		if(ret==TRUE)
		{
//			HBS.InitStat.DevStaus|= INIT_X_AXIS_BIT;

		}

		return 0;
	}

	return 1;


}

int init_xstage(void)
{
	int ret;
	int center_xpos;
	ret=DCM_move_center(DCM_XH); //detect center(PI) position
	motor_center(DCM_XH,&center_xpos);
	HBS.AutoStageMotorInfo.Xmotor.CenterPos=(S16)(center_xpos);

	if(ret==TRUE)
	{

		ret=move_to_xcent();
		if(ret==TRUE)
		{
			HBS.InitStat.DevStaus|= INIT_X_AXIS_BIT;

		}

		return 0;
	}

	return 1;


}

int init_zstage(void)
{
	int ret;
	int center_zpos;

	ret=DCM_move_center(DCM_ZH) ;
	motor_center(DCM_ZH,&center_zpos);
	HBS.AutoStageMotorInfo.Zmotor.CenterPos=(S16)(center_zpos);

	if(ret==TRUE)
	{
		ret=move_to_zcent();
		if(ret==TRUE)
		{
			HBS.InitStat.DevStaus|= INIT_Z_AXIS_BIT;

		}
	return 0;
	}



	return 1;
}

void init_xz_stage(void)
{
	init_xstage();
	init_zstage();
}

int main()
{
    init_platform();

    print("AR0130 TEST Program ******\n");

    IicPsInit(IIC_DEVICE_ID);

    ar0130_init(0);

    printf("AD5308 DAC TEST ******\n");

    SetLED (AD5308_REF_MOT_CH, 50);  //1.25V


    init_HBS();//initialize HBS
    init_dc_motors();

    init_xz_stage();

    cleanup_platform();
    return 0;
}

void init_HBS(void)
{
#if 0
	int i;

	strcpy(HBS.SysVer.AppVer, APP_VERSION);

	HBS.SysCfg.UartDebugEn=1;
	HBS.SysCfg.SlewRate =15;
	HBS.SysCfg.AutoSlew=1;
	HBS.SysCfg.ADC_EN=0;
	HBS.SysCfg.StrbCfg.Mode=0;
	HBS.SysCfg.xz_stage_limit_overide=0;
	HBS.SysCfg.StepMotorDiag=0;
	HBS.SysCfg.SLDWarningEn=0;
	HBS.Galvano_Dynamic.offsetX=0;
	HBS.Galvano_Dynamic.offsetY=0;
	HBS.InitStat.DevStaus=INIT_CPU_BIT|INIT_SDRAM_BIT;

	HBS.InitStat.Status=SYS_INIT_PROGRESS;//initialization sequence start.
//	for(i=0;i<MAX_STEP_MOTOR_NUM+2;i++){HBS.SysCfg.MotorWait[i]=step_motor_wait_flag[i];}
//	default_profile.time_step_us=3;

			/* intialize parameter buffer address table*/
	i=0;
	BufAddrTable[i++]= (U32)(&HBS.InitStat);
	BufAddrTable[i++]= (U32)(&HBS.SysVer);
	BufAddrTable[i++]= (U32)(&HBS.SysCfg);
	BufAddrTable[i++]= (U32)(&HBS.traj_profile1[0]);
	BufAddrTable[i++]= (U32)(&HBS.ADC_buffer[0]);
	BufAddrTable[i++]= (U32)(&HBS.DownloadBlkBuf[0]);
	BufAddrTable[i++]= (U32)(&HBS.GP_status);
	BufAddrTable[i++]= (U32)(&HBS.StepMotorInfo[0]);
	BufAddrTable[i++]= (U32)(&HBS.SysCal);
	BufAddrTable[i++]= (U32)(&HBS.Galvano_Dynamic);
	BufAddrTable[i++]= (U32)(&HBS.AutoStageMotorInfo);
	BufAddrTable[i++]= (U32)(&HBS.SysCal2);
	do
	{
		i--;
	//	uart_printf("%d,buf addr ptr addr=%x\n\r",i,(U32)(&BufAddrTable[i]));
		if((BufAddrTable[i]%4)!=0){
			uart_printf("Fatal Error! BufAddrTable[%d]=%08x is not word aligned!!\n",i,BufAddrTable[i]);
			while(1);
		}

	}while(i>0);

		//	uart_printf("APP version =%d.%d\n",(HBS.SysVer.AppVer)>>8,HBS.SysVer.AppVer&0xff);
	uart_printf("HBS InitStat[%x] \n"		,(U32)(&HBS.InitStat));
	uart_printf("HBS SysVer[%x] \n"			,(U32)(&HBS.SysVer));
	uart_printf("HBS SysCfg[%x] \n"			,(U32)(&HBS.SysCfg));
	uart_printf("HBS traj_profile1[%x] \n"	,(U32)(&HBS.traj_profile1[0]));
	uart_printf("HBS ADC_buffer[%x] \n"		,(U32)(&HBS.ADC_buffer[0]));
	uart_printf("HBS download buf[%x] \n"	,(U32)(&HBS.DownloadBlkBuf[0]));
	uart_printf("HBS GP status buf[%x] \n"	,(U32)(&HBS.GP_status));
	uart_printf("motor status buf[%x]\n",(U32)(&HBS.StepMotorInfo[0]));
//	uart_printf("acc table_isr buf[%x] \n",(int)(&gstep_motor_acc_table_isr[0][0][0]));
//	uart_printf("default_profile xbuf[%x]\n",(U32)&(default_profile.posx));
	uart_printf("default_profile ybuf[%x]\n",(U32)&(default_profile.posy));
	uart_printf("HBS traj_profile xbuf[%x] \n"	,(U32)(&HBS.traj_profile1[0].posx));
	uart_printf("HBS traj_profile ybuf[%x] \n"	,(U32)(&HBS.traj_profile1[0].posy));
	uart_printf("HBS SysCal Offset[%x]  \n"		,(U32)(&HBS.SysCal));
	uart_printf("HBS GalvanoDynamic Offset[%x] \n"	,(U32)(&HBS.Galvano_Dynamic));
	uart_printf("HBS AutoStageMotorInfo Offset[%x] \n"	,(U32)(&HBS.AutoStageMotorInfo));
	uart_printf("HBS SysCal2 Offset[%x] \n"	,(U32)(&HBS.SysCal2));
	uart_printf("HBS TOTAL SIZE[%d] Bytes,SysCal Size[%d] bytes \n",sizeof(HBS),sizeof(HBS.SysCal));
#endif
}
