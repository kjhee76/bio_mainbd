/*
 * OCT_Def.h
 *
 *  Created on: 2016. 5. 11.
 *      Author: 
 */

#ifndef _DEF_H_
#define _DEF_H_

//	Default C Standard Header
//////////////////////////////////////////////////////////////////////////
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <float.h>

#define U64			unsigned long long
#define U32			unsigned int
#define U16			unsigned short int	
#define S32			int
#define S16			short int
#define U8			unsigned char
#define S8			char
#define BOOL		int
typedef char bool;

//	Common Control Command 
//////////////////////////////////////////////////////////////////////////

#define ON				1
#define OFF				0 

//	Function Return
//////////////////////////////////////////////////////////////////////////
#define TRUE			1
#define FALSE			0
#define RET_OK			1
#define RET_ERR			0

//	Bit Shifter
//////////////////////////////////////////////////////////////////////////
#define SHIFT_BY_2BIT(bin, shift)	((bin)<<(shift<<1))

#define	MAX_STEP_MOTOR_NUM 9
#define MAX_MOTOR_NUM 12

#define		SM_IDLE_STATE		0
#define		SM_ACC_STATE		1
#define		SM_CONST_STATE		2
#define		SM_DEC_STATE		3

#ifdef	__BIG_ENDIAN
union	U16_Bit
{
	U16	UINT;
	struct
	{
		unsigned b15: 1;			unsigned b14: 1;
		unsigned b13: 1;			unsigned b12: 1;
		unsigned b11: 1;			unsigned b10: 1;
		unsigned b09: 1;			unsigned b08: 1;
		unsigned b07: 1;			unsigned b06: 1;
		unsigned b05: 1;			unsigned b04: 1;
		unsigned b03: 1;			unsigned b02: 1;
		unsigned b01: 1;			unsigned b00: 1;
	}	b;
};
#else
union	U16_Bit
{
	U16	UINT;
	struct
	{
		unsigned b00: 1;			unsigned b01: 1;
		unsigned b02: 1;			unsigned b03: 1;
		unsigned b04: 1;			unsigned b05: 1;
		unsigned b06: 1;			unsigned b07: 1;
		unsigned b08: 1;			unsigned b09: 1;
		unsigned b10: 1;			unsigned b11: 1;
		unsigned b12: 1;			unsigned b13: 1;
		unsigned b14: 1;			unsigned b15: 1;
	}	b;
};
#endif

//	Global Structure Type definition
//////////////////////////////////////////////////////////////////////////

typedef struct step_m_info_t_
{
	S32			sm_dir ;				// step motor direction
	S32			sm_pos_standby ;		// standby position. (+1.5D)
	S32			sm_pos_min ;			// minus end position
	S32			sm_pos_max ;			// plus end position
	S32			sm_pos ;				// plus position
	S32			sm_init_dir;

	S32			sm_pi_side_dir;
	S32			sm_pi_oppisite_dir;

	S32			sm_init_flag;
	S32			sm_default_pos_step;
	S32			sm_init_pos;
	S32			sm_pi_find_pulse;
	S32			sm_pi_escape_pulse;
} step_m_info_t ;

typedef struct _step_m_stat_t_
{
	S32			no_load_speed ;
	S32			slew_speed ;
	S32			constant_speed;
	S32			center_speed ;
	S32			joy_min_speed ;
	S32			joy_low_speed;
	S32			joy_high_speed;
	S32			joy_max_speed;
	S32			init_speed;

	S32			acc_max_step ;
	S32			acc_double_step ;
	S32			acc_type;
	S32			dec_start_step ;

	int			pi_found;
	int			sm_dec_flag;
	unsigned long step_no;
	S32			pi_detect_max;
	U32			one_us_timer_cnt;
	U32			no_load_timer_cnt;
	U32			stop_cmd;
	U32			stop_dec_cnt;
	U32			last_timer_cnt;
	U32			stop_pass_cnt;
	volatile	U32 timer_intr_cnt;
	volatile	U32 timer_acc_step;
	volatile	S32	move_flag; //indicate motor in moving
	S32 		joy_move_flag;
	volatile	S32	sm_constant_timer_cnt;
	volatile	S32	sm_constant_timer_high;
	volatile	S32	sm_constant_timer_low;
	volatile	S32	sm_max_timer_cnt;
	volatile	S32	sm_max_timer_high;
	volatile	S32	sm_max_timer_low;

	S32			sm_inital_timer_cnt;
	S32			sm_initial_timer_high;
	S32			sm_initial_timer_low;

	S32 		sm_joy_min_timer_cnt;
	S32 		sm_joy_low_timer_cnt;
	S32 sm_joy_high_timer_cnt;
	S32 sm_joy_max_timer_cnt;
	U32			sm_hit_low_limit;
	U32			sm_hit_high_limit;
	U32 		last_pi_status;

	volatile	int	sm_current_step;	// 현재 구동되는 구간이 가속 / 정속 / 감속 구간인지에 대한 flag
	volatile	int	sm_acc_step;		// 현재 가속중인 구간에서의 step
	volatile	int	sm_dec_step;		// 현재 감속중인 구간에서의 step
	volatile	int	sm_stop_step;
	volatile	int	sm_move_nowait;
	step_m_info_t	sm_info;

} volatile step_m_stat_t ;

#if 0
typedef struct
{
	U32 		pressed ;
	int 		enc_prev ;
	int			enc_diff ;
	int			pre_diff ;
	U32 		on_move ;
	U32			enc_init ;
	int			move_cnt ;


	int			cur_dir;
	int			prev_dir;
	int			enable;

} JOYSTICK_st ;
#endif

typedef struct
{
	U32 		pressed ;
	int 		enc_prev ;
	int			enc_diff ;
//	int			pre_diff ;
//	U32 		on_move ;
	U32			enc_init ;
//	int			move_cnt ;

//	int			move_sign;
//	int			pre_move_sign;
	int			move_dir;
//	int			move_last;
	int			prev_speed;
	int			p_enc_speed;
	int			pp_enc_speed;
	int 		prev_dir;
//	int			cur_speed;
	int			move_speed;
//	int			joy_history[2][5];
//	int			joy_cnt ;
//	int			chattering_cnt;
//	int			cur_dir;
//	int			prev_dir;
	int			enable;

	U32			joy_speed_low[2];
	U32			joy_speed_high[2];

	U32			joy_speed_low_step;
	U32			joy_speed_high_step;
} JOYSTICK_st ;

#define MAX_DAC_SAMPLE_SIZE	   8192

typedef struct 
{
	float time_step_us;
	U16 sample_size;
	S16 posx[MAX_DAC_SAMPLE_SIZE];
	S16 posy[MAX_DAC_SAMPLE_SIZE];
} linear_profile_st;

typedef struct
{
	U32 IntrCnt;
	U16 IntrFlag;
}SLDintr_st;

typedef struct OCT_AF
{
	int			af_on;
	int			af_x_on;
	int			af_x_move;
	int			af_y_on;
	int			af_y_move;
	int			af_z_on;
	int			af_z_move;
	int			af_fundus_focus_on;
	int			af_oct_focus_on;
	int			af_oct_z_on;
} _OCT_AF ;

typedef struct
{
	U8 row;      //Y position
	U8 column;   //X position
	U8 shapeNo;  // fixation Shape
	S32  color;
}fixation_target_st;

#endif /* _DEF_H_ */
