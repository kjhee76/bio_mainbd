/*
 * OCT_Def.h
 *
 *  Created on: 2016. 5. 11.
 *      Author: 
 */

#ifndef BIO_HBS_H_
#define BIO_HBS_H_

#include "xil_types.h"
#include "def.h"

#define MAX_MOTOR_NUM 5
#define MAX_DAC_SAMPLE_SIZE  1024
#define MAX_TRAJ_NUM  1
#define MAX_STEP_MOTOR_NUM 2
//	Default C Standard Header
//////////////////////////////////////////////////////////////////////////

////////////////HBS structure  

typedef struct
{
	u16 Mode; //0:Strobe mode, 1: continuous mode
	u16 StrbActiveTime; // actual time:16us* StrbActiveTime
	u16 StrbDelayTime; // Strb mode delay(16us*delay)
	u16 reserved;
} StrbCfg_st;// 8byte

typedef struct	//64bytes
{	
	u16 ADC_EN;
	u16 AutoSlew;
	u32 SlewRate;
	u16	UartDebugEn;
	u16 MotorWait[MAX_MOTOR_NUM];
	StrbCfg_st StrbCfg;
	u16 xz_stage_limit_overide;//XZ stage limit overide
	u16 StepMotorDiag; //Step motor diagnostic routine enable.
	u16 SLDWarningEn;
	u16  reserved[209];
} SysCfg_st; //256 bytes

typedef struct 
{
	u16 Status; // 0 initializing,1: fail,2 OK
	u32 DevStaus;
	u16 ErrCode[32];
	u16 reserved[3];
} SysInitStatus_st;

typedef struct 
{
	s8 AppVer[6]; // Applicatin version
	s8 FpgaVer[6]; //FPGA version
	s8 reserved[20]; //32 bytes
} SysVerInfo_st;

typedef struct 
{
	float time_step_us;
	u32 trig_delay; //1 cnt: 1HCLK period
	u16 repeat_num;
	u16 sample_size;
	u16 cam_trig_cnt;
	u16 cam_trig_itv; //camera trigger interval,: # of position samples
	u16 trig_st_index; // trigger start index
	u16 reserved[23];//reserved
	s16 posx[MAX_DAC_SAMPLE_SIZE]; //place the array end of struct for the variable size of sample size
	s16 posy[MAX_DAC_SAMPLE_SIZE];
} traj_profile_st;

typedef struct 
{
	u32 reserved[8];
	short int posx[MAX_DAC_SAMPLE_SIZE];
	short int posy[MAX_DAC_SAMPLE_SIZE];
} ADC_buffer_st;

typedef struct
{
	float SLD_current;  //mA
	float IPD_current;  //uA
	u32	  EPD_DN; //digit nunmber
	float temp; //celsius
}SLD_sensor_data_st;

typedef struct 
{
	s32 PktErrCode;
	SLD_sensor_data_st SLD_sensor_data; //16bytes
	u32 Intr_src;
	u32 PIstatus;
	u32 UpgradeStatus;
	u32 UpgradeProgressCnt;
	u32 reserved[7];
}GP_Status_st; //

typedef struct
{
	s32	max_speed;
	s32	min_speed;
	s32	acc_step;
}StepMotorParam_st;

typedef struct
{
	s32 CurPos;
	StepMotorParam_st StepMotorParam;
	s32	sm_pos_min ;			// minus end position,added 2013.9.27;
	s32	sm_pos_max ;			// plus end position//added 2013.9.27;

	//variables for PI position monitoring.
	s32 sm_pi_hit_ref_pos;  //expected PI signal level change position
	s32 sm_pi_hit_margin;   //expected PI signal level margin, ref_pos+- margin.
	s32 sm_last_pi_hit_pos; // last PI signal change position;
	s32 sm_pi_hit_pos_error; // set to 1 when pi hit position is outside of ref pos+- margin, cleared when system initialized.

	s32 reserved[12];
}StepMotorInfo_st;

typedef struct
{
	double a1;
	double a2;
	double a3;
} Dispersion_st;

typedef struct
{
	double a0;
	double a1;
	double a2;
	double a3;
} SpectroCal_st;

typedef struct
{	
	u16 IM_MAX; //uA, SLD interal PD current at MAX cornea power
	u16 IM_MIN; //uA, SLD internal PD current at Min Cornea power
	u16 IS_MAX; //mA, SLD current at Max cornea power
	u16 IS_MIN; //mA, SLD current at Min Cornea power
	u16 DN_REF_EPD_MAX; // MAX external REF PD level in Digit number
	u16 DN_REF_EPD_MIN; // MIN external REF PD level in Digit number

	u16 RmonHighCode;  //ISL22317 Digital Poentiometer code
	u16 RmonLowCode1;  //AD5252 Digital Poentiometer code
	u16 RmonLowCode2;  //AD5252 Digital Poentiometer code
	u16 RsiCode;       //ISL22317 Digital Poentiometer code
} SLDparam_st;

typedef struct
{
	float Galvano_offset;
	float Galvano_Range;
}GalvanoCal_st;

typedef struct
{
	u32 SpectroSN; //spectromter serial
	u32 ColorSensorSN; //fundus camera sensor serial
	s8  SLD_SN[8];  //SLD serial No.ASCII string ���� ����.
	u32 reserved[4];

}SerialNumber_st; //64bytes.

typedef struct
{
	s32 IR_focus_zeroD_pos;
	s32 Scan_focus_zeroD_pos;
	s32 reserved[254]; //reserved for Diopter table.
} DiopterCal_st; //1kbytes

typedef struct
{
	s32			duty_base ;
	s32 		duty_fcw ;
	s32 		duty_fccw ;
	s32 		duty_ncw ;
	s32 		duty_nccw ;
	s32 		enc_near ;
	s32 		enc_offs ;
}dcm_setup_st;

typedef struct
{
	dcm_setup_st dcm_setup_x;
	dcm_setup_st dcm_setup_z;
}AutostageCal_st;

typedef struct
{
	s16 InPos;
	s16 OutPos;
}ReturnMirrorPos_st;

typedef struct
{
	s16 SmallPupilPos;
	s16 NormalPupilpos;
}PupilMaskPos_st;

typedef struct
{
	s16 MinusLensPos;
	s16 PlusLensPos;
	s16 NoLensPos;
	s16 reserved;
}DiopterCompLensPos_st;

typedef struct
{
	u8 CorneaAgain; //
	u8 CorneaDgain; //
	u8 RetinaAgain; //
	u8 RetinaDgain; //
}IRCamParam_st;

typedef struct {
     float m;
     float n;
     float a;
     float mlen;
     float flen;
     float diff;
     char valid;
     u8 reserved[3]; //word align purpose.
} KerSetupData_st;

typedef struct {
     float mm;
     float diopt;
     KerSetupData_st data[11];
} KerSetupDiopter_st;

typedef struct {
     KerSetupDiopter_st diopt[5];
} KerSetupTable_st;

typedef struct{
     s16 xoffset;
     s16 yoffset;
}IntFixationOffset_st;


typedef struct
{
	SerialNumber_st SysSerial;
	s32 REF_RetinaPos;
	s32 REF_CorneaPos;
	s32 PolarizationPos;
	DiopterCal_st Diopter_Cal;
	Dispersion_st RetinaDispersion;	
	Dispersion_st CorneaDispersion;
	SpectroCal_st SpectroCal;
	GalvanoCal_st Galvano_Xcal;
	GalvanoCal_st Galvano_Ycal;
	SLDparam_st  SLD_Param;
	u16 SpectroRefProfile[2048];//spectrometer reference level
	AutostageCal_st AutoStage_Cal;
	ReturnMirrorPos_st QuickReturnMirrorCal;
	ReturnMirrorPos_st SplitFocusMirrorCal;
	PupilMaskPos_st     PupilMaskCal;
	DiopterCompLensPos_st FdiopterCompLensPosCal; //Fundus
	DiopterCompLensPos_st SdiopterCompLensPosCal; //Scan
	IRCamParam_st	IRCamParam;
	KerSetupTable_st KerSetupTable; //2016.11
	IntFixationOffset_st IntFixationOffset;//2017.5.21
	u64 chksum;//8 bytes.
}SysCal_st;

typedef struct
{
	s16 offsetX;
	s16 offsetY;
}Galvano_Dynamic_st;


typedef struct
{
	s32 CurPos; //current position. May not be accurate, just reference only
	s16 LimitSensorStatus[2]; //[0]: Low Limit,[1]: High Limit,value 1: sensor hit
	s32 sm_pos_min;
	s32 sm_pos_max;
	s32 sm_pi_hit_ref_pos;  //expected PI signal level change position
	s32 sm_pi_hit_margin;   //expected PI signal level margin, ref_pos+- margin.
	s32 sm_last_pi_hit_pos; // last PI signal change position;
	s32 sm_pi_hit_pos_error; // set to 1 when pi hit position is outside of ref pos+- margin, cleared when system initialized.
}Step_Amotor_st;

typedef struct
{
	s16 EncPos; //current encoder position
	s16 CenterPos; //center position
	s16 reserved[6];
}DC_Amotor_st;

typedef struct
{
	DC_Amotor_st Xmotor;
	DC_Amotor_st Zmotor;
	Step_Amotor_st Ymotor;
}AutoStage_st;

typedef struct
{
	u32 reserved[2048];
}SysCal2_st;//8192=8kbytes max

typedef struct
{
	SysInitStatus_st InitStat;
	SysVerInfo_st	 SysVer;
	SysCfg_st		 SysCfg;
	traj_profile_st	 traj_profile1[MAX_TRAJ_NUM+1];
	ADC_buffer_st	 ADC_buffer[MAX_TRAJ_NUM+1];
	u32				 DownloadBlkBuf[1024 * 1024]; //4Mbyte
	GP_Status_st	 GP_status;
	StepMotorInfo_st StepMotorInfo[MAX_STEP_MOTOR_NUM];
	SysCal_st		 SysCal;
	Galvano_Dynamic_st Galvano_Dynamic;
	AutoStage_st	 AutoStageMotorInfo;
	SysCal2_st		 SysCal2;//
} HBS_st;	//buffer structure for host I/F, 6368640bytes total . 2017.10.31

#endif /* BIO_HBS_H_ */
