
#include <stdlib.h>
#include "xil_types.h"
#include "ar0130.h"
#include "ar0130_data.h"
#include "xiicps.h"
#include "bio_hbs.h"

extern HBS_st HBS;
//#define IRCAM_DEBUG
/*
0 = Normal operation. Generate output data from pixel array
1 = Solid color test pattern.
2 = Full color bar test pattern
3 = Fade to grey color bar test pattern
256 = Marching 1 test pattern (12 bit)
*/

struct ar0130_frame_size {
	u16 width;
	u16 height;
};

const struct ar0130_frame_size ar0130_supported_framesizes[] = {
	{  640,  360 },
	{  640,  480 },
	{ 1280,  720 },
	{ 1280,  960 },
};

enum resolution {
AR0130_640x360_BINNED,
AR0130_640x480_BINNED,
AR0130_720P_60FPS,
AR0130_FULL_RES_45FPS
};

struct ar0130_priv {
//        struct v4l2_subdev subdev;
//        struct media_pad pad;
        struct v4l2_rect crop;  /* Sensor window */
        struct v4l2_rect curr_crop;
        struct v4l2_mbus_framefmt format;
        enum resolution res_index;
//        struct v4l2_ctrl_handler ctrls;
//        struct ar0130_platform_data *pdata;
//        struct mutex power_lock; /* lock to protect power_count */
        struct ar0130_pll_divs *pll;
        int power_count;
        int autoexposure;
        u16 xskip;
        u16 yskip;

        /* cache register values */
        u16 output_control;
};

struct ar0130_priv ar0130[MAX_CAMERA_COUNT];

/**
 * reg_read - reads the data from the given register
 * camPort : camera Port 0 or 1
 * regAddr : 16-bit register address
 * rData : 16-bit register data pointer
 */
int ar0130_reg_read(uint32_t camPort, uint16_t regAddr, uint16_t *rData)
{

	u8 SendBuffer[2]; // regAddr
	u8 RecvBuffer[2]; // regAddr
	u32 lRtn;
	s32 Status;

	SendBuffer[0] = (regAddr >> 8);
	SendBuffer[1] = (regAddr & 0xff);


//	lRtn = I2C_write( I2C0, AR0130_I2C_ADDR, buf, 2, SKIP_STOP_BIT_AFTER_WRITE);

	Status = XIicPs_MasterSendPolled(&Iic, SendBuffer,
				 2, IIC_SLAVE_ADDR);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}


//		lRtn = I2C_read(I2C0, AR0130_I2C_ADDR, buf, 2, SKIP_BUSY_BIT_CHECK);
	/*
	 * Wait until bus is idle to start another transfer.
	 */
	while (XIicPs_BusIsBusy(&Iic)) {
		/* NOP */
	}

	Status = XIicPs_MasterRecvPolled(&Iic, RecvBuffer,
			  2, IIC_SLAVE_ADDR);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}



	
	*rData = ((u16) RecvBuffer[0] << 8 ) | RecvBuffer[1] ;

//	uart_printf("AR0130 %d port : Read from offset 0x%4x value 0x%4x\n", camPort, regAddr, *rData);
	return 0;
}

/**
 * reg_write - writes the data into the given register
 * @camPort: camera Port 0 or 1
 * @regAddr: address of the register in which to write
 * @wdata: data to be written into the register
 *
 */
int ar0130_reg_write(uint32_t camPort, uint16_t regAddr, uint16_t wData)
{
	u8 SendBuffer[4];
	u32 lRtn;
	u16 readData;
	s32 Status;
	
	SendBuffer[0] = (regAddr >> 8);
	SendBuffer[1] = (regAddr & 0xff);
	SendBuffer[2] = wData >> 8;
	SendBuffer[3] = wData & 0xff;
	

//		lRtn = I2C_write( I2C0, AR0130_I2C_ADDR, buf, 4, SET_STOP_BIT_AFTER_WRITE);
		Status = XIicPs_MasterSendPolled(&Iic, SendBuffer,
					 4, IIC_SLAVE_ADDR);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

	
//	uart_printf("AR0130 %d port : Write to offset 0x%4x value 0x%4x\n", camPort, regAddr, wData);
//	ar0130_reg_read(camPort, regAddr, &readData);
//	return 0;
}

/**
 * ar0130_set_xclk - provides external clock without firmware intervention.
 */
int ar0130_set_xclk (int camPort, int hz , u32 xclk)
{
#if 0
	int ret;

    ret = ar0130_reg_write(camPort, AR0130_RESET_REG, 0x0001);
    if (ret < 0)
    	return ret;

    USTIMER_delay(10000);

    ret = ar0130_reg_write(camPort, AR0130_RESET_REG, AR0130_STREAM_OFF);
    if (ret < 0)
    	return ret;
#endif
    return 0;
}
#if 0
/**
 * ar0130_reset - Hard resets the sensor . On power up , hardware reset is deasserted.
 */
int ar0130_reset(unsigned int camPort, int enable)
{
#if 0
	int ret;

	ret = ar0130_reg_write(camPort, AR0130_RESET_REG, 0x0001);
	if (ret < 0)
		return ret;

	USTIMER_delay(10000);

	ret = ar0130_reg_write(camPort, AR0130_RESET_REG, AR0130_STREAM_OFF);
	if (ret < 0)
		return ret;
#endif
	return 0;
}
#endif
/**
 * ar0130_reset - Soft resets the sensor
 * @camPort: camera Port
 * 
 */
int ar0130_soft_reset(u32 camPort)
{
	int ret;

	ret = ar0130_reg_write(camPort, AR0130_RESET_REG, 0x0001);
	if (ret)
		return ret;
		
//	USTIMER_delay(10000);
	usleep(10000);
	ret = ar0130_reg_write(camPort, AR0130_RESET_REG, AR0130_STREAM_OFF);
	if (ret)
		return ret;

	return 0;
}

/**
 * ar0130_pll_enable - enable the sensor pll
 * @camPort: camera Port
 * 
 */
int ar0130_pll_enable(u32 camPort)
{
	int ret = 0;
/*
	ret = ar0130_reg_write(camPort, 0x302C, 0x0002);		// VT_SYS_CLK_DIV
	ret |= ar0130_reg_write(camPort, 0x302A, 0x0004);	// VT_PIX_CLK_DIV
	ret |= ar0130_reg_write(camPort, 0x302E, 0x0002);	// PRE_PLL_CLK_DIV
	ret |= ar0130_reg_write(camPort, 0x3030, 0x002C);	// PLL_MULTIPLIER
	ret |= ar0130_reg_write(camPort, 0x30B0, 0x1300);	// DIGITAL_TEST
*/
	/* PIXCLK 49.5MHz  30fps */
	ret = ar0130_reg_write(camPort, 0x302C, 0x0002);		// VT_SYS_CLK_DIV  P1
	ret |= ar0130_reg_write(camPort, 0x302A, 0x0006);	// VT_PIX_CLK_DIV      P2
	ret |= ar0130_reg_write(camPort, 0x302E, 0x0002);	// PRE_PLL_CLK_DIV  N
	ret |= ar0130_reg_write(camPort, 0x3030, 0x002C);	// PLL_MULTIPLIER   M
	//ret |= ar0130_reg_write(camPort, 0x30B0, 0x1300);//column gain x1
	ret |= ar0130_reg_write(camPort, 0x30B0, 0x1330); //column gain x8
	ret |= ar0130_reg_write(camPort, 0x3EE4, 0xD308); //column gain x8
//	USTIMER_delay(100000);
	usleep(100000);
	return ret;
}

/**
 * ar0130_power_on - power on the sensor
 * @ar0130: pointer to private data structure
 * 
 */
int ar0130_power_on(u32 camPort)
{
#if 0
	int ext_freq;

	/* Ensure RESET_BAR is low */
	ar0130_reset(camPort, 1);
	USTIMER_delay(1000);

	/* Enable clock */
	ar0130_set_xclk (camPort, 0, ext_freq);
	USTIMER_delay(1000);

	USTIMER_delay(2000);
	/* Now RESET_BAR must be high */
	ar0130_reset(camPort, 0);
	USTIMER_delay(1000);
#endif
//	USTIMER_delay(10000);
	usleep(100000);
	ar0130_soft_reset(camPort);
//	USTIMER_delay(1000);
	usleep(1000);

	return 0;
}

/**
 * ar0130_power_off - power off the sensor
 * @ar0130: pointer to private data structure
 * 
 */
static int ar0130_power_off(unsigned int camPort)
{
#if 0
    /* Disable clock */
	ar0130_set_xclk (camPort, 0, 0);
#endif
//	USTIMER_delay(1000);
	usleep(1000);
	return 0;
}

int ar0130_linear_mode_setup(u32 camPort)
{
	int i, ret;

	ret = ar0130_reg_write(camPort, 0x3088, 0x8000);		// SEQ_CTRL_PORT
	for(i = 0; i < 80; i++)
		ret |= ar0130_reg_write(camPort, AR0130_SEQ_PORT, ar0130_linear_data[i]);
 
	ret |= ar0130_reg_write(camPort, 0x309E, 0x0000);	// DCDS_PROG_START_ADDR
	ret |= ar0130_reg_write(camPort, 0x30E4, 0x6372);	// ADC_BITS_6_7
	ret |= ar0130_reg_write(camPort, 0x30E2, 0x7253);	// ADC_BITS_4_5
	ret |= ar0130_reg_write(camPort, 0x30E0, 0x5470);	// ADC_BITS_2_3
	ret |= ar0130_reg_write(camPort, 0x30E6, 0xC4CC);	// ADC_CONFIG1
	ret |= ar0130_reg_write(camPort, 0x30E8, 0x8050);	// ADC_CONFIG2

//	USTIMER_delay(200000);
	usleep(200000);
	ret |= ar0130_reg_write(camPort, 0x3082, 0x0029);	// OPERATION_MODE_CTRL
	ret |= ar0130_reg_write(camPort, 0x30B0, 0x1300);	// DIGITAL_TEST
	ret |= ar0130_reg_write(camPort, 0x30D4, 0xE007);	// COLUMN_CORRECTION
	ret |= ar0130_reg_write(camPort, 0x301A, 0x10DC);	// RESET_REGISTER
	ret |= ar0130_reg_write(camPort, 0x301A, 0x10D8);	// RESET_REGISTER
	ret |= ar0130_reg_write(camPort, 0x3044, 0x0400);	// DARK_CONTROL
	ret |= ar0130_reg_write(camPort, 0x3EDA, 0x0F03);	// DAC_LD_14_15
	ret |= ar0130_reg_write(camPort, 0x3ED8, 0x01EF);	// DAC_LD_12_13
	//ret |= ar0130_reg_write(camPort, 0x3012, 0x03BC);	// COARSE_INTEGRATION_TIME,956 line time
	if(camPort==1)//retina
		{ret |= ar0130_reg_write(camPort, 0x3012, 0x0706);	}// COARSE_INTEGRATION_TIME,956*2 line time, 16fps
	else
		{ret |= ar0130_reg_write(camPort, 0x3012, 0x03BC);}	// COARSE_INTEGRATION_TIME,956 line time, 30fps
	ret |= ar0130_reg_write(camPort, 0x305E, 0x00FF);	// digital gain.upper 3bit : integer part of digital gain(1 step: 1), lower 5bit: (fractional part , 1 step:0.03125),max gain

	return ret;
}

int ar0130_set_resolution(u32 camPort, enum resolution res_index)
{
	int ret = 0;

	switch(res_index){
	case AR0130_FULL_RES_45FPS: //1280x960
		ret = ar0130_reg_write(camPort, 0x3032, 0x0000);		// DIGITAL_BINNING
		ret |= ar0130_reg_write(camPort, 0x3002, 0x0002);	// Y_ADDR_START
		ret |= ar0130_reg_write(camPort, 0x3004, 0x0000);	// X_ADDR_START
		ret |= ar0130_reg_write(camPort, 0x3006, 0x03C1);	// Y_ADDR_END
		ret |= ar0130_reg_write(camPort, 0x3008, 0x04FF);	// X_ADDR_END
		ret |= ar0130_reg_write(camPort, 0x300A, 0x03DE);	// FRAME_LENGTH_LINES
		ret |= ar0130_reg_write(camPort, 0x300C, 0x0672);	// LINE_LENGTH_PCK
		break;
	case AR0130_720P_60FPS: //1280x720 
		ret = ar0130_reg_write(camPort, 0x3032, 0x0000);		// DIGITAL_BINNING
		ret |= ar0130_reg_write(camPort, 0x3002, 0x0002);	// Y_ADDR_START
		ret |= ar0130_reg_write(camPort, 0x3004, 0x0000);	// X_ADDR_START
		ret |= ar0130_reg_write(camPort, 0x3006, 0x02D1);	// Y_ADDR_END
		ret |= ar0130_reg_write(camPort, 0x3008, 0x04FF);	// X_ADDR_END
		ret |= ar0130_reg_write(camPort, 0x300A, 0x02EF);	// FRAME_LENGTH_LINES
		ret |= ar0130_reg_write(camPort, 0x300C, 0x0672);	// LINE_LENGTH_PCK
		break;
	case AR0130_640x480_BINNED: //(640,480):
		ret = ar0130_reg_write(camPort, 0x3032, 0x0002);		// DIGITAL_BINNING
		ret |= ar0130_reg_write(camPort, 0x3002, 0x0002);	// Y_ADDR_START
		ret |= ar0130_reg_write(camPort, 0x3004, 0x0000);	// X_ADDR_START
		ret |= ar0130_reg_write(camPort, 0x3006, 0x03C1);	// Y_ADDR_END
		ret |= ar0130_reg_write(camPort, 0x3008, 0x04FF);	// X_ADDR_END
		ret |= ar0130_reg_write(camPort, 0x300A, 0x03DE);	// FRAME_LENGTH_LINES
		ret |= ar0130_reg_write(camPort, 0x300C, 0x0672);	// LINE_LENGTH_PCK
		ret |= ar0130_reg_write(camPort, 0x306E, 0x9000);	// DATAPATH_SELECT,bayer space RGB disabled.
	//	ret |= ar0130_reg_write(camPort, 0x306E, 0x9010);	// DATAPATH_SELECT
		break;
	case AR0130_640x360_BINNED: //(640,360):
		ret = ar0130_reg_write(camPort, 0x3032, 0x0002);		// DIGITAL_BINNING
		ret |= ar0130_reg_write(camPort, 0x3002, 0x0002);	// Y_ADDR_START
		ret |= ar0130_reg_write(camPort, 0x3004, 0x0000);	// X_ADDR_START
		ret |= ar0130_reg_write(camPort, 0x3006, 0x02D1);	// Y_ADDR_END
		ret |= ar0130_reg_write(camPort, 0x3008, 0x04FF);	// X_ADDR_END
		ret |= ar0130_reg_write(camPort, 0x300A, 0x03DE);	// FRAME_LENGTH_LINES
		ret |= ar0130_reg_write(camPort, 0x300C, 0x0672);	// LINE_LENGTH_PCK
		ret |= ar0130_reg_write(camPort, 0x306E, 0x9010);	// DATAPATH_SELECT
		break;
	default: // AR0130_FULL_RES_45FPS
		ret = ar0130_reg_write(camPort, 0x3032, 0x0000);		// DIGITAL_BINNING
		ret |= ar0130_reg_write(camPort, 0x3002, 0x0002);	// Y_ADDR_START
		ret |= ar0130_reg_write(camPort, 0x3004, 0x0000);	// X_ADDR_START
		ret |= ar0130_reg_write(camPort, 0x3006, 0x03C1);	// Y_ADDR_END
		ret |= ar0130_reg_write(camPort, 0x3008, 0x04FF);	// X_ADDR_END
		ret |= ar0130_reg_write(camPort, 0x300A, 0x03DE);	// FRAME_LENGTH_LINES
		ret |= ar0130_reg_write(camPort, 0x300C, 0x0672);	// LINE_LENGTH_PCK
		break;
	}
	
	return ret;
}

int ar0130_set_autoexposure(u32 camPort, int enable)
{
	int ret;

	if(enable){
		ar0130[camPort].autoexposure = 1;
		ret = ar0130_reg_write(camPort, 0x3064, 0x1982);		// EMBEDDED_DATA_CTRL
		ret |= ar0130_reg_write(camPort, 0x3100, 0x001B);	// AE_CTRL_REG
		ret |= ar0130_reg_write(camPort, 0x3112, 0x029F);	// AE_DCG_EXPOSURE_HIGH_REG
		ret |= ar0130_reg_write(camPort, 0x3114, 0x008C);	// AE_DCG_EXPOSURE_LOW_REG
		ret |= ar0130_reg_write(camPort, 0x3116, 0x02C0);	// AE_DCG_GAIN_FACTOR_REG
		ret |= ar0130_reg_write(camPort, 0x3118, 0x005B);	// AE_DCG_GAIN_FACTOR_INV_REG
		ret |= ar0130_reg_write(camPort, 0x3102, 0x0384);	// AE_LUMA_TARGET_REG
		ret |= ar0130_reg_write(camPort, 0x3104, 0x1000);	// AE_HIST_TARGET_REG
		ret |= ar0130_reg_write(camPort, 0x3126, 0x0080);	// AE_ALPHA_V1_REG
		ret |= ar0130_reg_write(camPort, 0x311C, 0x03DD);	// AE_MAX_EXPOSURE_REG
		ret |= ar0130_reg_write(camPort, 0x311E, 0x0002);	// AE_MIN_EXPOSURE_REG
		return ret;
	}
	else {
		ar0130[camPort].autoexposure = 0;
		ret = ar0130_reg_write(camPort, AR0130_RESET_REG, AR0130_STREAM_OFF);
		//ret |= ar0130_reg_write(camPort, 0x3100, 0x001A);	// AE_CTRL_REG
		ret |= ar0130_reg_write(camPort, 0x3100, 0x0000);	// AE_CTRL_REG
		ret |= ar0130_reg_write(camPort, AR0130_RESET_REG, AR0130_STREAM_ON);
		return ret;
	}
}

int ar0130_s_power(u32 camPort, int on)
{
	int ret = 0;
	/*
	* If the power count is modified from 0 to != 0 or from != 0 to 0,
	* update the power state.
	*/	
	if (ar0130[camPort].power_count == !on) {
		if (on) {
			ret = ar0130_power_on(camPort);
			if (ret) {
				printf("Failed to power on: %d\n", ret);
				return ret;
			}
		} else 
			ret = ar0130_power_off(camPort);
	} 
	/* Update the power count. */
	ar0130[camPort].power_count += on ? 1 : -1;

	return ret;
}


int ar0130_s_stream(u32 camPort, int enable)
{
	int ret = 0;

	if (!enable) {
		return 3;
	}
	
	ret=ar0130_reg_write(camPort, 0x3064, 0x1982); // embedded data control, default value, should be placed for clk out stop anormaly at some part at low temperature,2019,0808
	if(ret){
	        printf("Failed to setup Cam %d embedded data ctrl reg: %d\n",camPort, ret);
	        return ret;
	    }

	ret = ar0130_linear_mode_setup(camPort);
	if(ret){
		printf("Failed to setup linear mode: %d\n", ret);
		return ret;
	}

	ret = ar0130_set_resolution(camPort, ar0130[camPort].res_index);
	if(ret){
		printf("Failed to setup resolution: %d\n", ret);
		return ret;
	}

	/* Up Down Flip */
	if (camPort == 0)
	{
		ret |= ar0130_reg_write(camPort, 0x3040, 0xC000); //horizontal/vertical flip
	}
	if (camPort == 1)
		{
		//	ret |= ar0130_reg_write(camPort, 0x3040, 0x1000);
			ret |= ar0130_reg_write(camPort, 0x3040, 0x4000); //horizontal flip
		}
	//ret |= ar0130_reg_write(camPort, 0x3040, AR0130_STREAM_OFF);
	ret |= ar0130_reg_write(camPort, AR0130_RESET_REG, AR0130_STREAM_OFF);
	ret |= ar0130_reg_write(camPort, 0x31D0, 0x0001);	// HDR_COMP enable
	//ret |= ar0130_reg_write(camPort, 0x31D0, 0x0000);	// HDR_COMP off

	ret = ar0130_pll_enable(camPort);
	if(ret){
		printf("Failed to enable pll: %d\n", ret);
		return ret;
	}

	ret |= ar0130_reg_write(camPort, AR0130_RESET_REG, AR0130_STREAM_ON);

	ret |= ar0130_set_autoexposure(camPort, DISABLE);

	ret |= ar0130_reg_write(camPort, AR0130_TEST_REG, AR0130_TEST_PATTERN);
	
	return ret;
}

/* Data syncronization HBS.SysCal.IRCamparam stucture .*/
int ar0130_gain_sync_syscal(u32 camPort, u8 Again , u8 Dgain)
{
	u8 AgainTemp0, AgainTemp1;
	u8 i;
	u32 lRet;

	/* Again */
	AgainTemp0  = (Again & (u8)0x10) >> 4;
	AgainTemp1  = Again & (u8)0x0f;
	
	i = 3;
	while((i>0) && ((AgainTemp1 >> i)==0x0)) i--;
	
	lRet = ar0130_reg_write (camPort, 0x30B0, (i << 4)|0x1080 ); //reserve default and set CFA to monochrome
	if(lRet){printf("ar0130 : failed to apply calib data\n"); return 1;}
	
	lRet = ar0130_reg_write (camPort, 0x3EE4, (AgainTemp0 << 8)|0xD208 );
	if(lRet){printf("ar0130 : failed to apply calib data\n"); return 1;}

	/*DGain*/
	lRet = ar0130_reg_write (camPort, 0x305E, (u16)Dgain);
	if(lRet){printf("ar0130 : failed to apply calib data\n"); return 1;}

#if defined (IRCAM_DEBUG)
	printf("\n IRCAM_AGAIN =%d,%d, DGAIN =%d\n",AgainTemp0, i, Dgain);
#endif
	return 0;
}
/***********************************************************
	internal operations
************************************************************/
int ar0130_registered(u32 camPort)
{
	int ret = 0;
	u16 rData;

	/* Read out the chip version register */
	ret = ar0130_reg_read(camPort, 0x3000, &rData);

	printf ("read Data : 0x%4X \n", rData );
	if (rData != AR0130_CHIP_ID)
	{
		printf("AR0130 not detected, wrong chip ID "
					"0x%4.4X\n", rData);
		return 4;
	}

//	uart_printf("AR0130 detected at address 0x%02X: chip ID = 0x%4.4X\n",
//			camPort, AR0130_CHIP_ID);
			
	return ret;
}

int ar0130_init(u32 camPort)
{
	int ret = 0;
    
	ar0130[camPort].crop.width     = AR0130_WINDOW_WIDTH_DEF;
	ar0130[camPort].crop.height    = AR0130_WINDOW_HEIGHT_DEF;
	ar0130[camPort].crop.left      = AR0130_COLUMN_START_DEF;
	ar0130[camPort].crop.top       = AR0130_ROW_START_DEF;
	
	ar0130[camPort].format.code 		= V4L2_MBUS_FMT_SGRBG12_1X12;
	ar0130[camPort].format.width 		= AR0130_WINDOW_WIDTH_DEF;
	ar0130[camPort].format.height 		= AR0130_WINDOW_HEIGHT_DEF;
	ar0130[camPort].format.field 		= V4L2_FIELD_NONE;
	ar0130[camPort].format.colorspace 	= V4L2_COLORSPACE_SRGB;

	ar0130[camPort].res_index    =  AR0130_640x480_BINNED;
    
	ret = ar0130_s_power(camPort, 1);
	if(ret){
		printf("Failed to supply power: %d\n", ret);
		return ret;
	}

	ret = ar0130_registered(camPort);
	if(ret){
		printf("Failed to identify : %d\n", ret);
		return ret;
	}

	ret = ar0130_s_stream(camPort, 1);
	if(ret){
		printf("Failed to setup stream : %d\n", ret);
		return ret;
	}

	return ret;
}

int init_camera(void)
{
	int i ;
	int ret;

	u8 CorneaAgain;
	u8 CorneaDgain;

	for (i=0; i<MAX_CAMERA_COUNT;i++ )
	{
		ret = ar0130_init(i);
		if(ret){
			printf("Failed to init camport %d ,error %d\n", i, ret);
		//	return ret;
		}
		else
		{
			printf("Init Camera Sucess!");
//			HBS.InitStat.DevStaus|=((INIT_C_IRCAM_BIT<<i));
		}
	}

	/* Analog Gain , Digital Gain Set from HBS.*/
	ret = ar0130_gain_sync_syscal(0, HBS.SysCal.IRCamParam.CorneaAgain, HBS.SysCal.IRCamParam.CorneaDgain);
#if defined (IRCAM_DEBUG)
	uart_printf("HBS.SysCal.IRCamParam address: 0x%X size: %d \n", (U32)&HBS.SysCal.IRCamParam , sizeof(HBS.SysCal.IRCamParam) );
#endif
	return ret;
}

int IicPsInit(u16 DeviceId)
{
	int Status;
	XIicPs_Config *Config;
	int Index;

	/*
	 * Initialize the IIC driver so that it's ready to use
	 * Look up the configuration in the config table,
	 * then initialize it.
	 */
	Config = XIicPs_LookupConfig(DeviceId);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XIicPs_CfgInitialize(&Iic, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to ensure that the hardware was built correctly.
	 */
	Status = XIicPs_SelfTest(&Iic);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Set the IIC serial clock rate.
	 */
	XIicPs_SetSClk(&Iic, IIC_SCLK_RATE);

	return XST_SUCCESS;
}
