#ifndef __DC_MOTOR_H__
#define __DC_MOTOR_H__


#include "xil_types.h"
//  Constant Definition.
/////////////////////////////////////////////////////////////////
#define CR_MOTOR                    0
#define X_MOTOR			            1
#define Y_MOTOR			            2
#define Z_MOTOR			            3
#define REF_MOTOR			        4

#define DCM_CR                      CR_MOTOR
#define DCM_XH                      X_MOTOR
#define DCM_YH                      Y_MOTOR
#define DCM_ZH                      Z_MOTOR
#define DCM_REF                     REF_MOTOR
#define DCM_NUM                     5
#define X_STAGE_MOTOR 				9
#define Z_STAGE_MOTOR 				11


#define DCM_DUTY_MAX                200
#define DCM_DUTY_MIN                0
#define DCM_DUTY_CW                 1
#define DCM_DUTY_CCW                2
#define DCM_DUTY_NEAR               3

#define ROTATE_CCW                  0
#define ROTATE_CW                   1
#define HIGH_SIDE                   0
#define LOW_SIDE                    1

#define CR_HIGH_SIDE				1
#define CR_LOW_SIDE				    0

#define DCMX_DUTY_BASE_DEFAULT 30
#define DCMX_DUTY_FCW_DEFAULT  180
#define DCMX_DUTY_FCCW_DEFAULT 180
#define DCMX_DUTY_NCW_DEFAULT  50
#define DCMX_DUTY_NCCW_DEFAULT 50
#define DCMX_ENC_NEAR_DEFAULT  4
#define DCMX_ENC_OFFS_DEFAULT  0

#define DCMZ_DUTY_BASE_DEFAULT 30
#define DCMZ_DUTY_FCW_DEFAULT  180
#define DCMZ_DUTY_FCCW_DEFAULT 180
#define DCMZ_DUTY_NCW_DEFAULT  50
#define DCMZ_DUTY_NCCW_DEFAULT 50
#define DCMZ_ENC_NEAR_DEFAULT  4
#define DCMZ_ENC_OFFS_DEFAULT  0



#define DCM_MOTOR_DEF_VAL           0x0008

#define MOTOR_SETUP_X 					1
#define MOTOR_SETUP_YZ					2
#define MOTOR_CTRL_N 					4
#define MOTOR_ITEM_N 					7

#define MOTOR_TEST_RUN 					1
#define MOTOR_TEST_STOP					0

#define		DCM_DUTY_BASE		0
#define		DCM_DUTY_FCW		1
#define		DCM_DUTY_FCCW		2

//copied .
#define RIGHT_SIDE		0	// 'R'
#define LEFT_SIDE		1	// 'L'
#define R_SIDE			0
#define L_SIDE			1
#define SIDE_N          2
#define FORWARD			1
#define BACKWARD		2

#define NONE            0x00
#define LEFT            0x01
#define RIGHT           0x02
#define UP              0x04
#define DOWN            0x08
#define DC_FORWARD         0x10
#define DC_BACKWARD        0x20
#define UP_LEFT         (UP|LEFT)
#define UP_RIGHT        (UP|RIGHT)
#define DOWN_LEFT       (DOWN|LEFT)
#define DOWN_RIGHT      (DOWN|RIGHT)
#define DIR_X           (LEFT|RIGHT)
#define DIR_Y           (UP|DOWN)
#define DIR_Z           (FORWARD|BACKWARD)
#define DIR_XY          (DIR_X|DIR_Y)
#define DIR_XYZ         (DIR_X|DIR_Y|DIR_Z)

#define ENC_HEAD_LEFT_END           +35			// +45			// +35			// +30
#define ENC_HEAD_RIGHT_END          -35			// -45 		// -35			// -30
#define ENC_HEAD_FRONT_END          +120
#define ENC_HEAD_REAR_END           -120
#define ENC_BASE_UP_END             -83			// -90			// -90 	//+90
#define ENC_BASE_DOWN_END           +95			// +90			// +90		// -90


#define CR_MOVE_UP 1
#define CR_MOVE_DN 0

#define DCMOTCR_BASE 0x43C00000
#define DCMOTX_BASE 0x43C00000
#define DCMOTY_BASE 0x43C00000
#define DCMOTZ_BASE 0x43C00000
#define DCMOTREF_BASE 0x43C30000

#define DCMOTCR_CTRL_REG				(*(volatile u32 *)(DCMOTCR_BASE))		//X
#define DCMOTCR_PWM_DIV_REG				(*(volatile u32 *)(DCMOTCR_BASE+0x4))		//
#define DCMOTCR_PWM_DIVL_REG			(*(volatile u32 *)(DCMOTCR_BASE+0x8))		//
#define DCMOTCR_PWM_DUTY_BASE_REG		(*(volatile u32 *)(DCMOTCR_BASE+0xC))		//
#define DCMOTCR_NEARENC_REG				(*(volatile u32 *)(DCMOTCR_BASE+0x10))		//
#define DCMOTCR_SPEED_NEAR_REG			(*(volatile u32 *)(DCMOTCR_BASE+0x14))		//
#define DCMOTCR_FAR_CW_REG				(*(volatile u32 *)(DCMOTCR_BASE+0x18))		//
#define DCMOTCR_FAR_CCW_REG				(*(volatile u32 *)(DCMOTCR_BASE+0x1C))		//
#define DCMOTCR_TARGET_ENC_REG			(*(volatile u32 *)(DCMOTCR_BASE+0x20))		//Z target encoder 12 bit.
#define DCMOTCR_LIMIT_L_REG				(*(volatile u32 *)(DCMOTCR_BASE+0x24))		//
#define DCMOTCR_LIMIT_H_REG				(*(volatile u32 *)(DCMOTCR_BASE+0x28))		//
#define DCMOTCR_MOVING_PULSE_REG		(*(volatile u32 *)(DCMOTCR_BASE+0x2C))		//
#define DCMOTCR_ENCODER_DATA_REG		(*(volatile u32 *)(DCMOTCR_BASE+0x30))		//		//

#define DCMOTX_CTRL_REG					(*(volatile u32 *)(DCMOTX_BASE))		//X
#define DCMOTX_PWM_DIV_REG				(*(volatile u32 *)(DCMOTX_BASE+0x4))		//
#define DCMOTX_PWM_DIVL_REG				(*(volatile u32 *)(DCMOTX_BASE+0x8))		//
#define DCMOTX_PWM_DUTY_BASE_REG		(*(volatile u32 *)(DCMOTX_BASE+0xC))		//
#define DCMOTX_NEARENC_REG				(*(volatile u32 *)(DCMOTX_BASE+0x10))		//
#define DCMOTX_SPEED_NEAR_REG			(*(volatile u32 *)(DCMOTX_BASE+0x14))		//
#define DCMOTX_FAR_CW_REG				(*(volatile u32 *)(DCMOTX_BASE+0x18))		//
#define DCMOTX_FAR_CCW_REG				(*(volatile u32 *)(DCMOTX_BASE+0x1C))		//
#define DCMOTX_TARGET_ENC_REG			(*(volatile u32 *)(DCMOTX_BASE+0x20))		//Z target encoder 12 bit.
#define DCMOTX_LIMIT_L_REG				(*(volatile u32 *)(DCMOTX_BASE+0x24))		//
#define DCMOTX_LIMIT_H_REG				(*(volatile u32 *)(DCMOTX_BASE+0x28))		//
#define DCMOTX_MOVING_PULSE_REG			(*(volatile u32 *)(DCMOTX_BASE+0x2C))		//
#define DCMOTX_ENCODER_DATA_REG			(*(volatile u32 *)(DCMOTX_BASE+0x30))		//		//

#define DCMOTY_CTRL_REG					(*(volatile u32 *)(DCMOTY_BASE))		//X
#define DCMOTY_PWM_DIV_REG				(*(volatile u32 *)(DCMOTY_BASE+0x4))		//
#define DCMOTY_PWM_DIVL_REG				(*(volatile u32 *)(DCMOTY_BASE+0x8))		//
#define DCMOTY_PWM_DUTY_BASE_REG		(*(volatile u32 *)(DCMOTY_BASE+0xC))		//
#define DCMOTY_NEARENC_REG				(*(volatile u32 *)(DCMOTY_BASE+0x10))		//
#define DCMOTY_SPEED_NEAR_REG			(*(volatile u32 *)(DCMOTY_BASE+0x14))		//
#define DCMOTY_FAR_CW_REG				(*(volatile u32 *)(DCMOTY_BASE+0x18))		//
#define DCMOTY_FAR_CCW_REG				(*(volatile u32 *)(DCMOTY_BASE+0x1C))		//
#define DCMOTY_TARGET_ENC_REG			(*(volatile u32 *)(DCMOTY_BASE+0x20))		//Z target encoder 12 bit.
#define DCMOTY_LIMIT_L_REG				(*(volatile u32 *)(DCMOTY_BASE+0x24))		//
#define DCMOTY_LIMIT_H_REG				(*(volatile u32 *)(DCMOTY_BASE+0x28))		//
#define DCMOTY_MOVING_PULSE_REG			(*(volatile u32 *)(DCMOTY_BASE+0x2C))		//
#define DCMOTY_ENCODER_DATA_REG			(*(volatile u32 *)(DCMOTY_BASE+0x30))		//		//

#define DCMOTZ_CTRL_REG					(*(volatile u32 *)(DCMOTZ_BASE))		//X
#define DCMOTZ_PWM_DIV_REG				(*(volatile u32 *)(DCMOTZ_BASE+0x4))		//
#define DCMOTZ_PWM_DIVL_REG				(*(volatile u32 *)(DCMOTZ_BASE+0x8))		//
#define DCMOTZ_PWM_DUTY_BASE_REG		(*(volatile u32 *)(DCMOTZ_BASE+0xC))		//
#define DCMOTZ_NEARENC_REG				(*(volatile u32 *)(DCMOTZ_BASE+0x10))		//
#define DCMOTZ_SPEED_NEAR_REG			(*(volatile u32 *)(DCMOTZ_BASE+0x14))		//
#define DCMOTZ_FAR_CW_REG				(*(volatile u32 *)(DCMOTZ_BASE+0x18))		//
#define DCMOTZ_FAR_CCW_REG				(*(volatile u32 *)(DCMOTZ_BASE+0x1C))		//
#define DCMOTZ_TARGET_ENC_REG			(*(volatile u32 *)(DCMOTZ_BASE+0x20))		//Z target encoder 12 bit.
#define DCMOTZ_LIMIT_L_REG				(*(volatile u32 *)(DCMOTZ_BASE+0x24))		//
#define DCMOTZ_LIMIT_H_REG				(*(volatile u32 *)(DCMOTZ_BASE+0x28))		//
#define DCMOTZ_MOVING_PULSE_REG			(*(volatile u32 *)(DCMOTZ_BASE+0x2C))		//
#define DCMOTZ_ENCODER_DATA_REG			(*(volatile u32 *)(DCMOTZ_BASE+0x30))		//		//

#define DCMOTREF_CTRL_REG					(*(volatile u32 *)(DCMOTREF_BASE))		//X
#define DCMOTREF_LIMIT_H_REG				(*(volatile u32 *)(DCMOTREF_BASE+0x4))		//
#define DCMOTREF_LIMIT_L_REG				(*(volatile u32 *)(DCMOTREF_BASE+0x8))		//
#define DCMOTREF_MOVING_PULSE_REG			(*(volatile u32 *)(DCMOTREF_BASE+0xC))		//
#define DCMOTREF_NEARENC_REG				(*(volatile u32 *)(DCMOTREF_BASE+0x10))		//
#define DCMOTREF_PWM_DUTY_BASE_REG		(*(volatile u32 *)(DCMOTREF_BASE+0x14))		//
#define DCMOTREF_PWM_DIVL_REG				(*(volatile u32 *)(DCMOTREF_BASE+0x18))		//
#define DCMOTREF_PWM_DIV_REG				(*(volatile u32 *)(DCMOTREF_BASE+0x1C))		//
#define DCMOTREF_FAR_CCW_REG				(*(volatile u32 *)(DCMOTREF_BASE+0x20))		//
#define DCMOTREF_FAR_CW_REG				(*(volatile u32 *)(DCMOTREF_BASE+0x24))		//
#define DCMOTREF_SPEED_NEAR_REG			(*(volatile u32 *)(DCMOTREF_BASE+0x28))		//
#define DCMOTREF_TARGET_ENC_REG			(*(volatile u32 *)(DCMOTREF_BASE+0x2C))		//Z target encoder 12 bit.

#define DCMOTREF_ENCODER_DATA_REG			(*(volatile u32 *)(DCMOTREF_BASE+0x34))		//		//



//  Struct Type Declaration
/////////////////////////////////////////////////////////////
typedef struct _dcm_control_t_
{
    u16         pwm_div ;
    u16         duty_base ;
    u16         duty_offs ;

	int			motor_on;
    int         auto_ctrl ;
    int         rotate_dir ;
    int         side ;
    u8          speed_near ;
    u8          speed_far_cw ;
    u8          speed_far_ccw ;
    u8          enc_near ;
    int         enc_ctrl ;

} dcm_control_t ;

typedef struct _dcm_state_t_
{
    int         on_brake ;
    int         on_move ;
    int         on_dir ;
    int 		on_near ;

    int         pos ;
    int         target_on ;
    int         target_enc ;
    int         center_enc ;
    int        center_set ;

} dcm_state_t ;


typedef struct _dcm_setup_t_
{
	int			duty_base ;
	int 		duty_fcw ;
	int 		duty_fccw ;
	int 		duty_ncw ;
	int 		duty_nccw ;
	int 		enc_near ;
	int 		enc_offs ;

	float 		kp; //proportional gain
	float		ki; //integral gain
} dcm_setup_t ;


typedef struct _dcm_sys_var_t_
{
	dcm_setup_t		dcm_setup[DCM_NUM] ;

	char 		reserv[128] ;

} dcm_sys_var_t ;


extern dcm_control_t       	dcm_ctrl[DCM_NUM] ;
extern dcm_state_t         	dcm_stat[DCM_NUM] ;
extern dcm_setup_t 			dcm_setup[DCM_NUM] ;
//  Function Declaration 

typedef struct _motor_ctrl_t_
{
	int			duty_base ;
	int 		duty_far_cw ;
	int 		duty_far_ccw ;
	int 		duty_near_cw ;
	int 		duty_near_ccw ;
	int 		enc_near ;
	int 		enc_offs ;

} motor_ctrl_t ;

typedef struct _motor_setup_t_
{
	motor_ctrl_t 		m_ctrl[MOTOR_CTRL_N] ;

} motor_setup_t ;

typedef struct _dcmotor_force_move_flag_t
{
	int x;
	int z;
}dcmotor_force_move_flag_t;

union	DC_Motor_Control
{
	u32	byte;
	struct
	{
		unsigned Motor_Enc_con: 1;
		// Encoder 값을 이용해서, 자동으로 모터를 움직이고자 할 떄 사용한다. 1:on, 0:off
		unsigned Motor_Break: 1;
		// Motor 이동중이라도 이 신호를 '1'로 주면, Break 가 걸린다.
		unsigned Motor_Enc_up: 1;
		// Motor 의 이동량을 Buffer 에 넣어주고, 이 신호를 1로 바꾸어 주면,
		// 이동량이 FPGA 내부의 관련 Register로 Latch 된다.
		unsigned Motor_Con: 1;
		// DC Motor 의 High side 구동인지. Low side 구동인지를 결정한다.
		// 1 : Low side 구동
		// 0 : High side 구동
		unsigned Motor_Start: 1;
		// DC Motor 의 이동 시작신호이다.
		unsigned Motor_Dir: 1;
		// DC Motor 의 이동방향을 결정한다.
		// 1 : Clockwise
		// 0 : Counter Clockwise
		unsigned Motor_Auto: 1;
		// DC Motor 의 Automatic control 를 사용할지 여부를 결정한다.
		// 이동량을 주고, Motor_On 을 시키면, 이동량만큼 이동한다.
		unsigned Motor_On: 1;
		// DC Motor 를 사용할지 안할 지를 결정한다. 동작중이라도 0으로 만들면, Motor 가 정지한다.
		// 1 : 정상구동
		// 0 : OPEN
		unsigned dummy: 24;
	}	b;
}; // Register 0x07

union	Motor_status
{
	u16	byte;
	struct
	{
		unsigned z_pos :	1;
		unsigned z_dir:		1;
		unsigned xh_pos :	1;
		unsigned xh_dir :	1;
		unsigned xb_pos :	1;
		unsigned xb_dir :	1;
		unsigned y_pos :	1;
		unsigned y_dir :	1;
		volatile unsigned dummy: 8;
	}	b;
};

void init_dcm_ctrl(int motor_no);
void update_motor_ctrl(int motor_no);
void motor_enable(int motor_no, int flag);
void motor_side(int motor_no, int side); 
void motor_rotate(int motor_no, int dir);
void motor_brake(int motor_no, int on_brake);
void motor_move(int motor_no, int on_move);
void update_motor_speed(int motor_no);
int update_motor_divider(int motor_no) ;
int DCM_move_center_zh(void);
void DCM_move_cw(int motor_no, int duty);
void DCM_move_ccw(int motor_no, int duty);
void DCM_move_auto(int motor_no, int targ_enc, int duty);
void DCM_move_auto2(int motor_no, int direct, int targ_enc, int dist_enc);
void DCM_brake(int motor_no) ;
void DCM_duty(int motor_no, int duty) ;
void DCM_duty_auto(int motor_no, int direct, int dist_enc) ;
void DCM_move(int motor_no) ;
void DCM_stop(int motor_no) ;
void motor_auto_mode(int motor_no, int flag) ;
int wait_enc_diff(int enc_no, int diff, int exp_tick) ;
int wait_enc_diff2(int enc_no, int diff, int exp_cnt) ;
int wait_pi_signal2(int enc_no, int pis, int exp_cnt) ;
int pis_read(int enc_no);
void update_motor_encoder(int motor_no, int targ_enc) ;
void init_dc_motors(void);
void init_dc_motor(int motor_no);
void HEAD_move_enc(int direct, int targ_enc);
int motor_encoder(int motor_no, int *enc_val);
int motor_center(int motor_no, int *cent_enc);
int PI_X_HEAD(void);
int PI_Z_HEAD(void);
int DCM_move_center(int motor_no);
int DCM_move_center_xh() ;
void DCM_af_xzmove(int motor_no, int dir,int duty);
int dc_motor_pi_state(int bit_pos);
int MoveHeadRestLowLimit(void);
int MoveHeadRestHighLimit(void);
void CR_move(char dir);
void CR_stop(void);
void cr_limit_check(void);

#endif 
