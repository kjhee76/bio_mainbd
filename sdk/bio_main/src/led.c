
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_types.h"
#include "led.h"

unsigned int * AD5308_BASEADDRESS = (unsigned int *)0x43C20000;
/*
 * AD5308 Register
 *
 * Data IN  ==> AD5308 input (cpu write)
 * spi_dataH_i_reg offset 4
 * spi_dataH_i_reg offset 0
 *
 * spi_data_wr_reg  offset 16  -- write strobe
 *
 * Data Out  ==>
 * spi_dataH_o_reg offset 12
 * spi_dataL_o_reg offset 8
 *
 */
short int AD5308DataWrite(u16 ch, u16 Data)
{

	if((ch>7)||(Data>256)) return -1;
	*(AD5308_BASEADDRESS+1)=0x0;
	*(AD5308_BASEADDRESS)=(ch<<12)|(Data<<4);
	usleep(1000); //delay for spi serialization time
	*(AD5308_BASEADDRESS+4)=1;
	usleep(1000); //delay for spi serialization time
	return 0;
}

//brightness 0~100

u16 SetLED(u8 LED_ch, u16 brightness)
{
	u16 lRet;
	u8 dac_dn;

	if(brightness>100) brightness=100;
	dac_dn= (u8)(2.55*brightness); //scale percentage to 8bit level
	if((dac_dn<LED_MIN_INTENSITY)&&(dac_dn>0)) dac_dn=LED_MIN_INTENSITY;
	//uart_printf("IRLED dac dn=%d",dac_dn);
	printf("AD5308 LED[%d], setdac dn=%d",LED_ch,dac_dn);
	lRet=AD5308DataWrite(LED_ch,dac_dn);

	if(lRet) return 2;
	return 0;

}


